#
# This is 
#
# Usage: python ./tools/format_text.py /Library/Fonts/PTMono.ttc 16 $'Hello World\nThis is a test.' 320 3
#

def line_chars(text, max_line_w, strides, wrap_whole_words=True):
    j = 0
    line_w = 0
    for i, c in enumerate(text):
        if c == '\n':
            break
        line_w += strides[ord(c)]
        if line_w >= max_line_w:
            if c != ' ':
                i = j
            break
        else:
            if c == ' ':
                j = i
    return i + 1

def paginate_text(text, page_w, max_lines, strides, justify=None, wrap_whole_words=True, trim=True):
    """Returns pages of text, where each page is a single string."""
    pages = []
    while text:
        lines = []
        while text and len(lines) < max_lines:
            j = line_chars(text, page_w, strides, wrap_whole_words)
            line = text[:j]
            if trim:
                line = line.strip()
            lines.append(line)
            text = text[j:]
        pages.append('\n'.join(lines))
    return pages

if __name__ == "__main__":
    import sys
    from PIL import Image, ImageFont, ImageDraw
    import dump_font    
    font = ImageFont.truetype(sys.argv[1], int(sys.argv[2]))
    strides = dump_font.get_font_strides(font)  # [1 for _ in range(128)]
    pages = paginate_text(sys.argv[3], int(sys.argv[4]), int(sys.argv[5]), strides)
    print('{{{}}}'.format(','.join(['"{}"'.format(repr(p)[1:-1]) for p in pages])))
        
