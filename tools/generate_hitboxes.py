import sys, re, itertools

from PIL import Image, ImageFilter, ImageDraw

def rect_empty(a):
    return a[0] >= a[2] or a[1] >= a[3]

def rect_inside(a, b):
    return a[0] >= b[0] and a[2] <= b[2] and a[1] >= b[1] and a[3] <= b[3]

def rect_adjacent_x(a, b):
    return a[1] == b[1] and a[3] == b[3] and (a[0] == b[2] or a[2] == b[0])

def rect_adjacent_y(a, b):
    return a[0] == b[0] and a[2] == b[2] and (a[1] == b[3] or a[3] == b[1])

def rect_adjacent(a, b):
    return rect_adjacent_x(a, b) or rect_adjacent_y(a, b)

def rect_inside_others(a, others):
    for b in others:
        if a is not b and rect_inside(a, b):
            return True
    return False

def rect_intersects(a, b):
    return rect_empty(intersect_rect(a, b))

def intersect_rect(a, b):
    return (max(a[0], b[0]), 
            max(a[1], b[1]), 
            min(a[2], b[2]), 
            min(a[3], b[3]))

def union_rect(a, b):
    return (min(a[0], b[0]),
            min(a[1], b[1]),
            max(a[2], b[2]),
            max(a[3], b[3]))

def image_normalize_and_extract_alpha(image, threshold=0):
    data = image.load()
    w, h = image.size
    max_a = 0
    for x in xrange(w):
        for y in xrange(h):
            r, g, b, a = data[x, y]
            max_a = max(max_a, a)
    for x in xrange(w):
        for y in xrange(h):
            r, g, b, a = data[x, y]
            a = 255 * a / max_a
            data[x, y] = (a, a, a, 255)
    return image

def image_blur(image, radius):
    return image.filter(ImageFilter.GaussianBlur(radius))

def image_apply_threshold(image, threshold):
    data = image.load()
    w, h = image.size
    for x in xrange(w):
        for y in xrange(h):
            r, g, b, a = data[x, y]
            a = 255 if r >= threshold else 0
            data[x, y] = (a, a, a, 255)
    return image

def image_calc_bounds(image):    
    data = image.load()
    w, h = image.size
    x1, y1, x2, y2 = 0, 0, w-1, h-1

    while x1 <= x2:
        for y in xrange(y1, y2):
            r, g, b, a = data[x1, y]
            if r:
                break
        else:
            x1 += 1
            continue
        break

    while y1 <= y2:
        for x in xrange(x1, x2):
            r, g, b, a = data[x, y1]
            if r:
                break
        else:
            y1 += 1
            continue
        break
    
    while x2 >= x1:
        for y in xrange(y1, y2):
            r, g, b, a = data[x2, y]
            if r:
                break
        else:
            x2 -= 1
            continue
        break    
    
    while y2 >= y1:
        for x in xrange(x1, x2):
            r, g, b, a = data[x, y2]
            if r:
                break
        else:
            y2 -= 1
            continue
        break
                
    return x1, y1, x2, y2

def image_hitbox(image, filename, blur_radius=None, threshold=150):
    w, h = image.size
    if blur_radius is None:
        blur_radius = min(w, h) / 8
    image = image.convert("RGBA")
    image = image_normalize_and_extract_alpha(image, 255)
    image = image_apply_threshold(image, 255)
    image = image_blur(image, blur_radius)
    image = image_apply_threshold(image, threshold)
    image.save(re.sub('\.png$', '_mask.png', filename))    
    return image_calc_bounds(image)
    
if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description='Generate hitboxes for sprites.')
    parser.add_argument('in_filenames', type=str, nargs='*',
                        help=("filenames of sprite images to include in the "
                              "sprite sheet"))
    parser.add_argument('--in-list', type=str,
                        help="list of sprite images to include")
    parser.add_argument('--prefix-full-path',
                        action='store_true',
                        help="prefix Pinky variables with the full path")
    parser.add_argument('--hitbox-threshold', type=int, default=150,
                        help="between 0 - 255")
    parser.add_argument('--prefix', type=str, default='BB_',
                        help="prefix for Pinky variables")
    parser.add_argument('--units', default='',
                        help="units-of-measurement for bounds")
    args = parser.parse_args()
    
    sprite_filenames = args.in_filenames

    if args.in_list:
        with open(args.in_list, "rt") as f:
            sprite_filenames.extend(filter(None, f.read().split("\n")))

    if not sprite_filenames:
        print "Not enough sprites given"
        exit(1)

    sprites = sorted(
        [(filename, Image.open(filename)) for filename in sprite_filenames],
        key=lambda x: x[0])

    # Compute and output hitboxes
    if args.hitbox_threshold:
        hitboxes = {}
        for group_name, group in itertools.groupby(
                sprites,
                key=lambda x: re.sub('_[0-9]+', '',
                                     re.sub('\.png$', '', x[0]))
        ):
            if not group_name:
                continue
            
            group = list(group)
            group_hitboxes = []
            
            for filename, sprite in group:
                if not sprite:
                    continue
                group_hitboxes.append(
                    image_hitbox(
                        sprite,
                        filename,
                        threshold=args.hitbox_threshold))
                
            hitboxes[group_name] = reduce(intersect_rect, group_hitboxes)

            # Draw outline and save image
            for filename, sprite in group:
                hitbox = hitboxes[group_name]
                draw = ImageDraw.Draw(sprite)
                draw.rectangle(hitbox, outline="red")
                sprite.save(re.sub('.png$', '_hitbox.png', filename))
            ######
            
        for group_name, hitbox in hitboxes.iteritems():
            name = group_name.upper()
            if not args.prefix_full_path:
                name = name.rsplit('/', 1)[-1]
            name = re.sub('\W', '_', re.sub('^\W+', '', name))
            print '{} = {{{}}} as Box'.format(args.prefix+name, ','.join(map(lambda x: '{}{}'.format(x, args.units), hitbox)))
