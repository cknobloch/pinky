import sys, re, itertools

from PIL import Image, ImageFilter, ImageDraw, ImageOps

def image_merge_into(image, sub_images):
    x = 0
    for i, sub_image in enumerate(sub_images):
        image.paste(sub_image, (x, 0))
        x += sub_image.width

def image_split(image, num_images):
    w = image.width / num_images
    return [image.crop((i*w, 0, i*w+w, image.height)) for i in range(num_images)]

def sub_image_flip_x(sub_image):
    return sub_image.transpose(Image.FLIP_LEFT_RIGHT)

if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description='Fixup sprites in a sprite series.')
    parser.add_argument('filename', type=str, help="filename of sprite series")
    parser.add_argument('num_sprites', type=int, help="number of sprites in the sprite series")
    parser.add_argument('--flip-x', action='store_true', help="flip sprites on x axis")
    args = parser.parse_args()

    original_image = Image.open(args.filename)
    sub_images = image_split(original_image, args.num_sprites)
    if args.flip_x:
        print("flipping")
        sub_images = map(sub_image_flip_x, sub_images)
    image_merge_into(original_image, sub_images)
    original_image.save(args.filename)
