import sys, re, itertools

from PIL import Image

def rect_empty(a):
    return a[0] >= a[2] or a[1] >= a[3]

def rect_inside(a, b):
    return a[0] >= b[0] and a[2] <= b[2] and a[1] >= b[1] and a[3] <= b[3]

def rect_adjacent_x(a, b):
    return a[1] == b[1] and a[3] == b[3] and (a[0] == b[2] or a[2] == b[0])

def rect_adjacent_y(a, b):
    return a[0] == b[0] and a[2] == b[2] and (a[1] == b[3] or a[3] == b[1])

def rect_adjacent(a, b):
    return rect_adjacent_x(a, b) or rect_adjacent_y(a, b)

def rect_inside_others(a, others):
    for b in others:
        if a is not b and rect_inside(a, b):
            return True
    return False

def rect_intersects(a, b):
    return rect_empty(intersect_rect(a, b))

def filter_inner_regions(regions):
    # If equal regions aren't filtered first, they're detected as
    # being inside each other, and both get filtered out
    regions = filter_equal_regions(filter_empty_regions(regions))
    return [a for a in regions if not rect_inside_others(a, regions)]

def filter_equal_regions(regions):
    return [a for i, a in enumerate(regions)
            if not [True for b in regions[i+1:] if a == b]]

def filter_empty_regions(regions):
    return [a for a in regions if not rect_empty(a)]

def intersect_rect(a, b):
    return (max(a[0], b[0]), 
            max(a[1], b[1]), 
            min(a[2], b[2]), 
            min(a[3], b[3]))

def union_rect(a, b):
    return (min(a[0], b[0]),
            min(a[1], b[1]),
            max(a[2], b[2]),
            max(a[3], b[3]))

def subtract_rect(a, b):
    b = intersect_rect(a, b)
    if rect_empty(b):
        return [a]
    else:
        return [(a[0], a[1], b[0], b[1]),
                (b[0], a[1], b[2], b[1]),
                (b[2], a[1], a[2], b[1]),
                (a[0], b[1], b[0], b[3]),
                (b[2], b[1], a[2], b[3]),
                (a[0], b[3], b[0], a[3]),
                (b[0], b[3], b[2], a[3]),
                (b[2], b[3], a[2], a[3])]

def subtract_rect_overlapping(a, b):
    b = intersect_rect(a, b)
    if rect_empty(b):
        return [a]
    else:
        return [(a[0], a[1], b[0], a[3]),
                (a[0], a[1], a[2], b[1]),
                (b[2], a[1], a[2], a[3]),
                (a[0], b[3], a[2], a[3])]
    
def merge_rect_into_list(a, regions):
    for i, b in enumerate(regions):
        if rect_adjacent(a, b):
            return merge_rect_into_list(union_rect(a, b), regions)
    return a

def add_rect_to_merged_list(a, regions):
    return filter_inner_regions(regions+[merge_rect_into_list(a, regions)])

def subtract_rect_from_merged_list(a, regions):
    out = []
    for b in regions:
        for c in filter_empty_regions(subtract_rect_overlapping(b, a)):
            out.append(merge_rect_into_list(c, out))
    return filter_inner_regions(out)

class NoEncompassingRegionFound(Exception):
    pass

def best_encompassing_region(w, h, regions):
    matches = [b for b in regions if (w <= (b[2]-b[0]) and h <= (b[3]-b[1]))]
    if not matches:
        raise NoEncompassingRegionFound()    
    return sorted(
        matches,  # pick smallest geom area, then x, y
        key=lambda b: ((b[2]-b[0])*(b[3]-b[1]), (b[0], b[1])))[0]

def sub_image(image, x, y, w, h):
    return image.crop((x, y, w, h))

class SpriteSheetBuilder:
    def __init__(self, size=None, dest=None, auto_resize=True, padding=0,
                 offset=None):
        size = size or (0, 0)
        self.dest = dest or Image.new("RGBA", size, (0, 0, 0, 0))
        self.free_regions = filter_empty_regions(
            [(0, 0, self.dest.width, self.dest.height)])
        self.sprite_regions = []
        self.auto_resize = auto_resize
        self.padding = padding
        self.offset = offset
    
    def add_sprite(self, sprite):
        w, h = sprite.width+self.padding*2, sprite.height+self.padding*2
        try:
            x, y, _, _ = best_encompassing_region(w, h, self.free_regions)
        except NoEncompassingRegionFound as e:
            if not self.auto_resize:
                raise

            dest_w = self.dest.width
            dest_h = self.dest.height
            if   dest_w > dest_h and dest_w >= w:
                self.resize_dest((max(self.dest.width, w), self.dest.height+h))
            elif dest_w < dest_h and dest_h >= h:
                self.resize_dest((self.dest.width+w, max(self.dest.height, h)))
            elif w > h:
                self.resize_dest((max(self.dest.width, w), self.dest.height+h))
            else:
                self.resize_dest((self.dest.width+w, max(self.dest.height, h)))
                
            x, y, _, _ = best_encompassing_region(w, h, self.free_regions)
            
        region = (x, y, x+w, y+h)
        
        self._update_free_regions(region)
        
        region_actual = (region[0]+self.padding,
                         region[1]+self.padding,
                         region[2]-self.padding,
                         region[3]-self.padding)
        
        self.sprite_regions.append((sprite, region_actual))

    def _update_free_regions(self, new_region):
        self.free_regions = subtract_rect_from_merged_list(
            new_region,
            self.free_regions)

    def resize_dest(self, size):
        w, h = size
        dest = Image.new("RGBA", size, (0, 0, 0, 0))
        dest.paste(self.dest, (0, 0))
        if h > self.dest.height:
            self.free_regions = add_rect_to_merged_list(
                (0, self.dest.height, w, h),
                self.free_regions)
        if w > self.dest.width:
            self.free_regions = add_rect_to_merged_list(
                (self.dest.width, 0, w, h),
                self.free_regions)
        self.dest = dest

    def save(self, filename):
        off_x, off_y = self.offset
        for sprite, region in self.sprite_regions:
            x, y = region[:2]
            self.dest.paste(sprite, (x+off_x, y+off_y))
        self.dest.save(filename)

    def to_ky(self, null_zero=False, units=''):
        def pixel_value_to_ky(value):
            return '{}{}'.format(value, units)
        def region_to_ky(region):
            return '{{{}}}'.format(','.join(map(pixel_value_to_ky, region)))
        str_regions = [
            region_to_ky(region)
            for _, region in self.sprite_regions]
        if null_zero:
            str_regions = ['null'] + str_regions
        return "{{{}}}".format(",".join(str_regions))

def filename_to_id(filename, prefix_full_path=False):
    name = filename.rsplit('.', 1)[0].upper()
    if not prefix_full_path:
        # TODO: remove this param and
        # always use minmum path that doesn't collide
        name = name.rsplit('/', 1)[-1]
    return re.sub('\W', '_', re.sub('^\W+', '', name))

if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description='Build sprite sheets.')
    parser.add_argument('out_filename', type=str,
                        help="filename of the outputted sprite sheet PNG image")
    parser.add_argument('in_filenames', type=str, nargs='*',
                        help=("filenames of sprite images to include in the "
                              "sprite sheet"))
    parser.add_argument('--in-list', type=str,
                        help="list of sprite images to include")
    parser.add_argument('--padding', type=int, default=0)
    parser.add_argument('--offset-x', type=int, default=0)
    parser.add_argument('--offset-y', type=int, default=0)    
    parser.add_argument('--null-zero', default=False,
                        action='store_true',
                        help="zeroth sprite is null")
    parser.add_argument('--seq-only',
                        action='store_true',
                        help="output sequences only not individual sprites")
    parser.add_argument('--prefix-full-path',
                        action='store_true',
                        help="prefix Pinky variables with the full path")
    parser.add_argument('--prefix', default='SI_',
                        help="prefix for Pinky constants")
    parser.add_argument('--seq-prefix', default='FS_',
                        help="prefix for Pinky (frame) sequence constants")    
    parser.add_argument('--units', default='',
                        help="units-of-measurement for sprite bounds")
    parser.add_argument('--base-index', default=0, type=int)
    args = parser.parse_args()
    
    sprite_sheet_filename = args.out_filename
    sprite_filenames = args.in_filenames

    if args.in_list:
        with open(args.in_list, "rt") as f:
            sprite_filenames.extend(filter(None, f.read().split("\n")))

    if not sprite_filenames:
        print("Not enough sprites given")
        exit(1)

    sprite_sheet_id = filename_to_id(sprite_sheet_filename)

    # TODO: allow updating existing sprite sheet?
    #try:
    #    sprite_sheet = Image.open(sprite_sheet_filename)
    # except IOError:
    #    sprite_sheet = Image.new("RGBA",
    #                             NEW_SPRITE_SHEET_DIMENSIONS,
    #                             (0,0,0,0))

    ss_builder = SpriteSheetBuilder(padding=args.padding,
                                    offset=(args.offset_x, args.offset_y))

    # Sort by image size first to keep the output more organized and optimal
    sprites = sorted([(filename, Image.open(filename))
                      for filename in sprite_filenames],
                     key=lambda (_, x): (x.width, x.height))

    # Build sprite names
    out_sprites = []
    for filename, sprite in sprites:
        sprite_id = filename_to_id(filename, args.prefix_full_path)
        match = re.match('(\w+?)_(\d+)X(\d+)', sprite_id)
        if not match:
            out_sprites.append((sprite_id, sprite))
        else:
            base_id, w, h = match.groups()
            w = int(w)
            h = int(h)
            sprite_w = sprite.width  // w
            sprite_h = sprite.height // h
            for y in range(h):
                for x in range(w):
                    i = x + y * w
                    x1 = x * sprite_w
                    y1 = y * sprite_h
                    sub_sprite = sub_image(
                        sprite, x1, y1, x1+sprite_w, y1+sprite_h)
                    out_sprites.append((base_id+'_{}'.format(i), sub_sprite))
    sprites = out_sprites
    
    for _, sprite in sprites:
        ss_builder.add_sprite(sprite)

    ss_builder.save(sprite_sheet_filename)

    # Output Pinky sprite regions
    print("{} = {}".format(
        sprite_sheet_id,
        ss_builder.to_ky(null_zero=args.null_zero, units=args.units)))

    print("")

    if args.null_zero:
        sprites = [('_', None)] + sprites

    # Output Pinky sprite index constants
    if args.seq_only:
        sprite_indices = {
            sprite_id: i
            for i, (sprite_id, _) in enumerate(sprites)}
    else:
        for i, (sprite_id, _) in enumerate(sprites):
            print('{} = {}'.format(args.prefix+sprite_id, i+args.base_index))

    # Output Pinky sequence constants
    seq_group = lambda (x, _): re.sub('_*\d+$', '', x)
    
    for i, (seq_id, seq_sprites) in enumerate(
            itertools.groupby(sprites, seq_group)):
        print('{}{} = {}'.format(args.seq_prefix, seq_id, i))
        
    for seq_id, seq_sprites in itertools.groupby(sprites, seq_group):
        frames = [sprite_id for sprite_id, _ in seq_sprites]
        if args.seq_only:
            frames = [str(sprite_indices[f]) for f in frames]
            print('{{{}}},'.format(",".join(frames)))
        else:
            frames = [(args.prefix+sprite_id) for sprite_id in frames]
            print('{}{} = {{{}}} as int[]'.format(
                args.seq_prefix, seq_id, ",".join((frames))))
        
