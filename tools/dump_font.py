
#
# This is a quick and dirty script that dumps a TTF font
# to a PNG sprite sheet.
#
# Usage: python ./tools/dump_font.py /Library/Fonts/PTMono.ttc font8.png 8
#

import sys, re

from PIL import Image, ImageFont, ImageDraw

def indent(text):
    return re.sub('^', '    ', text, flags=re.MULTILINE)

def px_to_ky(value):
    return '{}px'.format(float(value))

def list_to_ky(values, inline=False, start_char='{', end_char='}'):
    if values:
        if inline:
            return '{}{}{}'.format(
                start_char, ', '.join(values), end_char)
        else:
            return '{}\n{}\n{}'.format(
                start_char, indent(',\n'.join(values)), end_char)
    else:
        return start_char+end_char

def get_font_strides(font, start=0, end=128):
    return [font.font.getsize(chr(i))[0][0] for i in range(end-start)]

def create_font_image(font, font_size, chars_per_line=16, start=0, end=128, padding=0, fixed_width=False, color=(255, 255, 255)):
    grid_w = font_size + padding
    grid_h = font_size + padding
    w = chars_per_line
    num_chars = end - start
    h = (num_chars+w-1) // w
    img  = Image.new("RGBA", (w*grid_w+padding, h*grid_h+padding), (0,0,0,0))
    draw = ImageDraw.Draw(img)

    for i in range(h):
        y = i * grid_h + padding
        for j in range(w):
            x = j * grid_w + padding
            c = chr(start+i*w+j)
            (glyph_w, _), _ = font.font.getsize(c)
            if fixed_width:
                offset = (font_w-glyph_w) // 2
            else:
                offset = 0
            draw.text((x+offset, y), c, color, font=font)

    return img

if __name__ == "__main__":
    font_size = int(sys.argv[3])    
    font = ImageFont.truetype(sys.argv[1], font_size)
    
    img = create_font_image(font, font_size)
    img.save(sys.argv[2])

    spacing = int(sys.argv[4]) if len(sys.argv) > 4 else 0
        
    print(list_to_ky(map(px_to_ky, [x+spacing for x in get_font_strides(font)]), inline=True))
