#
# This is a quick and dirty script that converts a
# tilemap created with the application Tiled (thanks T.L.)
# to one compatible with Pinky.
#
# Usage: python3 ./convert_tmx_tilemap.py map.tmx
#

import os, sys, base64, re, math
import json
import xml.etree.ElementTree as ET
from shutil import copyfile
from collections import OrderedDict

if sys.version_info < (3,):
    def ord_to_bytes(x):
        return b"".join(map(chr, x))
else:
    ord_to_bytes = bytes

DEFAULT_WORLD_PREFIX = 'world = '
DEFAULT_TS_PREFIX = 'tilesets.'

# Bits on the far end of the 32-bit global tile ID are used for tile flags
FLIPPED_HORIZONTALLY_FLAG = 0x80000000
FLIPPED_VERTICALLY_FLAG   = 0x40000000
FLIPPED_DIAGONALLY_FLAG   = 0x20000000
FLAGS_MASK = 0xF0000000

# TODO: implement these flags by referencing different tilesets
tiled_tile_flag_lookup = {
    FLIPPED_HORIZONTALLY_FLAG: 0,
    FLIPPED_VERTICALLY_FLAG  : 0,
    FLIPPED_DIAGONALLY_FLAG  : 0
}

flags_lookup = {
    'left'    : 0x01,
    'bottom'  : 0x02,
    'right'   : 0x04,
    'top'     : 0x08,
    'wall'    : 0x10,
    'ice'     : 0x20,    
    'sand'    : 0x30,
    'spike'   : 0x40,    
    'fire'    : 0x50,
    'water'   : 0x60,
    'jump'    : 0x70,
    'guide'   : 0x80,
}

def D_index(self, k):
    i = 0
    for kk in self.keys():
        if kk == k:
            return i
        i += 1
    raise KeyError(k)

def camel_to_snake(name):
    name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', name).lower()

def indent(text):
    return re.sub('^', '    ', text, flags=re.MULTILINE)

def px_to_ky(value):
    return '{}px'.format(float(value))

def fp_to_ky(value):
    return str(float(value))

def int_to_ky(value):
    return str(value)

def bool_to_ky(value):
    return 'true' if value else 'false'

def image_to_ky(filename):
    # if not filename:
    #     return 'null'
    # return 'img(@"{}")'.format(data)
    return filename_to_var(filename, prefix='images.')

def color_to_ky(color):
    return hex(color)

def blob_to_ky(data):
    if not data:
        return 'null'
    data_b64 = base64.b64encode(data).decode('ascii')
    return 'decodeb64("{}")'.format(data_b64)

def list_to_ky(values, inline=False, start_char='{', end_char='}'):
    if not values:
        return start_char + end_char
    if inline:
        return '{}{}{}'.format(
            start_char, ', '.join(values), end_char)
    else:
        return '{}\n{}\n{}'.format(
            start_char, indent(',\n'.join(values)), end_char)

def as_ky(value, type):
    return '{} as {}'.format(value, type)

def args_to_ky(values, inline=False):
    return list_to_ky(values, inline=inline, start_char='', end_char='')

def convert_tile(packed_tile):
    tile, tile_flags = packed_tile&~FLAGS_MASK, packed_tile&FLAGS_MASK
    return tile | tiled_tile_flag_lookup.get(tile_flags, 0)

def convert_global_tile(packed_tile, ts_index):
    i = convert_tile(packed_tile)
    if i == 0:
        return 0
    else:
        return i - ts_index + 1

def convert_layer_data(data, start_index, sw, sh, tw, th):
    out_data = b""
    w  = int(math.ceil(sw/tw))
    h  = int(math.ceil(sh/th))
    sy = th - 1  # bigger tiles draw from the *bottom* left
    for y in range(h):
        sx = 0
        for x in range(w):
            out_data += ord_to_bytes([
                convert_global_tile(data[sx+sy*sw], start_index)
            ])
            sx += tw
        sy += th
    return out_data

def convert_tile_flags(flags):
    out_flags = 0
    clean_flags = [f.strip().lower() for f in flags.split(',')]
    for f in clean_flags:
        out_flags |= flags_lookup.get(f, 0)
    if out_flags and not (out_flags&0xF0):
        out_flags |= flags_lookup['wall']
    return out_flags

def find_tile_range(data):
    tile_min, tile_max = 1000000, 0
    for tile in data:
        if tile:
            tile_min = min(tile, tile_min)
            tile_max = max(tile, tile_max)
    return tile_min, tile_max

def find_property(elem, name, default_value=None):
    props = elem.find('properties')
    if props is not None:
        for prop in props.findall('property'):
            if prop.get('name') == name:
                return prop.get('value')
    return default_value

def find_tileset_index(gid, tilesets):
    return sorted(i for i in tilesets if i <= gid)[-1]

def filename_to_var(filename, prefix='', prefix_full_path=False):
    name = filename.rsplit('.', 1)[0]
    if not prefix_full_path:
        # TODO: remove this param and
        # always use minmum path that doesn't collide
        name = name.rsplit('/', 1)[-1]
    return prefix + re.sub('\W', '_', re.sub('^\W+', '', name))

def tileset_ref_to_ky(ts):
    return filename_to_var(ts.name, prefix=DEFAULT_TS_PREFIX)
    
class Tileset:
    def __init__(self, name, src, tw, th, spacing, margin,
                 anims, tile_flags, tile_count, tile_images, start_index=0):
        self.name = name
        self.src = src
        self.tw = tw
        self.th = th
        self.spacing = spacing
        self.margin = margin
        self.anims = anims
        self.tile_flags = tile_flags
        self.tile_count = tile_count
        self.tile_images = tile_images
        
        # Tiles in a tileset don't start at zero when a tileset has been deleted from
        self.start_index = start_index
        
    @staticmethod
    def from_tmx(root, source_dir='.'):
        # <tileset firstgid="185" name="EmbeddedTileset" tilewidth="16" tileheight="16" tilecount="88" columns="8">
        #  <image source="../Downloads/Fantasy Assets/CastleDungeonPack/Tileset/Tileset.png" width="128" height="176"/>
        # <tile id="2">
        #  <animation>
        #   <frame tileid="19" duration="100"/>
        #   <frame tileid="27" duration="100"/>
        #   <frame tileid="18" duration="100"/>
        #  </animation>
        # </tile>        
        # <tile id="1">
        #  <properties>
        #   <property name="Flags" value="top"/>
        #  </properties>
        # </tile>        
        # </tileset>
        
        tilesets = {}
        
        for tileset in root.findall('tileset'):
            start_index = int(tileset.attrib.get('firstgid', 0))
            
            if 'source' in tileset.attrib:
                ts_path = os.path.join(source_dir, tileset.attrib['source'])
                tileset = ET.parse(ts_path).getroot()

            tilesets[start_index] = Tileset.from_tsx(tileset)

        tilesets = OrderedDict(sorted(tilesets.items(), key=lambda item: item[0]))

        # Scout for usages of the tileset in tilemaps and tileobjs,
        # so that unused tilesets aren't included in the final output.

        used_tilesets = set()
        
        for layer in root.findall('layer'):
            data = list(map(int, layer.find('data').text.split(',')))
            map_tile_min, _ = find_tile_range(data)
            tileset_index = find_tileset_index(map_tile_min, tilesets)
            used_tilesets.add(tileset_index)
            
        for obj_group in root.findall('objectgroup'):
            for obj in obj_group.findall('object'):
                if not obj.get('gid'):
                    continue  # not a Tile
                if obj.get('type') and obj.get('type')[0] != '+':
                    continue  # not a TileObject
                i = convert_tile(int(obj.get('gid')))
                assert i != 0
                tileset_index = find_tileset_index(i, tilesets)
                used_tilesets.add(tileset_index)            

        return OrderedDict([(k, v) for k, v in tilesets.items() if k in used_tilesets])

    @staticmethod
    def from_tsx(tileset):
        anims = []
        all_tile_flags = {}

        base_image = tileset.find('image')
        
        tile_images = []

        first_tile_id = None
        
        for tile in tileset.findall('tile'):
            tile_id = int(tile.attrib['id'])

            if first_tile_id is None:
                first_tile_id = tile_id
            else:
                first_tile_id = min(first_tile_id, tile_id)
            
            frames = []
            anim_elem = tile.find('animation')
            if anim_elem is not None:
                for frame in anim_elem.findall('frame'):
                    frames.append(frame.attrib['tileid'])
                anims.append((tile_id, frames))

            tile_flags = convert_tile_flags(find_property(tile, 'Flags', ''))
            if tile_flags:
                all_tile_flags[tile_id] = tile_flags

            tile_image = tile.find('image')
            if tile_image is not None:
                tile_images.append(tile_image.attrib['source'])

        if base_image is None:
            base_image_src = None
        else:
            base_image_src = base_image.attrib['source']
                
        return Tileset(
            tileset.attrib.get('name'),
            base_image_src,
            int(tileset.attrib['tilewidth']),
            int(tileset.attrib['tileheight']),
            int(tileset.attrib.get('spacing', 0)),
            int(tileset.attrib.get('margin', 0)),
            anims,
            all_tile_flags,
            int(tileset.attrib['tilecount']),
            tile_images,
            first_tile_id or 0)

    def to_ky(self):
        def anim_to_ky(anim):
            return '{{{i}, {{{frames}}}}}'.format(
                i=anim[0],
                frames=', '.join(map(str, anim[1])))

        tile_flags = [
            self.tile_flags.get(i, 0)
            for i in range(self.tile_count)]

        tile_images = [
            image_to_ky(tile_image)
            for tile_image in self.tile_images]
        
        return ('# {} - {}\n{{}}'.format(self.name, self.tile_count) if self.name else '{}').format(
            list_to_ky([
                'null' if not self.src else image_to_ky(self.src),
                'null' if not tile_images else list_to_ky(tile_images),
                list_to_ky(list(map(anim_to_ky, self.anims))),
                blob_to_ky(ord_to_bytes(tile_flags)),
                px_to_ky(self.tw),
                px_to_ky(self.th),
                px_to_ky(self.spacing),
                px_to_ky(self.margin)
            ]))

class Tilemap:
    def __init__(self, data, w, h, tw, th, x, y,
                 tileset_index=-1, tileset=None, name=None):
        self.name = name
        self.data = data
        self.w  = w
        self.h  = h
        self.tw = tw
        self.th = th
        self.x  = x
        self.y  = y
        self.tileset_index = tileset_index
        self.tileset = tileset

    def upscale_width(self, new_w_factor):
        out_data = b""
        w = math.ceil(self.w)
        h = math.ceil(self.h)
        new_w = self.w * new_w_factor
        for y in range(h):
            for x in range(math.ceil(new_w)):
                out_data += ord_to_bytes([self.data[(x%w)+(y*w)]])
        self.data = out_data
        self.w = new_w
        
    @property
    def pw(self):
        return math.floor(self.w * self.tw)  # + self.x

    @property
    def ph(self):
        return math.floor(self.h * self.th)  # + self.y

    def to_ky(self):
        return '# {}\n{}'.format(
            self.name or '?',
            list_to_ky([
                tileset_ref_to_ky(self.tileset),
                blob_to_ky(self.data),
                str(int(math.ceil(self.w))),
                str(int(math.ceil(self.h))),
                px_to_ky(self.tw),
                px_to_ky(self.th),
                px_to_ky(self.x),
                px_to_ky(self.y),
                px_to_ky(self.pw),
                px_to_ky(self.ph)
            ]))
            
    @staticmethod
    def from_tmx(root, tilesets=None):
        # <map version="1.2" tiledversion="1.2.1" orientation="orthogonal" renderorder="right-down" width="160" height="160" tilewidth="16" tileheight="16" infinite="0" nextlayerid="2" nextobjectid="1">
        #  <tileset firstgid="1" source="Tileset.tsx"/>
        #  <layer id="1" name="Tile Layer 1" width="160" height="160">
        #   <data encoding="csv">

        # <objectgroup id="3" name="Object Layer 1">
        #  <object id="1" name="Main Camera Path" type="campath" x="45" y="2524">
        #   <polyline points="0,0 -28,-190 63,-331 461,-340 512,-239 467,-136 378,-31 183,-40 104,-99 106,-200 207,-254 298,-227 335,-194 267,-106 178,-175 -18,-202"/>
        #  </object>
        # </objectgroup>
    
        tilemaps = []

        sw = int(root.get('width'))
        sh = int(root.get('height'))
        tw = int(root.get('tilewidth'))
        th = int(root.get('tileheight'))

        for layer in root.findall('layer'):
            data = list(map(int, layer.find('data').text.split(',')))
            map_tile_min, map_tile_max = find_tile_range(data)
            tileset_index = find_tileset_index(map_tile_min, tilesets)
            tileset = tilesets[tileset_index]
            tile_min = tileset_index + tileset.start_index

            w = float(find_property(layer, 'actualw', layer.attrib['width']))
            h = float(find_property(layer, 'actualh', layer.attrib['height']))
                
            tilemaps.append(Tilemap(
                convert_layer_data(
                    data,
                    tile_min,
                    sw,
                    sh,
                    tileset.tw//tw,
                    tileset.th//th),
                w, h,
                tileset.tw,
                tileset.th,
                int(layer.get('offsetx', 0)),
                int(layer.get('offsety', 0)),
                tileset_index=tileset_index,
                tileset=tileset,
                name=layer.get('name')))
        
        return tilemaps

class TileObject:
    def __init__(self, id_, x, y, w, h, tileset, tile_index, name=None):
        self.x  = x
        self.y  = y
        self.w  = w
        self.h  = h
        self.tileset = tileset
        self.tile_index = tile_index
        self.name = name
        self.id = id_

    def to_ky(self):
        return '{}{}'.format(
            '# {}\n'.format(self.name) if self.name else '',
            list_to_ky([
                tileset_ref_to_ky(self.tileset),
                int_to_ky(self.tile_index),
                px_to_ky(self.x),
                px_to_ky(self.y),
                px_to_ky(self.w),
                px_to_ky(self.h),
                int_to_ky(0),
            ]))

    @staticmethod
    def from_tmx(root, tilesets):
        sprites = []

        for obj_group in root.findall('objectgroup'):
            for obj in obj_group.findall('object'):
                if not obj.get('gid'):
                    continue  # not a Tile

                if obj.get('type') and obj.get('type')[0] != '+':
                    continue  # not a TileObject

                x = int(obj.get('x'))
                y = int(obj.get('y'))
                w = int(obj.get('width'))
                h = int(obj.get('height'))
                i = convert_tile(int(obj.get('gid')))

                assert i != 0

                tileset_index = find_tileset_index(i, tilesets)
                tileset = tilesets[tileset_index]
                start_index = tileset_index + tileset.start_index
                # FIXME: keep id -> index map in tileset since ids aren't always sequential
                tile_index = i - start_index

                sprites.append(TileObject(
                    obj.get('id'),
                    x, y-h, w, h,
                    tileset,
                    tile_index,
                    name=obj.get('name')
                ))

        return sprites
    
class Thing:
    def __init__(self, x, y, w, h, type_, fx, fy, params,
                 name=None, tileobj_index=None, hidden=False):
        self.x  = x
        self.y  = y
        self.w  = w
        self.h  = h
        self.type_ = type_
        self.fx = fx
        self.fy = fy
        self.params = params
        self.name = name
        self.tileobj_index = tileobj_index
        self.hidden = hidden
        
    def to_ky(self):
        params = [
            px_to_ky(self.x), # +(0 if self.fx == 1 else self.w)),
            px_to_ky(self.y), # +(0 if self.fy == 1 else self.h)),
            px_to_ky(self.w),
            px_to_ky(self.h),
            int_to_ky(self.fx),
            int_to_ky(self.fy)
        ]

        if self.tileobj_index is not None:
            params.append(str(self.tileobj_index))

        if self.params:
            params.append(self.params)

        thing_ky = list_to_ky(
            [
                'cls_{}'.format(camel_to_snake(self.type_)),
                as_ky(
                    list_to_ky(params, inline=True),
                    '{}Params'.format(self.type_)
                )
            ],
            inline=True)
            
        return '# {}\n{}'.format(self.name, thing_ky) if self.name else thing_ky

    @staticmethod
    def from_tmx(obj_group, tileobjs=None, names_to_indexes={}):
        props = []

        if obj_group is None:
            return props

        for obj in obj_group.findall('object'):
            #if not obj.get('gid'):
            #    continue  # not a Tile

            if not obj.get('type'):
                continue  # not a Thing

            x  = int(obj.get('x'))
            y  = int(obj.get('y'))
            w  = int(obj.get('width') or 0)
            h  = int(obj.get('height') or 0)
            fx = -1 if (int(obj.get('gid'))&0x80000000) else 1
            fy = -1 if (int(obj.get('gid'))&0x40000000) else 1

            pass_tileobj_ref, type_, params, custom_params = re.match(
                '(\+?)(\w+)(\[[-\d,]+\])?\(([^)]*)\)',
                obj.get('type')
            ).groups()

            def resolve_at_param(param):
                return str(D_index(names_to_indexes, param))

            custom_params = re.sub(
                r'(\@\w+)',
                lambda m: resolve_at_param(m.group(0)[1:]),
                custom_params)

            if pass_tileobj_ref:
                tileobj_index = [
                    t_obj_index for t_obj_index, t_obj in enumerate(tileobjs)
                    if t_obj.id == obj.get('id')
                ][0]
            else:
                tileobj_index = None

            props.append(Thing(
                x, y-h, w, h,
                type_,
                fx,
                fy,
                custom_params,
                tileobj_index=tileobj_index,
                name=obj.get('name'),
                hidden=not bool(int(obj.get('visible') or '1'))
            ))

        return props

class Room:
    def __init__(self,
                 bg_image, bg_color,
                 layers, sprites, props, enemies, players,
                 tilesets,
                 overlay,
                 w, h,
                 screen_w, screen_h,
                 fg_index,
                 location=None,
                 tick_proc=None,
                 filename=None):
        self.bg_image = bg_image
        self.bg_color = bg_color
        self.layers = layers
        self.sprites = sprites
        self.props = props
        self.enemies = enemies
        self.players = players
        self.tilesets = tilesets
        self.overlay = overlay
        self.w = w
        self.h = h
        self.w_in_screens = w // screen_w
        self.h_in_screens = h // screen_h
        self.fg_index = fg_index
        self.location = location
        self.location_in_screens = (location[0]//screen_w, location[1]//screen_h)
        self.tick_proc = tick_proc
        self.filename = filename
        
    def to_ky(self):
        bg_image = image_to_ky(self.bg_image) if self.bg_image is not None else 'null'
        
        layers = list_to_ky([
            layer.to_ky()
            for layer in self.layers])

        sprites = list_to_ky([
            layer.to_ky()
            for layer in self.sprites])
        
        props = list_to_ky([layer.to_ky() for layer in self.props])
        enemies = list_to_ky([layer.to_ky() for layer in self.enemies])
        players = list_to_ky([layer.to_ky() for layer in self.players])
        tilesets = list_to_ky(list(map(tileset_ref_to_ky, self.tilesets.values())))
        
        return list_to_ky([
            '# {}'.format(self.filename),
            self.tick_proc,
            bg_image,
            color_to_ky(self.bg_color or 0),
            layers,
            self.overlay.to_ky() if self.overlay else 'null',
            tilesets,
            'null',
            'null',
            'null',
            'null',
            sprites,
            props,
            enemies,
            players,
            px_to_ky(self.location[0] if self.location else 0),
            px_to_ky(self.location[1] if self.location else 0),
            px_to_ky(self.w),
            px_to_ky(self.h),
            int_to_ky(self.fg_index)            
        ])

    @staticmethod
    def gather_object_names_to_indexes_from_tmx(root, room_index):
        assert root.attrib['renderorder'] == 'left-down'
        assert root.attrib['orientation'] == 'orthogonal'
        assert root.attrib['infinite'] == '0'

        names_to_indexes = OrderedDict()

        index = 0
        obj_group = root.findall('objectgroup')[0]
        for obj in obj_group.findall('object'):
            if not obj.get('type'):
                continue  # not a Thing
            names_to_indexes[obj.get('name')] = (room_index, index)
            index += 1

        return names_to_indexes

    @staticmethod
    def from_tmx(root, source_dir='.', location=None, names_to_indexes={}, filename=None, screen_w=None, screen_h=None):
        assert root.attrib['renderorder'] == 'left-down'
        assert root.attrib['orientation'] == 'orthogonal'
        assert root.attrib['infinite'] == '0'

        try:
            image_layer = root.findall('imagelayer')[0]
            bg_image = image_layer.find('image').attrib['source']
        except (IndexError, KeyError):
            bg_image = None

        bg_color = root.attrib.get('backgroundcolor')
        if bg_color:
            bg_color = int(bg_color[1:], 16)

        # for object_group in root.findall('objectgroup'):
        #     for object_ in object_group.findall('object'):
        #         if object_.attrib['type'] == "campath":
        #             polyline = object_.find('polyline')

        tilesets = Tileset.from_tmx(root, source_dir=source_dir)
        tilemaps = Tilemap.from_tmx(root, tilesets)
        tileobjs = TileObject.from_tmx(root, tilesets)

        w = int(root.get('width')) *int(root.get('tilewidth'))
        h = int(root.get('height'))*int(root.get('tileheight'))

        fg_index = [i for i, tilemap in enumerate(tilemaps)
                    if tilemap.name == 'fg'][0]
        
        overlay_indices = [i for i, tilemap in enumerate(tilemaps) if tilemap.name == 'overlay']
        if overlay_indices:
            overlay = tilemaps.pop(overlay_indices[0])
            new_w_factor = int((w // (overlay.w*overlay.tw))*1.5)
            overlay.upscale_width(new_w_factor)
        else:
            overlay = None
        
        return Room(
            bg_image,
            bg_color,
            tilemaps,
            tileobjs,
            Thing.from_tmx(
                root.find("objectgroup[@name='props']"),
                tileobjs,
                names_to_indexes=names_to_indexes),
            Thing.from_tmx(root.find("objectgroup[@name='enemies']")),
            Thing.from_tmx(root.find("objectgroup[@name='players']")),
            tilesets,
            overlay,
            w, h,
            screen_w, screen_h,
            fg_index,
            location=location,
            tick_proc=find_property(root, 'Tick', 'null'),
            filename=filename)
    
if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description='Build sprite sheets.')
    parser.add_argument('filenames', type=str, nargs='*',
                        help=("name of world/tsx/tmx to convert"))
    parser.add_argument('--prefix-full-path',
                        action='store_true',
                        help="prefix Pinky variables with the full path")
    parser.add_argument('--tileset-ref-prefix',
                        default=DEFAULT_TS_PREFIX,
                        help="prefix for tileset references")
    parser.add_argument('--world-prefix',
                        default=DEFAULT_WORLD_PREFIX,
                        help="prefix for output world ky")
    parser.add_argument('--units', default='',
                        help="units-of-measurement for sprite bounds")
    parser.add_argument('--base-index', default=0, type=int)
    parser.add_argument('--shift-maps')
    parser.add_argument('--out-filename')    

    args = parser.parse_args()

    world = None
    tileset_vars = []
    tilesets = []
    rooms = []

    for source in args.filenames:
        
        # Map moving/renaming functions
        if args.out_filename or args.shift_maps:
            out_filename_template = args.out_filename
            shift_x, shift_y = tuple(map(int, (
                args.shift_maps or '0,0'
            ).split(',')))

            if source.endswith('.world'):
                with open(source, 'rt') as world_file:
                    world = json.load(world_file)

                    # Find map files using regex from world
                    # - use map settings when writing maps to rooms:
                    #   "multiplierX": 320,
                    #   "multiplierY": 192,
                    #   "offsetX": 0,
                    #   "offsetY": 0
                    # - add reference to map attr using syntax like @overworld.offsetX + 0
                    
                    source_dir = os.path.dirname(source)
                    room_filename_re = world['patterns'][0]['regexp'] + '$'
                    room_files = []
                    for f in sorted(os.listdir(source_dir)):
                        room_file = os.path.join(source_dir, f)
                        m = re.match(room_filename_re, f)
                        if not m:
                            continue
                        room_files.append((m, room_file))

                    # Backup the original files for safe keeping, and also to
                    # prevent overwriting files with newly shifted ones,
                    # if file name format has not changed.
                    for _, f in room_files:
                        os.rename(f, f+'.original')

                    # Rename map files
                    for m, f in room_files:
                        x, y = m and tuple(map(int, m.groups()))
                        if not out_filename_template:
                            out_filename_template = '{}'.join((
                                f[:m.start(1)],
                                f[m.end(1):m.start(2)],
                                f[m.end(2):]
                            ))
                        out_filename = out_filename_template.format(
                            x+shift_x, y+shift_y)
                        copyfile(
                            f+'.original',
                            os.path.join(source_dir, out_filename)
                        )

            exit(1)  # FIXME: this stands out as ugly even among the other shit

        if source.endswith('.world'):
            with open(source, 'rt') as world_file:
                world = json.load(world_file)
                source_dir = os.path.dirname(source)

                def find_map_attrs_for_file(f):
                    for pattern in world['patterns']:
                        m = re.match(pattern['regexp']+'$', f)
                        if m:
                            x, y = tuple(map(int, m.groups()))
                            map_attrs = pattern.copy()
                            map_attrs['x'] = x
                            map_attrs['y'] = y
                            return map_attrs

                global_names_to_indexes = OrderedDict()
                room_index = 0
                for f in sorted(os.listdir(source_dir)):
                    if find_map_attrs_for_file(f) is None:
                        continue
                    room_file = os.path.join(source_dir, f)
                    global_names_to_indexes.update(
                        Room.gather_object_names_to_indexes_from_tmx(
                            ET.parse(room_file).getroot(),
                            room_index
                        ),
                    )
                    room_index += 1

                rooms = []
                for f in sorted(os.listdir(source_dir)):
                    map_attrs = find_map_attrs_for_file(f)
                    if map_attrs is None:
                        continue
                    room_file = os.path.join(source_dir, f)
                    screen_w = map_attrs['multiplierX']
                    screen_h = map_attrs['multiplierY']
                    x = map_attrs['x'] * screen_w + map_attrs['offsetX']
                    y = map_attrs['y'] * screen_h + map_attrs['offsetY']
                    rooms.append(Room.from_tmx(
                        ET.parse(room_file).getroot(),
                        source_dir=source_dir,
                        location=(x, y),
                        screen_w=screen_w,
                        screen_h=screen_h,
                        names_to_indexes=global_names_to_indexes,
                        filename=f
                    ))

        elif source.endswith('.tsx'):
            tileset_vars.append(filename_to_var(source))
            tilesets.append(Tileset.from_tsx(
                ET.parse(source).getroot()
            ))
            
        else:
            rooms.append(Room.from_tmx(
                ET.parse(source).getroot(),
                source_dir=os.path.dirname(source),
                filename=source
            ))

    if tilesets:
        print('struct Tilesets\n{}\nend'.format(
            '\n'.join('    {} : Tileset'.format(k) for k in tileset_vars)
        ))
        print(list_to_ky([ts.to_ky() for ts in tilesets]))

    if rooms and not world:
        print(list_to_ky([room.to_ky() for room in rooms]))

    if rooms and world:
        room_map_w = room_map_h = 0
        for i, room in enumerate(rooms):
            if room.location_in_screens is None:
                continue
            room_x, room_y = room.location_in_screens
            room_map_w = max(room_map_w, room_x+room.w_in_screens)
            room_map_h = max(room_map_h, room_y+room.h_in_screens)            

        room_map = [-1] * (room_map_w*room_map_h)
        
        for i, room in enumerate(rooms):
            if room.location_in_screens is None:
                continue
            for x in range(room.w_in_screens):
                for y in range(room.h_in_screens):
                    room_x, room_y = room.location_in_screens
                    room_map[(room_x+x)+(room_y+y)*room_map_w] = i

        # TODO: output in ky format!
        # Then...load the map in legend.ky and use where room change
        # placeholder code is now.
        room_map = '{{{}}}'.format(
            ',\n'.join([
                ','.join(map(
                    str, room_map[i*room_map_w:(i+1)*room_map_w]
                ))
                for i in range(room_map_h)
            ])
        )

        # print the world structure (this should be an object)
        print(args.world_prefix+list_to_ky([
            list_to_ky([room.to_ky() for room in rooms]),
            room_map,
            list_to_ky([list_to_ky(map(int_to_ky, link), inline=True)
                        for link in global_names_to_indexes.values()]),
            int_to_ky(room_map_w),
            int_to_ky(room_map_h),
        ]))
