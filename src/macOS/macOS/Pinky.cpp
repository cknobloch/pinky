//
//  Pinky.cpp
//  macOS
//
//  Created by Curt Knobloch on 10/18/16.
//  Copyright © 2016 Curt Knobloch. All rights reserved.
//

#include "Pinky.hpp"

#include "base64.h"



KyInt _BIT_POS[37] = {0, 0, 1, 26, 2, 23, 27, 0, 3, 16, 24, 30, 28, 11, 0, 13, 4, 7, 17, 0, 25, 22, 31, 15, 29, 10, 12, 6, 0, 21, 14, 9, 5, 20, 8, 19, 18};

KyInt ffb(KyInt x) {
    return _BIT_POS[(x&-x)%37];
}

KyInt min(KyInt a, KyInt b) { return MIN(a, b); }
KyInt max(KyInt a, KyInt b) { return MAX(a, b); }

KyBool isDigit(KyChar c) { return c >= '0' && c <= '9'; }
KyBool isUpper(KyChar c) { return c >= 'A' && c <= 'Z'; }
KyBool isLower(KyChar c) { return c >= 'a' && c <= 'z'; }
KyBool isAlpha(KyChar c) { return isUpper(c) || isLower(c); }

KyInt countLines(const KyChar* text) {
    KyInt n = 0, textLength = (KyInt)strlen(text);
    if (text[0] == 0) { return 0; }
    for (KyInt i = 0; i < textLength; i++) {
        if (text[i] == '\n') {
            n += 1;
        }
    }
    return n + 1;
}

KyInt endOfLine(const KyChar* text, KyInt i) {
    KyInt textLength = (KyInt)strlen(text);
    for (; i < textLength && text[i] != '\n'; i++) {}
    return i;
}

KyInt unescapeText(KyChar* text) {
    KyInt  i = 0, j = 0;
    KyChar digits[12], c;
    while ((c=text[i]) != 0) {
        i++;
        if (c == '\\') {
            c = text[i];
            // TODO: support hex with xtoi()
            if (isDigit(c)) {
                KyInt di = 0;
                while (isDigit(text[i])) { digits[di++] = text[i++]; }
                digits[di] = 0;
                c = atoi(digits);
            } else {
                switch (c) {
                case 'n': c = '\n'; break;
                case 't': c = '\t'; break;
                }
                i++;
            }
        }
        text[j++] = c;
    }
    return j;
}

KyLong currentTime() {
    struct timeval te;
    gettimeofday(&te, NULL);
    return te.tv_sec*1000LL + te.tv_usec/1000;
}

//
// LIST CLASS
//

class KyBlob {
public:
    KyByte* data;
    KyInt   length;
    
    KyBlob(KyInt length=0) {
        this->data = (KyByte*)malloc(this->length=length);
    }
    
    ~KyBlob() {
        if (data) {
            free(data);
        }
    }
    
    KyInt   getInt  (KyInt i) { return *(KyInt*)(data+i); }
    KyShort getShort(KyInt i) { return *(KyShort*)(data+i); }
    KyByte  getByte (KyInt i) { return data[i]; }
    
    void setInt  (KyInt x, KyInt i)   { set(&x, 4, i); }
    void setShort(KyShort x, KyInt i) { set(&x, 2, i); }
    void setByte (KyByte x, KyInt i)  { set(&x, 1, i); }
 
    void appendInt  (KyInt x)   { extend(&x, 4); }
    void appendShort(KyShort x) { extend(&x, 2); }
    void appendByte (KyByte x)  { extend(&x, 1); }
    
    void extend(const void* data, KyInt n) {
        setLength(length+n);
        set(data, n, length-n);
    }
    
    void set(const void* data, KyInt n, KyInt off=0) {
        memcpy(this->data+off, data, n);
    }
    
    void setLength(KyInt length) {
        data = (KyByte*)realloc(data, this->length=length);
    }
    
    void* returnData() {
        void* retData = data;
        data = NULL;
        return retData;
    }
};

//
// VM
//

void Ky_assert(KyBool x) {
    if (!x) {
        throw KyAssertionError();
    }
}

// KyVMObject

KyVMObject::KyVMObject() {
    rc = 0;
}

KyVMObject::~KyVMObject() {}

KyBool KyVMObject::is(KyVMObject* other) {
    return this == other;
}

KyVMObject* KyVMObject::assign(KyVMObject* other) {
    rc++;
    other->release();
    return this;
}

KyVMObject* KyVMObject::ref() {
    rc++;
    return this;
}

void KyVMObject::release() {
    rc--;
    if (!rc) {
        free();
        delete this;
    }
}

void KyVMObject::free() {
    ;
}

// KyVMNull

KyVMObject* KyVMNull::ref() {
    return this;
}

void KyVMNull::release() {
}

// KyVMInt

KyVMInt::KyVMInt(KyInt value) {
    this->value = value;
}

KyVMInt* KyVMInt::cmp(KyVMInt* other) {
    if (this->value < other->value) {
        return kyVMNegOne;
    } else if (this->value > other->value) {
        return kyVMOne;
    } else {
        return kyVMZero;
    }
}

KyBool KyVMInt::isZero() {
    return !this->value;
}

KyVMInt* KyVMInt::fromCFloat(KyFloat v) {
    return new KyVMInt((KyInt)(v*0x10000));
}

KyFloat KyVMInt::toCFloat() {
    return (KyFloat)this->value / 0x10000;
}

KyInt KyVMInt::toCInt() {
    return this->value>>16;
}

// KyVMStr

KyVMStr::KyVMStr(KyChar* str) {
    this->str = str;
    this->len = (KyInt)strlen(str);
}

KyVMStr::KyVMStr(KyInt capacity) {
    this->str = (KyChar*)calloc(capacity+1, sizeof(KyChar));
    this->len = capacity;
}

void KyVMStr::free() {
    ::free(this->str);
}

KyVMStr* KyVMStr::slice(KyInt i, KyInt len) {
    KyVMStr* newStr = new KyVMStr(len);
    memcpy(newStr->str, str+i, sizeof(KyChar)*len);
    return newStr;
}

KyVMConstStr::KyVMConstStr(const KyChar* str) 
: KyVMStr((KyChar*)str) {
}

void KyVMConstStr::free() {
}

// KyVMArray

KyVMArray::KyVMArray(KyVMObject** data, KyInt len) {
    this->data = data; 
    this->len  = len;
}

KyVMArray::KyVMArray(KyInt len) 
: KyVMArray((KyVMObject**)malloc(len*sizeof(KyVMObject*)), len) {
    for (KyInt i = 0; i < len; i++) {
        data[i] = kyVMNull;
    }
}
   
KyVMArray* KyVMArray::create(KyInt len, KyVMObject** initialData) {
    KyVMArray* array = new KyVMArray((KyVMObject**)malloc(len*sizeof(KyVMObject*)), len);
    
    if (initialData == NULL) {
        for (KyInt i = 0; i < len; i++) {
            array->data[i] = kyVMNull;
        }
    } else {
        for (KyInt i = 0; i < len; i++) {
            array->data[i] = initialData[i]->ref();
        }
    }
    
    return array;
}

void KyVMArray::free() {
    while (len) {
        this->data[--len]->release();
    }
    ::free(this->data);
}

KyVMObject* KyVMArray::set(KyInt i, KyVMObject* value) {
    Ky_assert(i >= 0 && i < this->len);
    return this->data[i] = value->assign(this->data[i]);
}

KyVMObject* KyVMArray::get(KyInt i) {
    Ky_assert(i >= 0 && i < this->len);
    return this->data[i];
}

KyInt KyVMArray::setInt(KyInt i, KyInt value) {
    return ((KyVMInt*)set(i, new KyVMInt(value)))->value;
}

KyInt KyVMArray::getInt(KyInt i) {
    return ((KyVMInt*)get(i))->value;
}

KyVMArray* KyVMArray::sub(KyInt i) {
    return ((KyVMArray*)get(i));
}

KyVMArray* KyVMArray::slice(KyInt i, KyInt len) {
    // NOTE: this could be easily optimized by adding a ArraySlice wrapper
    //       class that reads from the existing array and only duplicates
    //       the data if a write is performed.
    return create(len, this->data+i);
}

KyVMArray* KyVMArray::reverse() {
    KyVMArray* array = create(len);
    for (KyInt i = 0; i < len; i++) {
        array->set(i, data[len-i-1]);
    }
    return array;
}

void KyVMArray::fill(KyVMObject* x, KyInt start, KyInt len) {
    for (KyInt i = 0; i < len; i++) { set(start+i, x); }
}

void KyVMArray::copy(KyVMObject** data, KyInt start, KyInt len) {
    for (KyInt i = 0; i < len; i++) { set(start+i, data[i]); }
}

KyBool KyVMArray::is(KyVMObject* other) {
    return (
        this->data == ((KyVMArray*)other)->data && 
        this->len == ((KyVMArray*)other)->len
    );
}

// KyVMWeakArray

KyVMWeakArray::KyVMWeakArray(const KyVMArray* array) 
    : KyVMArray(array->data, array->len) {
}

KyVMObject* KyVMWeakArray::ref() {
    return this;
}

void KyVMWeakArray::release() {
}
                
// KyVMBlob

KyVMBlob::KyVMBlob(KyInt n) {
    this->data = (KyByte*)malloc(n);
    this->len  = n;
}

void KyVMBlob::free() {
    ::free(data);
}

// KyVMProc

KyVMProc::KyVMProc(KyInt addr, KyInt vn) {
    this->addr = addr;
    this->vn   = vn;
    this->p    = 0;
}

// KyVMFrame

KyVMFrame::~KyVMFrame() {
    free();
}

void KyVMFrame::reset(KyInt ip, KyVMObject** vars, KyInt vn, KyVMObject** args, KyInt argc) {
    this->vars       = vars;
    this->ip         = ip;
    this->vn         = vn;
    this->proc       = args != NULL ? (KyVMProc*)args[0] : NULL;
    this->lineNo     = 0;
    this->isTopLevel = FALSE;
    
    KyInt i = 0;
    
    for (; i < argc; i++) { this->vars[i] = args[i]->ref(); }
    for (; i < vn;   i++) { this->vars[i] = kyVMNull; }
}

void KyVMFrame::resetWithProc(KyInt ip, KyVMObject** vars, KyInt vn, KyVMObject** args, KyInt argc, KyVMProc* proc) {
    this->vars       = vars;
    this->ip         = ip;
    this->vn         = vn;
    this->proc       = proc;
    this->lineNo     = 0;
    this->isTopLevel = FALSE;
    
    KyInt i = 0;
    
    this->vars[i++] = proc->ref();
    for (; i <=argc; i++) { this->vars[i] = args[i-1]->ref(); }
    for (; i < vn;   i++) { this->vars[i] = kyVMNull; }
}


KyVMObject* KyVMFrame::setVar(KyInt i, KyVMObject* value) {
    Ky_assert(i >= 0 && i < this->vn);
    KyVMObject** x = this->vars + i;
    return *x=value->assign(*x);
}

KyVMObject* KyVMFrame::getVar(KyInt i) {
    Ky_assert(i >= 0 && i < this->vn);
    return this->vars[i];
}

void KyVMFrame::free() {
    while (vn) {
        this->vars[--vn]->release();
    }
}

// VM constants

KyVMNull* kyVMNull     = (KyVMNull*)(new KyVMNull())->ref();
KyVMInt*  kyVMZero     = (KyVMInt*) (new KyVMInt( 0))->ref();
KyVMInt*  kyVMOne      = (KyVMInt*) (new KyVMInt( 1))->ref();
KyVMInt*  kyVMNegOne   = (KyVMInt*) (new KyVMInt(-1))->ref();
KyVMInt*  kyVMMinInt   = (KyVMInt*) (new KyVMInt(MIN_INT))->ref();
KyVMInt*  kyVMMaxInt   = (KyVMInt*) (new KyVMInt(MAX_INT))->ref();
KyVMStr*  kyVMEmptyStr = (KyVMStr*) (new KyVMConstStr(""))->ref();

KyVMInt* Ky_toVMBool(KyBool x) { 
    return x ? kyVMOne : kyVMZero; 
}

KyVMObject* Ky_nullable(KyVMObject* x) {
    return x ? x : kyVMNull;
}

// KyVM

struct KyVMObjectSortContext {
    KyVM* vm;
    KyVMProc* cmp;
};

int ky_cmpVMObjects(void* arg, const void* a, const void* b) {
    KyVMObjectSortContext* ctx = (KyVMObjectSortContext*)arg;
    const KyVMInt* ab[] = {*(KyVMInt**)a, *(KyVMInt**)b};
    KyVMInt* x = (KyVMInt*)ctx->vm->call(ctx->cmp, (KyVMObject**)ab, 2);
    KyInt retValue = x->value;
    x->release();
    return retValue;
}

KyVM::KyVM() {
    this->osi = 0;
    this->fsi = 0;
    this->delegate = NULL;
    this->code = NULL;
    this->cmdOffsets = NULL;
    this->fileOffsets[0] = 0x80000000;
    this->globalCount = 0;
    this->startTime = currentTime();
}

KyVM::~KyVM() {
    cleanup();
    
    if (code) {
        ::free(code);
    }
    
    if (cmdOffsets) {
        ::free(cmdOffsets);
    }
}

void KyVM::setDelegate(KyVMDelegate* delegate) {
    this->delegate = delegate;
}

void KyVM::run() {
    KyVMProc* mainEntryProc = (KyVMProc*)(new KyVMProc(0, globalCount))->ref();
    // this voidCall has been inlined because of the way vars is inherited
    // from the previous frame
    // voidCall(mainEntryProc, NULL, 0);
    fs[fsi++].resetWithProc(mainEntryProc->addr, gv, mainEntryProc->vn, NULL, 0, mainEntryProc);
    topFrame()->isTopLevel = TRUE;
    evalWhileFrame();
    mainEntryProc->release();
}

void KyVM::voidCall(KyVMProc* proc, KyVMObject** args, KyInt argc) {
    pushFrameWithProc(proc->addr, proc->vn, args, argc, proc);
    topFrame()->isTopLevel = TRUE;
    evalWhileFrame();
}

KyVMObject* KyVM::call(KyVMProc* proc, KyVMObject** args, KyInt argc) {
    voidCall(proc, args, argc);
    return pop();
}

void KyVM::resume() {
    topFrame()->ip++;
    evalWhileFrame();
}

void KyVM::evalWhileFrame() {
    Ky_assert(this->osi==0);  // Ensure that stack stays balanced!
    
    const KyByte* code = this->code;
    const KyInt* cmdOffsets = this->cmdOffsets;
    
    try {
        while (1) {
            KyVMFrame* curFrame = topFrame();
            KyInt ci = cmdOffsets[curFrame->ip];
            KyInt opcode = code[ci++];
            
            switch (opcode) {
                case VM_NOP: {
                    break;
                }
                case VM_NULL: {
                    push(kyVMNull);
                    break;
                }
                case VM_ZERO: {
                    push(kyVMZero);
                    break;
                }
                case VM_ONE: {
                    push(kyVMOne);
                    break;
                }
                case VM_LODI: {
                    push(new KyVMInt(*(KyInt*)(code+ci)));
                    break;
                }
                case VM_LODS: {
                    KyChar* str = (KyChar*)(code+ci); 
                    push((*str)?(new KyVMConstStr(str)):kyVMEmptyStr);
                    break;
                }
                case VM_LODF: {
                    push(new KyVMProc(*(KyInt*)(code+ci), *(KyInt*)(code+ci+4)));
                    break;
                }
                case VM_ADD: {
                    KyVMInt *b = popInt(), *a = popInt();
                    push(new KyVMInt(a->value+b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_SUB: {
                    KyVMInt *b = popInt(), *a = popInt();
                    push(new KyVMInt(a->value-b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_MUL: {
                    KyVMInt *b = popInt(), *a = popInt();
                    push(new KyVMInt(a->value*b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_DIV: {
                    KyVMInt *b = popInt(), *a = popInt();
                    push(new KyVMInt(a->value/b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_MULF: {
                    KyVMInt *b = popInt(), *a = popInt();
                    push(new KyVMInt((KyInt)(((KyLong)a->value*b->value)>>16)));
                    a->release();
                    b->release();
                    break;
                }
                case VM_DIVF: {
                    KyVMInt *b = popInt(), *a = popInt();
                    push(new KyVMInt((KyInt)(((KyLong)a->value<<16)/b->value)));
                    a->release();
                    b->release();
                    break;
                }
                case VM_NOT: {
                    push((popInt())->isZero()?kyVMOne:kyVMZero);
                    break;
                }
                case VM_NEG: {
                    KyVMInt *a = popInt();
                    push(new KyVMInt(-(a->value)));
                    a->release();
                    break;
                }
                case VM_LT: {
                    KyVMInt *b = popInt(), *a = popInt();
                    push(Ky_toVMBool(a->value< b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_GT: {
                    KyVMInt *b = popInt(), *a = popInt();
                    push(Ky_toVMBool(a->value> b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_LTE: {
                    KyVMInt *b = popInt(), *a = popInt();
                    push(Ky_toVMBool(a->value<=b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_GTE: {
                    KyVMInt *b = popInt(), *a = popInt();
                    push(Ky_toVMBool(a->value>=b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_EQ: {
                    KyVMInt *b = popInt(), *a = popInt();
                    push(Ky_toVMBool(a->value==b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_NE: {
                    KyVMInt *b = popInt(), *a = popInt();
                    push(Ky_toVMBool(a->value!=b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_JMP: {
                    topFrame()->ip = *(KyInt*)(code+ci);
                    continue;
                }
                case VM_JIF: {
                    if (!(popInt())->isZero()) {
                        topFrame()->ip = *(KyInt*)(code+ci);
                        continue;
                    }
                    break;
                }
                case VM_JZ: {
                    if ( (popInt())->isZero()) {
                        topFrame()->ip = *(KyInt*)(code+ci);
                        continue;
                    }
                    break;
                }
                case VM_JSR: {
                    KyInt        argc = code[ci] + 1;
                    KyVMObject** args = this->os + (this->osi-=argc);
                    KyVMProc*    proc = (KyVMProc*)args[0];
                    proc->p++;
                    pushFrame(proc->addr, proc->vn, args, argc);
                    KyVMObject::releaseMany(args, argc);
                    topFrame()->ip = proc->addr;
                    continue;
                }
                case VM_RET: {
                    if (dropFrame()) {
                        throw KyEscapeException();
                    }
                    break;
                }
                case VM_DUP: {
                    push(this->os[this->osi-1]);
                    break;
                }
                case VM_DROP: {
                    drop();
                    break;
                }
                case VM_SETA: {
                    KyVMObject* v = pop();
                    KyVMArray* x = popArray();
                    x->set(code[ci], v);
                    x->release();
                    v->release();
                    break;
                }
                case VM_SETI: {
                    KyVMObject* v = pop();
                    KyVMInt* i = popInt();
                    KyVMArray* x = popArray();
                    x->set(i->value, v);
                    x->release();
                    i->release();
                    v->release();
                    break;
                }
                case VM_SETL: {
                    curFrame->setVar(code[ci], pop())->release();
                    break;
                }
                case VM_SETG: {
                    this->fs[0].setVar(*(KyShort*)(code+ci), pop())->release();
                    break;
                }
                case VM_GETA: {
                    KyVMArray* x = popArray();
                    push(x->get(code[ci]));
                    x->release();
                    break;
                }
                case VM_GETI: {
                    KyVMInt* i = popInt();
                    KyVMArray* x = popArray();
                    push(x->get(i->value));
                    x->release();
                    i->release();
                    break;
                }
                case VM_GETL: {
                    push(curFrame->getVar(code[ci]));
                    break;
                }
                case VM_GETG: {
                    push(this->fs[0].getVar(*(KyShort*)(code+ci)));
                    break;
                }
                case VM_NEW: {
                    push(KyVMArray::create(*(KyInt*)(code+ci)));
                    break;
                }
                case VM_NEWV: {
                    KyInt len  = *(KyInt*)(code+ci);
                    this->osi -= len;
                    KyVMArray* array = KyVMArray::create(len, os+osi);
                    KyVMObject::releaseMany(os+osi, len);
                    push(array);
                    break;
                }
                case VM_REM: {
                    curFrame->lineNo = *(KyInt*)(code+ci);
                    break;
                }
                case VM_EVAL: {
                    try {
                        push(this->delegate->eval(this, 
                                                  *(KyInt*)(code+ci), 
                                                  curFrame->vars+1,
                                                  curFrame->vn));
                    } catch(const KyInterrupt& e) {
                        dropFrame();
                        throw;
                    }
                    if (dropFrame()) {
                        throw KyEscapeException();
                    }
                    break;
                }
                case VM_MARK: {
                    KyVMFrame* f = topFrame();
                    f->proc->addr = f->ip + code[ci] + 1;
                    break;
                }
                case VM_STEP: {
                    KyVMFrame* f = topFrame();
                    push(new KyVMInt(f->proc->p));
                    break;
                }
                case VM_CLR: {
                    KyVMFrame* f = topFrame();
                    f->proc->addr = f->ip + 1;
                    f->proc->p = 1;
                    break;
                }
                case VM_ARGS: {   
                    KyVMArray* args = popArray();
                    for (KyInt i = 0; i < args->len; i++) {
                        push(args->get(i));
                    }
                    args->release();
                    break;
                }
                    
                    //
                    // Core
                    //
                case VM_ASSERT: {
                    KyVMInt* x = popInt();
                    KyInt xIsZero = x->isZero(); 
                    x->release();
                    Ky_assert(!xIsZero);
                    break;
                }
                case VM_PRINT: {
                    KyVMStr* x = popStr();
                    printf("%s\n", x->str);
                    x->release();
                    break;
                }
                case VM_TIME: {
                    push(new KyVMInt((KyInt)(currentTime()-startTime)));
                    break;
                }
                case VM_RAND: {
                    KyVMInt* end = popInt(), *start = popInt();
                    this->seed = ((KyUInt)this->seed*9301+49297) % 233280;
                    push(new KyVMInt((start->value)+(KyInt)((KyUInt)this->seed*((end->value)-(start->value))/233280)));
                    start->release();
                    end->release();
                    break;
                }
                case VM_SEED: {
                    KyVMInt* seed = popInt();
                    this->seed = seed->value;
                    seed->release();
                    break;
                }
                case VM_WEAK: {
                    KyVMArray* x = popArray();
                    push(new KyVMWeakArray(x));
                    x->release();
                    break;
                }
                case VM_IS: {
                    KyVMObject* b = pop(), *a = pop();
                    push(Ky_toVMBool(a->is(b)));
                    a->release();
                    b->release();
                    break;
                }
                case VM_DEFAULT: {
                    KyVMObject* b = pop(), *a = pop();
                    push(a==kyVMNull ? b : a);
                    a->release();
                    b->release();
                    break;
                }
                case VM_SELECT: {
                    KyVMObject* b = pop(), *a = pop();
                    KyVMInt* x = popInt();
                    push(x->isZero() ? b : a);
                    x->release();
                    a->release();
                    b->release();
                    break;
                }
                case VM_SLEEP: {
                    KyVMInt* t = popInt();
                    KyInt tValue = t->value;
                    t->release();
                    throw KySleep(tValue);
                }
                case VM_MACRO: {
                    KyVMProc* proc = (KyVMProc*)pop();
                    push(new KyVMProc(proc->addr, proc->vn));
                    proc->release();
                    break;
                }
                case VM_MACROSTEP: {
                    KyVMProc* proc = (KyVMProc*)pop();
                    push(new KyVMInt(proc->p));
                    proc->release();
                    break;
                }
                case VM_GETTYPE: {
                    KyVMObject* x = pop();
                    push(new KyVMInt(x->typeID()));
                    x->release();
                    break;
                }
                case VM_CLONE: {
                    KyVMArray* x = popArray();
                    push(x->slice(0, x->len));
                    x->release();
                    break;
                }
                case VM_ATTRS: {
                    break;
                }
                    
                    //
                    // Integer
                    //
                case VM_AND: {
                    KyVMInt* b = popInt(), *a = popInt();
                    push(new KyVMInt(a->value &b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_OR: {
                    KyVMInt* b = popInt(), *a = popInt();
                    push(new KyVMInt(a->value |b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_XOR: {
                    KyVMInt* b = popInt(), *a = popInt();
                    push(new KyVMInt(a->value ^b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_SHL: {
                    KyVMInt* b = popInt(), *a = popInt();
                    push(new KyVMInt(a->value<<b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_SHR: {
                    KyVMInt* b = popInt(), *a = popInt();
                    push(new KyVMInt(a->value>>b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_ROT: {
                    KyVMInt* n = popInt();
                    KyVMInt* i = popInt();
                    KyVMInt* x = popInt();
                    KyInt xVal = x->value, iVal = i->value, nVal = n->value;
                    push(new KyVMInt(((xVal&((1<<nVal)-1))>>iVal)|(xVal<<(nVal-iVal)&((1<<nVal)-1))));
                    x->release();
                    i->release();
                    n->release();
                    break;
                }
                case VM_BITS: {
                    KyVMInt* m = popInt();
                    KyVMInt* x = popInt();
                    push(new KyVMInt((x->value&m->value)>>ffb(m->value)));
                    x->release();
                    m->release();            
                    break;
                }
                case VM_SETBITS: {
                    KyVMInt* y = popInt();
                    KyVMInt* m = popInt();
                    KyVMInt* x = popInt();
                    push(new KyVMInt((x->value&~m->value)|(y->value<<ffb(m->value))));
                    x->release();
                    m->release();
                    y->release();
                    break;
                }
                case VM_SIGNED: {
                    // This is a no-op since ints are always already signed
                    // KyVMInt* x = popInt();
                    // push(new KyVMInt(x->value));
                    // x->release();
                    break;
                }
                case VM_MOD:
                case VM_FMOD: {
                    KyVMInt* b = popInt(), *a = popInt();
                    push(new KyVMInt(((a->value%b->value)+b->value)%b->value));
                    a->release();
                    b->release();
                    break;
                }
                case VM_MIN:
                case VM_FMIN: {
                    KyVMInt* b = popInt(), *a = popInt();
                    push(new KyVMInt(min(a->value, b->value)));
                    a->release();
                    b->release();
                    break;
                }
                case VM_MAX:
                case VM_FMAX: {
                    KyVMInt* b = popInt(), *a = popInt();
                    push(new KyVMInt(max(a->value, b->value)));
                    a->release();
                    b->release();
                    break;
                }
                case VM_ABS:               
                case VM_FABS: {
                    KyVMInt* x = popInt();
                    push(new KyVMInt(abs(x->value)));
                    x->release();
                    break;
                }
                case VM_ICMP:
                case VM_FCMP: {
                    KyVMInt* b = popInt(), *a = popInt();
                    push(a->cmp(((KyVMInt*)b)));
                    a->release();
                    b->release();
                    break;
                }
                case VM_ITOS: {
                    KyVMInt* x = popInt();
                    KyVMStr* newStr = new KyVMStr(11);
                    newStr->len = sprintf(newStr->str, "%d", x->value);
                    push(newStr);
                    x->release();
                    break;
                }
                    
                    //
                    // Fixed-point
                    //
                case VM_FTOS: {
                    KyVMInt* x = popInt();
                    KyFloat xVal = x->toCFloat();
                    KyVMStr* newStr = new KyVMStr(19);
                    if (xVal == (KyInt)xVal) {
                        newStr->len = sprintf(newStr->str, "%i.0", (KyInt)xVal);
                    } else {
                        KyInt i = sprintf(newStr->str, "%f", xVal);
                        while (newStr->str[--i] == '0');
                        newStr->len = i + 1;
                        newStr->str[newStr->len] = 0;
                    }
                    push(newStr);
                    x->release();
                    break;
                }
                case VM_FTOI: {
                    KyVMInt* x = popInt();
                    push(new KyVMInt(((x->value>>16))));
                    x->release();
                    break;
                }
                case VM_ITOF: {
                    KyVMInt* b = popInt(), *a = popInt();
                    push(new KyVMInt((a->value<<16|(b->value&0xFFFF))));
                    a->release();
                    b->release();
                    break;
                }
                case VM_FLOOR: {
                    KyVMInt* x = popInt();
                    push(new KyVMInt((x->value)&~0xFFFF));
                    x->release();
                    break;
                }
                case VM_CEIL: {
                    KyVMInt* x = popInt();
                    push(new KyVMInt((x->value+0xFFFF)&~0xFFFF));
                    x->release();
                    break;
                }
                case VM_FRAC: {
                    KyVMInt* x = popInt();
                    push(new KyVMInt((x->value)& 0xFFFF));
                    x->release();
                    break;
                }
                case VM_POW: {
                    KyVMInt* b = popInt(), *a = popInt();
                    push(KyVMInt::fromCFloat(pow  (a->toCFloat(), b->toCFloat())));
                    a->release();
                    b->release();
                    break;
                }
                case VM_SQRT: {
                    KyVMInt* x = popInt();
                    push(KyVMInt::fromCFloat(sqrt (x->toCFloat())));
                    x->release();
                    break;
                }
                case VM_HYPOT: {
                    KyVMInt* b = popInt(), *a = popInt();
                    push(KyVMInt::fromCFloat(hypot(a->toCFloat(), b->toCFloat())));
                    a->release();
                    b->release();
                    break;
                }
                case VM_SIN: {
                    KyVMInt* x = popInt();
                    push(KyVMInt::fromCFloat(sin (x->toCFloat())));
                    x->release();
                    break;
                }
                case VM_COS: {
                    KyVMInt* x = popInt();
                    push(KyVMInt::fromCFloat(cos (x->toCFloat())));
                    x->release();
                    break;
                }
                case VM_TAN: {
                    KyVMInt* x = popInt();
                    push(KyVMInt::fromCFloat(tan (x->toCFloat())));
                    x->release();
                    break;
                }
                case VM_ASIN: {
                    KyVMInt* x = popInt();
                    push(KyVMInt::fromCFloat(asin(x->toCFloat())));
                    x->release();
                    break;
                }
                case VM_ACOS: {
                    KyVMInt* x = popInt();
                    push(KyVMInt::fromCFloat(acos(x->toCFloat())));
                    x->release();
                    break;
                }
                case VM_ATAN: {
                    KyVMInt* x = popInt();
                    push(KyVMInt::fromCFloat(atan(x->toCFloat())));
                    x->release();
                    break;
                }
                    
                    //
                    // String
                    //
                case VM_CONCAT: {
                    KyVMStr* b = popStr(), *a = popStr(); 
                    KyVMStr* c = new KyVMStr(a->len+b->len);
                    memcpy(c->str, a->str, sizeof(KyChar)*a->len);
                    memcpy(c->str+a->len, b->str, sizeof(KyChar)*b->len);
                    push(c);
                    a->release();
                    b->release();
                    break;
                }
                case VM_CHR: {
                    KyVMInt* x = popInt();
                    KyVMStr* newStr = new KyVMStr(1);
                    newStr->str[0] = x->value;
                    push(newStr);
                    x->release();
                    break;
                }
                case VM_ORD: {
                    KyVMInt* i = popInt();
                    KyVMStr* x = popStr();
                    push(new KyVMInt(x->str[i->value]));
                    x->release();
                    i->release();
                    break;
                }
                case VM_SLEN: {
                    KyVMStr* x = popStr();
                    push(new KyVMInt(x->len));
                    x->release();
                    break;
                }
                case VM_SCMP: {
                    KyVMStr* b = popStr();
                    KyVMStr* a = popStr();
                    KyInt    d = a->len - b->len;
                    push(new KyVMInt(d?d:(KyInt)memcmp(a->str, b->str, sizeof(KyChar)*a->len)));
                    a->release();
                    b->release();
                    break;
                }
                case VM_STOI: {
                    KyVMStr* x = popStr();
                    push(new KyVMInt(atoi(x->str)));
                    x->release();
                    break;
                }
                case VM_DECODEB64: {
                    KyVMStr*  b64str = popStr();
                    KyVMBlob* blob   = new KyVMBlob(Base64decode_len(b64str->str));
                    blob->len = Base64decode((KyChar*)blob->data, b64str->str);
                    push(blob);
                    b64str->release();
                    break;
                }
                case VM_B64: {
                    KyVMBlob* blob   = popBlob();
                    KyVMStr*  b64str = new KyVMStr(Base64encode_len(blob->len));
                    b64str->len = Base64encode(b64str->str, (KyChar*)blob->data, blob->len) - 1;
                    push(b64str);
                    blob->release();
                    break;
                }
                case VM_SUBSTR: {
                    KyVMInt* n = popInt();
                    KyVMInt* i = popInt();
                    KyVMStr* x = popStr();
                    push(x->slice(i->value, n->value));
                    x->release();
                    i->release();
                    n->release();
                    break;
                }
                    
                    //
                    // Array
                    //
                case VM_SLICE: {
                    KyVMInt*   n = popInt();
                    KyVMInt*   i = popInt();
                    KyVMArray* x = popArray();
                    push(x->slice(i->value, n->value));
                    x->release();
                    i->release();
                    n->release();
                    break;
                }
                case VM_REVERSE: {
                    KyVMArray*  x = popArray();
                    push(x->reverse());
                    x->release();
                    break;
                }
                case VM_FILL: {
                    KyVMInt*    n = popInt();
                    KyVMInt*    i = popInt();
                    KyVMObject* y = pop();
                    KyVMArray*  x = popArray();
                    x->fill(y, i->value, n->value);
                    x->release();
                    y->release();
                    i->release();
                    n->release();
                    break;
                }
                case VM_COPY: {
                    KyVMInt*   n = popInt();
                    KyVMInt*   i = popInt();
                    KyVMArray* y = popArray();
                    KyVMArray* x = popArray();
                    x->copy(y->data, i->value, n->value);
                    break;
                }
                case VM_SORT: {
                    KyVMProc*  f = (KyVMProc*)pop();
                    KyVMArray* x = popArray();
                    KyVMObjectSortContext ctx = {this, f};
                    qsort_r(x->data,
                            x->len,
                            sizeof(KyVMObject*),
                            &ctx,
                            ky_cmpVMObjects);
                    x->release();
                    f->release();
                    break;
                }
                case VM_APPLY: {
                    KyVMProc*  f = (KyVMProc*)pop();
                    KyVMArray* x = popArray();
                    KyVMObject* params[16];
                    KyInt numObjs = x->len;
                    KyVMObject** objs = x->data;
                    for (KyInt i = 0; i < numObjs; i++) {
                        params[0] = objs[i];
                        this->voidCall(f, params, 1);
                    }
                    x->release();
                    f->release();
                    break;
                }
                case VM_ALEN: {
                    KyVMArray* x = popArray();
                    push(new KyVMInt(x->len));
                    x->release();
                    break;
                }
                case VM_ARRAY: {
                    KyVMInt* n = popInt();
                    push(KyVMArray::create(n->value));
                    n->release();
                    break;
                }
                    
                    //
                    // Blob
                    //
                case VM_BLOB: {
                    KyVMInt* n = popInt();
                    push(new KyVMBlob(n->value));
                    n->release();
                    break;
                }
                case VM_BLOAD: {
                    KyVMStr* n = popStr();
                    FILE* f = fopen(n->str, "rb");
                    if (f) {
                        fseek(f, 0, SEEK_END);
                        KyVMBlob* blob = new KyVMBlob((KyInt)ftell(f));
                        fseek(f, 0, SEEK_SET);
                        fread(blob->data, sizeof(KyByte), blob->len, f);
                        fclose(f);
                        push(blob);
                    } else {
                        push(kyVMNull);
                    }
                    n->release();
                    break;
                }
                case VM_BSAVE: {
                    KyVMStr* x = popStr();
                    KyVMBlob* blob = (KyVMBlob*)pop();
                    FILE* f = fopen(x->str, "wb");
                    fwrite(blob->data, sizeof(KyByte), blob->len, f);
                    fclose(f);
                    blob->release();
                    x->release();
                    break;
                }
                case VM_BLEN: {
                    KyVMBlob* blob = (KyVMBlob*)pop();
                    push(new KyVMInt(blob->len));
                    blob->release();
                    break;
                }
                case VM_GET: {
                    KyVMInt* i = popInt();
                    KyVMBlob* blob = (KyVMBlob*)pop();
                    push(new KyVMInt(blob->data[i->value]));
                    blob->release();
                    i->release();
                    break;
                }
                case VM_SET: {
                    KyVMInt* y = popInt();
                    KyVMInt* i = popInt();
                    KyVMBlob* blob = (KyVMBlob*)pop();
                    blob->data[i->value] = y->value;
                    blob->release();
                    i->release();
                    y->release();
                    break;
                }
                case VM_GETS: {
                    KyVMInt* n = popInt();
                    KyVMInt* i = popInt();
                    KyVMBlob* blob = (KyVMBlob*)pop();
                    KyVMStr*  str  = new KyVMStr(n->value);
                    memcpy(str->str, (const KyChar*)(blob->data+(i->value)), n->value*sizeof(KyChar));
                    push(str);
                    blob->release();
                    i->release();
                    n->release();
                    break;
                }
                case VM_SETS: {
                    KyVMInt* n = popInt();
                    KyVMStr* y = popStr();
                    KyVMInt* i = popInt();
                    KyVMBlob* blob = (KyVMBlob*)pop();
                    memcpy((KyChar*)(blob->data+(i->value)), y->str, n->value);
                    blob->release();
                    i->release();
                    n->release();
                    y->release();
                    break;
                }
                    
                    //
                    // Default
                    //
                default:
                    throw KyRuntimeError("Operation not supported");
            }
            
            topFrame()->ip++;
        }
    } catch(const KyEscapeException& e) {
        ;
    } catch(const KyError& e) {
        printStackTrace();
        throw;
    }
}

void KyVM::pushFrame(KyInt ip, KyInt vn, KyVMObject** args, KyInt argc) {
    KyVMObject** vars;
    // all frames share the same vars underneath, 
    // so start this new frame at the end of the last
    KyVMFrame* curFrame = topFrame();
    vars = curFrame->vars + curFrame->vn;
    fs[fsi++].reset(ip, vars, vn, args, argc);
}

void KyVM::pushFrameWithProc(KyInt ip, KyInt vn, KyVMObject** args, KyInt argc, KyVMProc* proc) {
    KyVMObject** vars;
    // all frames share the same vars underneath, 
    // so start this new frame at the end of the last
    KyVMFrame* curFrame = topFrame();
    vars = curFrame->vars + curFrame->vn;
    fs[fsi++].resetWithProc(ip, vars, vn, args, argc, proc);
}

KyBool KyVM::dropFrame() {
    fs[--fsi].free();
    return fs[fsi].isTopLevel;
}

KyVMFrame* KyVM::topFrame(KyInt i) {
    return fs + fsi + i; // the default for i is -1, returning the actual "top"
}

void KyVM::push(KyVMObject* obj) {
    if (obj == NULL) {
        return;  // FIXME -- move this check to returns
    }
    os[osi++] = obj->ref();
}

KyVMObject* KyVM::pop() {
    return os[--osi];
}

void KyVM::drop() {
    os[--osi]->release();
}

const KyChar* KyVM::findFileName(KyInt ip) {
    KyInt i = 0;
    while (ip > fileOffsets[i]) { 
        i++; 
    }
    return i ? fileNames[i-1] : "";
}

void KyVM::printStackTrace() {
    for (KyInt i = 0; i < fsi; i++) {
        KyVMFrame* f = fs + i;
        if (f->lineNo) {
            printf("%s:%d\n", findFileName(f->ip), f->lineNo);
        }
    }
}

void KyVM::cleanup() {
    while (osi) { drop();  }
    while (fsi) { dropFrame(); }
}

KyVM* KyVM::fromJamcode(const KyChar* jamcode) {
    KyVM*    vm = new KyVM();
    KyInt    cmdCount = 0, cmdEnd, intArg;
    KyInt*   cmdOffsets = (KyInt*)malloc(countLines(jamcode)*4);
    KyInt    fileCount = 0;
    KyInt    globalCount = 0;
    KyInt    jamLength = (KyInt)strlen(jamcode);
    KyBlob   codeBuilder;
    KyInt    rawTextLength;
    KyChar*  rawText;
    
    // Iterate over jamcode lines
    for (KyInt i = 0; i < jamLength; i = cmdEnd+1) {
        cmdEnd = endOfLine(jamcode, i);
        
        // Store line address
        cmdOffsets[cmdCount++] = codeBuilder.length;
        
        KyInt jamOp = *(int*)(jamcode+i), vmOp;
        const KyChar* arg = (jamcode+i+5);
        
        // Translate instruction
        switch (jamOp) {
            case JAM_NOP:  vmOp = VM_NOP;  break;
            case JAM_NULL: vmOp = VM_NULL; break;
            case JAM_ZERO: vmOp = VM_ZERO; break;
            case JAM_ONE:  vmOp = VM_ONE;  break;
            case JAM_LODI: vmOp = VM_LODI; break;
            case JAM_LODS: vmOp = VM_LODS; break;
            case JAM_LODF: vmOp = VM_LODF; break;
            case JAM_ADD:  vmOp = VM_ADD;  break;
            case JAM_SUB:  vmOp = VM_SUB;  break;
            case JAM_MUL:  vmOp = VM_MUL;  break;
            case JAM_DIV:  vmOp = VM_DIV;  break;
            case JAM_MULF: vmOp = VM_MULF; break;
            case JAM_DIVF: vmOp = VM_DIVF; break;
            case JAM_NOT:  vmOp = VM_NOT;  break;
            case JAM_NEG:  vmOp = VM_NEG;  break;
            case JAM_LT:   vmOp = VM_LT;   break;
            case JAM_GT:   vmOp = VM_GT;   break;
            case JAM_LTE:  vmOp = VM_LTE;  break;
            case JAM_GTE:  vmOp = VM_GTE;  break;
            case JAM_EQ:   vmOp = VM_EQ;   break;
            case JAM_NE:   vmOp = VM_NE;   break;
            case JAM_JMP:  vmOp = VM_JMP;  break;
            case JAM_JIF:  vmOp = VM_JIF;  break;
            case JAM_JZ:   vmOp = VM_JZ;   break;
            case JAM_JSR:  vmOp = VM_JSR;  break;
            case JAM_ARGS: vmOp = VM_ARGS; break;                
            case JAM_RET:  vmOp = VM_RET;  break;
            case JAM_MARK: vmOp = VM_MARK; break;
            case JAM_STEP: vmOp = VM_STEP; break;
            case JAM_CLR:  vmOp = VM_CLR;  break;                
            case JAM_DUP:  vmOp = VM_DUP;  break;
            case JAM_DROP: vmOp = VM_DROP; break;
            case JAM_SETA: vmOp = VM_SETA; break;
            case JAM_SETI: vmOp = VM_SETI; break;
            case JAM_SETL: vmOp = VM_SETL; break;
            case JAM_SETG: vmOp = VM_SETG; break;
            case JAM_GETA: vmOp = VM_GETA; break;
            case JAM_GETI: vmOp = VM_GETI; break;
            case JAM_GETL: vmOp = VM_GETL; break;
            case JAM_GETG: vmOp = VM_GETG; break;
            case JAM_NEW:  vmOp = VM_NEW;  break;
            case JAM_NEWV: vmOp = VM_NEWV; break;
            case JAM_REM:  vmOp = VM_REM;  break;
            case JAM_EVAL: vmOp = VM_EVAL; break;
            default: 
                vmOp = (KyInt)strtoul(jamcode+i+1, NULL, 16);
                if (!vmOp) {
                    throw KyNotImplementedError();
                }
                break;
        }
        
        codeBuilder.appendByte(vmOp);
        
        // Translate argument
        switch (jamOp) {
            case JAM_LODF:
                codeBuilder.appendInt(atoi(arg));
                while (*(arg++) != ' ') {}
                codeBuilder.appendInt(atoi(arg));
                break;
            case JAM_LODI:
                codeBuilder.appendInt(atoi(arg)); 
                break;
            case JAM_LODS:
                rawText = (KyChar*)malloc(cmdEnd-i-5+1);
                strncpy(rawText, arg, cmdEnd-i-5);
                rawText[cmdEnd-i-5] = 0;
                rawTextLength = unescapeText(rawText);
                codeBuilder.extend(rawText, rawTextLength);
                codeBuilder.appendByte(0);
                free(rawText);
                break;
            case JAM_JMP:
            case JAM_JIF:
            case JAM_JZ:
                codeBuilder.appendInt(atoi(arg));
                break;
            case JAM_JSR:
                codeBuilder.appendByte(atoi(arg));
                break;
            case JAM_MARK: 
                codeBuilder.appendByte(atoi(arg)); 
                break;
            case JAM_SETA:
            case JAM_SETL:
            case JAM_GETA:
            case JAM_GETL: 
                intArg = atoi(arg);
                Ky_assert(intArg<(1<<8));
                codeBuilder.appendByte(intArg); 
                break;
            case JAM_SETG:
            case JAM_GETG:
                intArg = atoi(arg);
                Ky_assert(intArg<(1<<16));
                globalCount = max(globalCount, intArg+1);
                codeBuilder.appendShort(intArg); 
                break;
            case JAM_NEW:
            case JAM_NEWV:
                codeBuilder.appendInt(atoi(arg)); 
                break;
            case JAM_REM:
                if (*arg == '@') {
                    vm->fileOffsets[fileCount] = cmdCount;
                    strncat(vm->fileNames[fileCount], arg+1, cmdEnd-i-6);
                    fileCount++;
                }
                codeBuilder.appendInt(atoi(arg));
                break;
            case JAM_EVAL:
                codeBuilder.appendInt(atoi(arg)); 
                break;
        }
    }
    
    cmdOffsets[cmdCount++] = codeBuilder.length;
    codeBuilder.appendByte(VM_RET);
    
    vm->fileOffsets[fileCount]  = cmdCount;
    vm->fileNames[fileCount][0] = 0;
    vm->code = (KyByte*)codeBuilder.returnData();
    vm->cmdOffsets = cmdOffsets;
    vm->globalCount = globalCount;

    return vm;
}

// End of KyVM
