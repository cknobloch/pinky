//
//  KyOpenGLView.m
//  macOS
//
//  Created by Curt Knobloch on 10/18/16.
//  Copyright © 2016 Curt Knobloch. All rights reserved.
//

#import "KyOpenGLView.hpp"

#include <OpenGL/gl.h>
#include <stdlib.h>

#include "base64.h"


//
// Enumerations below are taken from HIToolbox/Events.h
// Can't seem to locate the right header for VK codes, so I'm resorting
// to copy-and-paste.
//
enum {
    kVK_ANSI_A                    = 0x00,
    kVK_ANSI_S                    = 0x01,
    kVK_ANSI_D                    = 0x02,
    kVK_ANSI_F                    = 0x03,
    kVK_ANSI_H                    = 0x04,
    kVK_ANSI_G                    = 0x05,
    kVK_ANSI_Z                    = 0x06,
    kVK_ANSI_X                    = 0x07,
    kVK_ANSI_C                    = 0x08,
    kVK_ANSI_V                    = 0x09,
    kVK_ANSI_B                    = 0x0B,
    kVK_ANSI_Q                    = 0x0C,
    kVK_ANSI_W                    = 0x0D,
    kVK_ANSI_E                    = 0x0E,
    kVK_ANSI_R                    = 0x0F,
    kVK_ANSI_Y                    = 0x10,
    kVK_ANSI_T                    = 0x11,
    kVK_ANSI_1                    = 0x12,
    kVK_ANSI_2                    = 0x13,
    kVK_ANSI_3                    = 0x14,
    kVK_ANSI_4                    = 0x15,
    kVK_ANSI_6                    = 0x16,
    kVK_ANSI_5                    = 0x17,
    kVK_ANSI_Equal                = 0x18,
    kVK_ANSI_9                    = 0x19,
    kVK_ANSI_7                    = 0x1A,
    kVK_ANSI_Minus                = 0x1B,
    kVK_ANSI_8                    = 0x1C,
    kVK_ANSI_0                    = 0x1D,
    kVK_ANSI_RightBracket         = 0x1E,
    kVK_ANSI_O                    = 0x1F,
    kVK_ANSI_U                    = 0x20,
    kVK_ANSI_LeftBracket          = 0x21,
    kVK_ANSI_I                    = 0x22,
    kVK_ANSI_P                    = 0x23,
    kVK_ANSI_L                    = 0x25,
    kVK_ANSI_J                    = 0x26,
    kVK_ANSI_Quote                = 0x27,
    kVK_ANSI_K                    = 0x28,
    kVK_ANSI_Semicolon            = 0x29,
    kVK_ANSI_Backslash            = 0x2A,
    kVK_ANSI_Comma                = 0x2B,
    kVK_ANSI_Slash                = 0x2C,
    kVK_ANSI_N                    = 0x2D,
    kVK_ANSI_M                    = 0x2E,
    kVK_ANSI_Period               = 0x2F,
    kVK_ANSI_Grave                = 0x32,
    kVK_ANSI_KeypadDecimal        = 0x41,
    kVK_ANSI_KeypadMultiply       = 0x43,
    kVK_ANSI_KeypadPlus           = 0x45,
    kVK_ANSI_KeypadClear          = 0x47,
    kVK_ANSI_KeypadDivide         = 0x4B,
    kVK_ANSI_KeypadEnter          = 0x4C,
    kVK_ANSI_KeypadMinus          = 0x4E,
    kVK_ANSI_KeypadEquals         = 0x51,
    kVK_ANSI_Keypad0              = 0x52,
    kVK_ANSI_Keypad1              = 0x53,
    kVK_ANSI_Keypad2              = 0x54,
    kVK_ANSI_Keypad3              = 0x55,
    kVK_ANSI_Keypad4              = 0x56,
    kVK_ANSI_Keypad5              = 0x57,
    kVK_ANSI_Keypad6              = 0x58,
    kVK_ANSI_Keypad7              = 0x59,
    kVK_ANSI_Keypad8              = 0x5B,
    kVK_ANSI_Keypad9              = 0x5C
};

/* keycodes for keys that are independent of keyboard layout*/
enum {
    kVK_Return                    = 0x24,
    kVK_Tab                       = 0x30,
    kVK_Space                     = 0x31,
    kVK_Delete                    = 0x33,
    kVK_Escape                    = 0x35,
    kVK_Command                   = 0x37,
    kVK_Shift                     = 0x38,
    kVK_CapsLock                  = 0x39,
    kVK_Option                    = 0x3A,
    kVK_Control                   = 0x3B,
    kVK_RightShift                = 0x3C,
    kVK_RightOption               = 0x3D,
    kVK_RightControl              = 0x3E,
    kVK_Function                  = 0x3F,
    kVK_F17                       = 0x40,
    kVK_VolumeUp                  = 0x48,
    kVK_VolumeDown                = 0x49,
    kVK_Mute                      = 0x4A,
    kVK_F18                       = 0x4F,
    kVK_F19                       = 0x50,
    kVK_F20                       = 0x5A,
    kVK_F5                        = 0x60,
    kVK_F6                        = 0x61,
    kVK_F7                        = 0x62,
    kVK_F3                        = 0x63,
    kVK_F8                        = 0x64,
    kVK_F9                        = 0x65,
    kVK_F11                       = 0x67,
    kVK_F13                       = 0x69,
    kVK_F16                       = 0x6A,
    kVK_F14                       = 0x6B,
    kVK_F10                       = 0x6D,
    kVK_F12                       = 0x6F,
    kVK_F15                       = 0x71,
    kVK_Help                      = 0x72,
    kVK_Home                      = 0x73,
    kVK_PageUp                    = 0x74,
    kVK_ForwardDelete             = 0x75,
    kVK_F4                        = 0x76,
    kVK_End                       = 0x77,
    kVK_F2                        = 0x78,
    kVK_PageDown                  = 0x79,
    kVK_F1                        = 0x7A,
    kVK_LeftArrow                 = 0x7B,
    kVK_RightArrow                = 0x7C,
    kVK_DownArrow                 = 0x7D,
    kVK_UpArrow                   = 0x7E
};

enum {
    NATIVE_GINIT = 0,
    NATIVE_GBEGIN,
    NATIVE_GEND,
    NATIVE_GENABLE,
    NATIVE_CLEAR,
    NATIVE_SETVIEWPORT,
    NATIVE_PROGSTATE,
    NATIVE_POLLKEYS,
    NATIVE_IMG,
    NATIVE_SUBIMGEX,
    NATIVE_IMGW,
    NATIVE_IMGH,
    NATIVE_DRAWEX,
    NATIVE_DRAWSUBEX,
    NATIVE_DRAWSTRETCHEDEX,
    NATIVE_DRAWMAP,
    NATIVE_DRAWTEXT,
    NATIVE_REMPTY,
    NATIVE_RINTER,
    NATIVE_RUNION,
    NATIVE_RHITPOINT,
    NATIVE_RHITSEGMENT,
    NATIVE_RESOLVEHIT,
    NATIVE_RESOLVEMAPHIT,
    NATIVE_WAV,
    NATIVE_PLAYEX,
    NATIVE_STOP,
    NATIVE_SETGAMEPARAMS,
    NATIVE_CAMSETPATH,
    NATIVE_CAMADVANCE,
    NATIVE_CAM,
    NATIVE_DRAWSPRITE,
    NATIVE_DRAWTILEMAP,
    NATIVE_DRAWTILEOBJ,
    NATIVE_SETTILESETFRAME,
    NATIVE_MOVESPRITE
};

//
// UTILITY
//

KyInt floatToFp(KyFloat value) {
    return (KyInt)(value*0x10000);
}

KyFloat fpToFloat(KyInt value) {
    return (KyFloat)value / 0x10000;
}

KyInt fpMul(KyInt a, KyInt b) {
    return (KyInt)(((KyLong)a*b)>>16);
}

KyInt fpDiv(KyInt a, KyInt b) {
    return (KyInt)(((KyLong)a<<16)/b);
}

KyInt fpHypot(KyInt a, KyInt b) {
    return floatToFp(hypot(fpToFloat(a), fpToFloat(b)));
}

KyInt clamp(KyInt x, KyInt a, KyInt b) {
    return min(max(x, a), b);    
}

KyInt easeout(KyInt a, KyInt b, KyFloat t) {
    return a + (KyInt)((b-a)*(1.0-((1.0-t)*(1.0-t))));
}

void rectIntersection(KyInt c[4], KyInt a[4], KyInt b[4]) {
    c[0] = max(a[0], b[0]);
    c[1] = max(a[1], b[1]);
    c[2] = min(a[2], b[2]);
    c[3] = min(a[3], b[3]);
}

void rectUnion(KyInt c[4], KyInt a[4], KyInt b[4]) {
    c[0] = min(a[0], b[0]);
    c[1] = min(a[1], b[1]);
    c[2] = max(a[2], b[2]);
    c[3] = max(a[3], b[3]);
}

KyBool rectEmpty(KyInt a[4]) {
    return a[0] >= a[2] || a[1] >= a[3];
}

KyInt rectSeparationFlags(KyInt a[4], KyInt b[4]) {
    return (((a[1] >= b[3]) ? 8 : 0) |
            ((a[2] <= b[0]) ? 4 : 0) |
            ((a[3] <= b[1]) ? 2 : 0) |
            ((a[0] >= b[2]) ? 1 : 0));    
}

KyBool rectHitPoint(KyInt a[4], KyInt x, KyInt y) {
    return a[0] <= x && a[2] > x && a[1] <= y && a[3] > y;
}

KyBool rectHitSegment(KyInt a[4], KyInt x1, KyInt y1, KyInt x2, KyInt y2) {
    KyInt ax1 = a[0],
    ay1 = a[1],
    ax2 = a[2],
    ay2 = a[3];
    
    if ((x1 <  ax1 && x2 <  ax1) ||
        (y1 <  ay1 && y2 <  ay1) ||
        (x1 >= ax2 && x2 >= ax2) ||
        (y1 >= ay2 && y2 >= ay2)) {
        return FALSE;
    }
    
    KyInt dx = x2 - x1;
    KyInt dy = y2 - y1;
    
    if (dx) {
        KyFloat m = dy / (KyFloat)dx;
        KyInt y;
        y = y1 + (KyInt)(m*(ax1-x1));
        if (y >= ay1 && y < ay2) return TRUE;
        y = y1 + (KyInt)(m*(ax2-x1));
        if (y >= ay1 && y < ay2) return TRUE;
    }
    
    if (dy) {
        KyFloat m = dx / (KyFloat)dy;
        KyInt x;
        x = x1 + (KyInt)(m*(ay1-y1));
        if (x >= ax1 && x < ax2) return TRUE;  
        x = x1 + (KyInt)(m*(ay2-y1));
        if (x >= ax2 && x < ax2) return TRUE;
    }
    
    return FALSE;
}

KyChar* readWholeTextFile(const KyChar* path) {
    KyInt   capacity = 1;
    KyInt   len      = 0;
    KyChar* data     = (KyChar*)malloc(capacity);
    FILE*   file	 = fopen(path, "rb");
    KyInt   c;
    while ((c=fgetc(file)) != EOF) {
        data[len++] = c;
        if (len >= capacity) {
            data = (KyChar*)realloc(data, capacity*=2);
        }
    }
    fclose(file);
    data = (KyChar*)realloc(data, len+1);
    data[len] = 0;
    return data;
}

//
// NATIVE TYPES
//

class MacOSImage {
public:
    CGImageRef image;
    
    MacOSImage(const KyByte* data, KyInt len) {
        CFDataRef cfData = CFDataCreateWithBytesNoCopy(NULL, (const UInt8*)data, (CFIndex)len, kCFAllocatorNull);
        CGImageSourceRef imageSource = CGImageSourceCreateWithData(cfData, NULL);
        image = CGImageSourceCreateImageAtIndex(imageSource, 0, NULL);
        CFRelease(imageSource);
        CFRelease(cfData);
    }
    
    ~MacOSImage() {
        CGImageRelease(image);
    }
    
    KyInt w() { return (KyInt)CGImageGetWidth(image); }
    KyInt h() { return (KyInt)CGImageGetHeight(image); }
    
    KyInt* getColorData(KyInt* colorData, KyInt x, KyInt y, KyInt w, KyInt h, KyInt scanlen) {
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGContextRef context = CGBitmapContextCreate(colorData, w, h, 8, w*4, colorSpace, kCGImageAlphaPremultipliedLast); //kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Host);        
        CGContextSetBlendMode(context, kCGBlendModeCopy);
        CGContextDrawImage(context, CGRectMake((CGFloat)x, (CGFloat)y, (CGFloat)w, (CGFloat)h), image);
        CGColorSpaceRelease(colorSpace);
        CGContextRelease(context);
        return colorData;
    }    
};

class KyVMBaseImage : public KyVMObject {
public:
    KyInt offX, offY, w, h, flags, color;
    KyVMBaseImage() {
        offX = offY = w = h = flags = color = 0;
    }
    
    virtual void draw(KyInt x, KyInt y, KyInt flags) = 0;
    virtual void drawStretched(KyInt x, KyInt y, KyInt w, KyInt h, KyInt flags) = 0;
    virtual void drawRegion(KyInt x, KyInt y, KyInt w, KyInt h, KyInt sx, KyInt sy, KyInt sw, KyInt sh, KyInt flags) = 0;
    
    virtual KyVMBaseImage* parentImage() { return this; }
    
    static void drawMap(KyVMBaseImage** images, 
                        KyByte* map, KyInt mapW, KyInt mapH,
                        KyInt offX, KyInt offY, KyInt w, KyInt h,
                        KyInt tw, KyInt th, KyInt x, KyInt y) {
        for (KyInt y1 = offY; y1 < offY+h; y1++) {
            for (KyInt x1 = offX; x1 < offX+w; x1++) {
                KyInt i = map[(x1%mapW)+(y1%mapH)*mapW];
                if (i != 0) {
                    images[i-1]->draw(x+x1*tw, y+y1*th, 0x00);
                }
            }
        }
    }
    
    static void drawText(KyVMBaseImage** images,
                         KyVMInt** strides,
                         const KyChar* text, 
                         KyInt xi, KyInt y, KyInt offset, KyInt lh,
                         KyVMInt** subChars) {
        KyInt i = 0;
        KyInt j = 0;
        KyInt n = (KyInt)strlen(text);
        KyInt x = xi;
        while (i < n) {
            KyInt c = text[i++];
            if (c == '\n') {
                x = xi;
                y = y + lh;
                continue;
            } else if (c == '\t') {
                ;
            } else if (c == '$') {
                c = subChars[j++]->value;
                images[c]->draw(x, y, 0x00);
            } else if (c != ' ') {
                images[c]->draw(x, y, 0x00);
            }
            x += offset + strides[c]->toCInt();
        }
    }
};

class KyVMImage : public KyVMBaseImage {
public:
    GLuint glID;
    
    KyVMImage(const KyByte* data, KyInt len) {
        glGenTextures(1, &glID);
        MacOSImage image(data, len);
        offX = offY = 0;
        w = image.w(); 
        h = image.h();
        KyInt* colorData = (KyInt*)malloc(w*h*4);
        setColorData(image.getColorData(colorData, 0, 0, w, h, w), w, h);
        ::free(colorData);
    }
    
    ~KyVMImage() {
        glDeleteTextures(1, &glID);
    }
    
    void setColorData(const KyInt* colorData, KyInt w, KyInt h, KyInt level=0) {
        glBindTexture(GL_TEXTURE_2D, glID);
        glTexImage2D(GL_TEXTURE_2D, level, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, colorData);
    }
    
    void draw(KyInt x, KyInt y, KyInt flags) {
        drawGLImage(this->glID, this->w, this->h, x, y, this->w, this->h, 0, 0, this->w, this->h, color, flags);
    }
    
    void drawStretched(KyInt x, KyInt y, KyInt w, KyInt h, KyInt flags) {
        drawGLImage(this->glID, this->w, this->h, x, y, w, h, 0, 0, this->w, this->h, color, flags);
    }
    
    void drawRegion(KyInt x, KyInt y, KyInt w, KyInt h, 
                    KyInt sx, KyInt sy, KyInt sw, KyInt sh, 
                    KyInt flags) {
        drawGLImage(this->glID, this->w, this->h, x, y, w, h, sx, sy, sw, sh, color, flags);
    }
    
    static void drawGLImage(GLuint glID,
                            KyInt glW, KyInt glH,
                            KyInt x, KyInt y, KyInt w, KyInt h,
                            KyInt sx, KyInt sy, KyInt sw, KyInt sh,
                            KyInt color,
                            KyInt flags) {
        if ((flags&0xC0) == 0xC0) {
            return;
        }
        
        glBindTexture(GL_TEXTURE_2D, glID);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
        KyInt blendMode = flags >> 4 & 0x3;
        
        if (!blendMode) {
            glDisable(GL_COLOR_MATERIAL);
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
        } else {
            // NOTE: alpha doesn't seem to be added for some reason
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, 
                      (blendMode==3?
                       GL_MODULATE:
                       (blendMode==1?
                        GL_ADD:GL_SUBTRACT)));
            glEnable(GL_COLOR_MATERIAL);
            glColor4f((color>>16&0xFF)/255.0f, 
                      (color>> 8&0xFF)/255.0f, 
                      (color    &0xFF)/255.0f,
                      (color>>24&0xFF)/255.0f);
        }
        
        glMatrixMode(GL_TEXTURE);
        glLoadIdentity();
        glScalef(1.0f/glW, 1.0f/glH, 1.0f);
        glTranslatef(sx, sy, 0);
        glScalef(sw, sh, 1.0f);
        
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        
        glTranslatef(x, y, 0);
        glScalef(w, h, 1.0f);
        glTranslatef(0.5f, 0.5f, 0.0f);
        glRotatef(((flags&0xC)>>2)*90.0f, 0.0f, 0.0f, 1.0f);
        glScalef((flags&1)?-1.0f:1.0f, (flags&2)?-1.0f:1.0f, 1.0f);
        glTranslatef(-0.5f, -0.5f, 0.0f);
        
        glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex2f(0.0, 0.0);
        glTexCoord2f(1.0, 0.0); glVertex2f(1.0, 0.0);
        glTexCoord2f(1.0, 1.0); glVertex2f(1.0, 1.0);
        glTexCoord2f(0.0, 1.0); glVertex2f(0.0, 1.0);
        glEnd();
        
        glPopMatrix();
    }
};

class KyVMSubImage : public KyVMBaseImage {
public:
    KyVMImage* image;
    
    KyVMSubImage(KyVMBaseImage* baseImage, KyInt x, KyInt y, KyInt w, KyInt h, KyInt flags, KyInt color) {
        KyInt blendModeA = baseImage->flags&0xF0, blendModeB = flags&0xF0, blendModeC = 0;
        KyInt colorA = baseImage->color, colorB = color, colorC = 0; 
        
        if (blendModeA == 0) {
            blendModeC = blendModeB;
            colorC = colorB;
        } else if (blendModeB == 0) {
            blendModeC = blendModeA;
            colorC = colorA;
        } else if (blendModeA == 0x30 && blendModeA == blendModeB) {
            blendModeC = blendModeA;
            colorC = rgbaMultiply(colorA, colorB);
        } else if (blendModeA == blendModeB) {
            blendModeC = blendModeA;
            colorC = rgbaAdd(colorA, colorB);
        } else if (blendModeB != 0x30 && blendModeA != 0x30) {
            blendModeC = blendModeA;
            colorC = rgbaSubtract(colorA, colorB);
        } else {
            blendModeC = blendModeB;
            colorC = colorB;
        }
        
        this->image = (KyVMImage*)baseImage->parentImage()->ref();
        this->offX  = baseImage->offX + x;
        this->offY  = baseImage->offY + y;
        this->w     = min(w, baseImage->w-x); 
        this->h     = min(h, baseImage->h-y);
        this->flags = blendModeC | ((flags^baseImage->flags)&0xF);        
        this->color = colorC;
    }
    
    ~KyVMSubImage() {
        image->release();
    }
    
    void draw(KyInt x, KyInt y, KyInt flags) {
        KyVMImage::drawGLImage(image->glID, image->w, image->h, x, y, w, h, offX, offY, w, h, color, this->flags^flags);
    }
    
    void drawRegion(KyInt x, KyInt y, KyInt w, KyInt h, KyInt sx, KyInt sy, KyInt sw, KyInt sh, KyInt flags) {
        KyVMImage::drawGLImage(image->glID, image->w, image->h, x, y, w, h, offX+sx, offY+sy, sw, sh, color, this->flags^flags);
    }
    
    void drawStretched(KyInt x, KyInt y, KyInt w, KyInt h, KyInt flags) {
        KyVMImage::drawGLImage(image->glID, image->w, image->h, x, y, w, h, offX, offY, this->w, this->h, color, this->flags^flags);
    }
    
    KyVMBaseImage* parentImage() { return image; }
    
    static KyInt rgbaAdd(KyInt a, KyInt b) {
        return (min((a>>24&0xFF)+(b>>24&0xFF), 0xFF)<<24 |
                min((a>>16&0xFF)+(b>>16&0xFF), 0xFF)<<16 |
                min((a>> 8&0xFF)+(b>> 8&0xFF), 0xFF)<< 8 |
                min((a    &0xFF)+(b    &0xFF), 0xFF));
    }
    
    static KyInt rgbaSubtract(KyInt a, KyInt b) {
        return (max((a>>24&0xFF)-(b>>24&0xFF), 0xFF)<<24 |
                max((a>>16&0xFF)-(b>>16&0xFF), 0xFF)<<16 |
                max((a>> 8&0xFF)-(b>> 8&0xFF), 0xFF)<< 8 |
                max((a    &0xFF)-(b    &0xFF), 0xFF));
    }
    
    static KyInt rgbaMultiply(KyInt a, KyInt b) {
        return (min((a>>24&0xFF)*(b>>24&0xFF)/0xFF, 0xFF)<<24 |
                min((a>>16&0xFF)*(b>>16&0xFF)/0xFF, 0xFF)<<16 |
                min((a>> 8&0xFF)*(b>> 8&0xFF)/0xFF, 0xFF)<< 8 |
                min((a    &0xFF)*(b    &0xFF)/0xFF, 0xFF));
    }
};

class KyVMSound : public KyVMObject {
public:
    NSSound* nsSound;
    KyVMSound(KyByte* data, KyInt len) {
        NSData* nsData = [[NSData alloc] initWithBytesNoCopy:data
                                                      length:(NSUInteger)len
                                                freeWhenDone:(BOOL)FALSE];
        nsSound = [[NSSound alloc] initWithData:nsData];
    }
    ~KyVMSound() {
        // [nsSound release]; // automatic reference counting mode is on!
    }
    void play(KyFloat gain=1.0, KyBool loop=FALSE) {
        nsSound.volume = gain;
        nsSound.loops  = loop;
        [nsSound play];
    }
    void stop() {
        [nsSound stop];
    }
};

//
// NATIVE STRUCTS
//

class KyVMRect : public KyVMArray {
public:
    KyVMRect(KyVMInt* x1, KyVMInt* y1, KyVMInt* x2, KyVMInt* y2) 
    : KyVMArray(4) {
        setX1(x1);
        setY1(y1);
        setX2(x2);
        setY2(y2);
    }
    KyVMInt* getX1() { return (KyVMInt*)get(0); }
    KyVMInt* getY1() { return (KyVMInt*)get(1); }
    KyVMInt* getX2() { return (KyVMInt*)get(2); }
    KyVMInt* getY2() { return (KyVMInt*)get(3); }
    void setX1(KyVMInt* value) { set(0, value); }
    void setY1(KyVMInt* value) { set(1, value); }
    void setX2(KyVMInt* value) { set(2, value); }
    void setY2(KyVMInt* value) { set(3, value); }
    
    KyInt hitRect(KyVMRect* _a1, KyVMRect* _b, KyInt aFlags, KyInt bFlags) {
        
        KyVMRect* _a2 = this;
        
        KyInt a1[4] = {_a1->getX1()->value, _a1->getY1()->value, _a1->getX2()->value, _a1->getY2()->value};
        KyInt a2[4] = {this->getX1()->value, this->getY1()->value, this->getX2()->value, this->getY2()->value};
        KyInt b [4] = {_b->getX1()->value, _b->getY1()->value, _b->getX2()->value, _b->getY2()->value};
        
        KyInt aTypes = aFlags & 0xFFFF0;
        KyInt bTypes = bFlags & 0xFFFF0;
        KyInt cTypes = aTypes & bTypes;
        if (!cTypes) {
            return 0;
        }
        
        KyInt inter[4];
        
        rectIntersection(inter, a2, b);
        
        if (rectEmpty(inter)) {
            rectIntersection(inter, a1, b);
            return rectEmpty(inter) ? 0 : ((0x40000000&aFlags)|cTypes);
        }
         
        KyInt aMass  = aFlags & 0xF00000;
        KyInt bMass  = bFlags & 0xF00000;
        KyInt bSideFlags = ((bFlags>>2)&0x3) | ((bFlags<<2)&0xC);
        KyInt prevSeparationFlags = rectSeparationFlags(a1, b); 
        KyInt touchFlag = prevSeparationFlags ? 0 : (0x80000000&aFlags);
        KyInt cSideFlags = prevSeparationFlags & aFlags & bSideFlags;
        KyInt cMass  = aMass ? bMass : aMass;
        // Displace based on intersection
        if (cSideFlags && cMass) {
            KyFloat s;
            if (aMass == 0xF00000) {
                s = 0.0;
            } else if (bMass == 0xF00000) {
                s = 1.0;
            } else {
                s = max(min(0.5+((bMass-aMass)>>12)/7.0, 1.0), 0.0);
            }
            
            if (cSideFlags&~0xA) {
                KyInt dx   = a2[0] - a1[0];
                KyInt nx   = dx / abs(dx);
                KyInt dx1  = inter[2] - inter[0];
                KyInt a_dx = nx * (KyInt)(dx1*(s));
                KyInt b_dx = nx * (KyInt)(ceil(dx1*(1.0-s)));
                _a2->getX1()->value -= a_dx;
                _a2->getX2()->value -= a_dx;
                _b ->getX1()->value += b_dx;
                _b ->getX2()->value += b_dx;
            } else {
                KyInt dy   = a2[1] - a1[1];
                KyInt ny   = dy / abs(dy);
                KyInt dy1  = inter[3] - inter[1];
                KyInt a_dy = ny * (KyInt)(dy1*(s));
                KyInt b_dy = ny * (KyInt)(ceil(dy1*(1.0-s)));
                _a2->getY1()->value -= a_dy;
                _a2->getY2()->value -= a_dy;
                _b ->getY1()->value += b_dy;
                _b ->getY2()->value += b_dy;
            }
        }
        
        return touchFlag | cMass | cTypes | cSideFlags;
    }
    
    KyInt hitMap(KyVMRect* old, KyByte* mapData, KyByte* tsFlags, KyInt mapW, KyInt mapH, KyInt tw, KyInt th, KyInt aFlags) {
        
        KyInt interXY = 0, interX = 0, interY = 0, optXFlags = 0, optYFlags = 0, cFlags = 0;
        KyInt aTypes = (aFlags>>4) & 0xFF;
        
        KyInt a1[4] = {old->getX1()->value, old->getY1()->value, old->getX2()->value, old->getY2()->value};
        KyInt a2[4] = {this->getX1()->value, this->getY1()->value, this->getX2()->value, this->getY2()->value};
        
        KyInt tx1 = max((a2[0])/tw, 0);
        KyInt ty1 = max((a2[1])/th, 0);
        KyInt tx2 = min((a2[2]+tw-1)/tw, mapW);
        KyInt ty2 = min((a2[3]+th-1)/th, mapH);
        
        for (KyInt ty = ty1; ty < ty2; ty++) {
            for (KyInt tx = tx1; tx < tx2; tx++) {
                KyByte tileIndex = mapData[tx+ty*mapW];
                if (tileIndex) {
                    KyByte bFlags = tsFlags[tileIndex-1];
                    KyInt bType = 1 << (bFlags>>4) >> 1; // convert type to flag
                    KyInt cTypeFlag = (aTypes&bType) << 4;
                    if (!cTypeFlag) { // allow zero, otherwise skip if type not set
                        continue;
                    }
                    KyInt bx1 = tx*tw, by1 = ty*th;
                    KyInt b[4] = {bx1, by1, bx1+tw, by1+th};
                    KyInt inter[4];
                    rectIntersection(inter, a2, b);
                    if (rectEmpty(inter)) {
                        continue;
                    }
                    KyInt bSideFlags = ((bFlags>>2)&0x3) | ((bFlags<<2)&0xC);
                    KyInt cSideFlags = rectSeparationFlags(a1, b) & aFlags;
                    if (cSideFlags&bSideFlags) {
                        cSideFlags &= bSideFlags;
                        if (cSideFlags == (cSideFlags&0x5)) {
                            interX  = max(interX,  inter[2]-inter[0]);
                        } else if (cSideFlags == (cSideFlags&0xA)) {
                            interY  = max(interY,  inter[3]-inter[1]);
                        } else {
                            interXY = max(interXY, inter[3]-inter[1]);
                        }
                        cFlags |= cSideFlags | cTypeFlag | 0xF00000;
                    } else {
                        if (cSideFlags == (cSideFlags&0x5)) {
                            optXFlags |= cTypeFlag;
                        } else if (cSideFlags == (cSideFlags&0xA)) {
                            optYFlags |= cTypeFlag;
                        }
                    }
                }
            }
        }
        
        // If any collision flags have been set,
        // we need to displace the original dx/dy by the intersection
        if (interX) {
            KyInt dx = a2[0] < a1[0] ? -interX : interX;
            this->setX1(new KyVMInt(this->getX1()->value-dx));
            this->setX2(new KyVMInt(this->getX2()->value-dx));
        } else {
            if (!interY) {
                interY = interXY;
            }
            cFlags = (cFlags&~0xF) | (cFlags&0xA) | optXFlags;
        }
        
        if (interY) {
            KyInt dy = a2[1] < a1[1] ? -interY : interY;
            this->setY1(new KyVMInt(this->getY1()->value-dy));
            this->setY2(new KyVMInt(this->getY2()->value-dy));
        } else {
            cFlags = (cFlags&~0xF) | (cFlags&0x5) | optYFlags;
        }
        
        return cFlags;
    }
};

class KyVMCam : public KyVMArray {
public:
    KyVMCam() 
    : KyVMArray(13) {
        setX(kyVMZero);
        setY(kyVMZero);
        setNx(kyVMZero);
        setNy(kyVMZero);
        setD(kyVMZero);
        setDAcc(kyVMZero);
        setDMax(kyVMZero);
        setIdx(kyVMZero);
        _setPath((KyVMArray*)kyVMNull);
        setXMin(kyVMZero);
        setYMin(kyVMZero);
        setXMax(kyVMZero);
        setYMax(kyVMZero);
    }
    
    KyVMInt* getX()      { return (KyVMInt*)get(0); }
    KyVMInt* getY()      { return (KyVMInt*)get(1); }
    KyVMInt* getNx()     { return (KyVMInt*)get(2); }
    KyVMInt* getNy()     { return (KyVMInt*)get(3); }
    KyVMInt* getD()      { return (KyVMInt*)get(4); }
    KyVMInt* getDAcc()   { return (KyVMInt*)get(5); }
    KyVMInt* getDMax()   { return (KyVMInt*)get(6); }
    KyVMInt* getIdx()    { return (KyVMInt*)get(7); }
    KyVMArray* getPath() { return (KyVMArray*)get(8); }
    KyVMInt* getXMin()   { return (KyVMInt*)get(9); }
    KyVMInt* getYMin()   { return (KyVMInt*)get(10); }
    KyVMInt* getXMax()   { return (KyVMInt*)get(11); }
    KyVMInt* getYMax()   { return (KyVMInt*)get(12); }
    
    void setX(KyVMInt* value)      { set(0, value); }
    void setY(KyVMInt* value)      { set(1, value); }
    void setNx(KyVMInt* value)     { set(2, value); }
    void setNy(KyVMInt* value)     { set(3, value); }
    void setD(KyVMInt* value)      { set(4, value); }
    void setDAcc(KyVMInt* value)   { set(5, value); }
    void setDMax(KyVMInt* value)   { set(6, value); }
    void setIdx(KyVMInt* value)    { set(7, value); }
    void _setPath(KyVMArray* value) { set(8, value); }
    void setXMin(KyVMInt* value)   { set(9, value); }
    void setYMin(KyVMInt* value)   { set(10, value); }
    void setXMax(KyVMInt* value)   { set(11, value); }
    void setYMax(KyVMInt* value)   { set(12, value); }
            
    void setBounds(KyVMInt* x_min, 
                   KyVMInt* y_min, 
                   KyVMInt* x_max, 
                   KyVMInt* y_max) {
        setXMin(x_min);
        setYMin(y_min);
        setXMax(x_max);
        setYMax(y_max);
    }
        
    void nextPath() {
        KyInt      i     = getIdx()->value;
        KyVMArray* dest  = getPath()->sub(i);
        KyInt      dx    = dest->getInt(0) - getX()->value;
        KyInt      dy    = dest->getInt(1) - getY()->value;
        KyInt      d_max = max(abs(dx), abs(dy));
        
        if (d_max) {
            setNx(new KyVMInt(fpDiv(dx, d_max)));
            setNy(new KyVMInt(fpDiv(dy, d_max)));
            setD (new KyVMInt(fpHypot(dx, dy)));
        } else {
            setNx(kyVMZero);
            setNy(kyVMZero);
            setD (kyVMZero);
        }
        
        setIdx(new KyVMInt(i+1));
    }
        
    void advance() {
        KyInt d_acc = getDAcc()->value;
        KyInt d_max = getDMax()->value;
        
        if (d_acc == d_max) {
            return;
        }
        
        KyInt i = getIdx()->value;
        
        // smooth out speed over entire path
        KyInt d = easeout(getPath()->sub(i-1)->getInt(2), 
                          1<<16, 
                          ((KyFloat)d_acc)/d_max);
        
        KyInt total_d = getD()->value;
        
        if (d <= total_d) {
            setX(new KyVMInt(clamp(getX()->value+fpMul(getNx()->value, d), 
                                   getXMin()->value, 
                                   getXMax()->value)));
            setY(new KyVMInt(clamp(getY()->value+fpMul(getNy()->value, d), 
                                   getYMin()->value, 
                                   getYMax()->value)));
            d_acc += d;
            total_d -= d;
            
            setDAcc(new KyVMInt(d_acc));
            setD(new KyVMInt(total_d));
        } else {
            setX(new KyVMInt(clamp(getX()->value+fpMul(getNx()->value, total_d), 
                                   getXMin()->value, 
                                   getXMax()->value)));
            setY(new KyVMInt(clamp(getY()->value+fpMul(getNy()->value, total_d), 
                                   getYMin()->value, 
                                   getYMax()->value)));
            
            d_acc += total_d;
            
            if (i == getPath()->len) {
                total_d = 0;
                setDAcc(new KyVMInt(d_acc));
                setD(new KyVMInt(total_d));
            } else {
                total_d = d - total_d;
                setDAcc(new KyVMInt(d_acc));
                setD(new KyVMInt(total_d));
                nextPath();
                advance();
            }
        }
    }
        
    void setPath(KyVMArray* path) {
        _setPath(path);
        setX(new KyVMInt(clamp(((KyVMInt*)path->sub(0)->get(0))->value, 
                               getXMin()->value, 
                               getXMax()->value)));
        setY(new KyVMInt(clamp(((KyVMInt*)path->sub(0)->get(1))->value, 
                               getYMin()->value, 
                               getYMax()->value)));
        setIdx(kyVMOne);
        setDAcc(kyVMZero);
        setDMax(KyVMCam::calcPathDist(path));
        nextPath();
    }
    
    static KyVMInt* calcPathDist(KyVMArray* path) {
        if (!path->len) {
            return 0;
        }
        KyInt d = 0;
        KyInt n = path->len - 1;
        KyVMArray* b = path->sub(n), *a;
        while (n) {
            n = n - 1;
            a = path->sub(n);
            d = d + fpHypot(b->getInt(0)-a->getInt(0), b->getInt(1)-a->getInt(1));
            b = a;
        }
        return new KyVMInt(d);
    }
};

class KyVMSprite : public KyVMArray {
public:
    KyVMBaseImage* getSrc()      { return (KyVMBaseImage*)get(0); }
    KyVMRect*      getBnds()     { return (KyVMRect*)get(1); }
    KyVMRect*      getOldBnds()  { return (KyVMRect*)get(2); }
    KyVMRect*      getInset()    { return (KyVMRect*)get(3); }
    KyVMInt*       getRotation() { return (KyVMInt*)get(4); }
    KyVMInt*       getFlipX()    { return (KyVMInt*)get(5); }
    KyVMInt*       getFlipY()    { return (KyVMInt*)get(6); }
    KyVMInt*       getHide()     { return (KyVMInt*)get(7); }
    KyVMInt*       getVX()       { return (KyVMInt*)get(8); }
    KyVMInt*       getVY()       { return (KyVMInt*)get(9); }
    KyVMInt*       getAX()       { return (KyVMInt*)get(10); }
    KyVMInt*       getAY()       { return (KyVMInt*)get(11); }
    KyVMInt*       getVXT()      { return (KyVMInt*)get(12); }
    KyVMInt*       getVYT()      { return (KyVMInt*)get(13); }
    KyVMInt*       getFX()       { return (KyVMInt*)get(14); }
    KyVMInt*       getFY()       { return (KyVMInt*)get(15); }
    KyVMInt*       getHid()      { return (KyVMInt*)get(16); }
    KyVMInt*       getHidsOn()   { return (KyVMInt*)get(17); }
    KyVMInt*       getHidsOff()  { return (KyVMInt*)get(18); }
    KyVMInt*       getSides()    { return (KyVMInt*)get(19); }
    KyVMInt*       getWeight()   { return (KyVMInt*)get(20); }
    KyVMInt*       getHits()     { return (KyVMInt*)get(21); }
    
    void setSrc(KyVMBaseImage* value) { set(0, value); }
    void setBnds(KyVMRect* value)     { set(1, value); }
    void setOldBnds(KyVMRect* value)  { set(2, value); }
    void setInset(KyVMRect* value)    { set(3, value); }
    void setRotation(KyVMInt* value)  { set(4, value); }
    void setFlipX(KyVMInt* value)     { set(5, value); }
    void setFlipY(KyVMInt* value)     { set(6, value); }
    void setHide(KyVMInt* value)      { set(7, value); }
    void setVX(KyVMInt* value)        { set(8, value); }
    void setVY(KyVMInt* value)        { set(9, value); }
    void setAX(KyVMInt* value)        { set(10, value); }
    void setAY(KyVMInt* value)        { set(11, value); }
    void setVXT(KyVMInt* value)       { set(12, value); }
    void setVYT(KyVMInt* value)       { set(13, value); }
    void setFX(KyVMInt* value)        { set(14, value); }
    void setFY(KyVMInt* value)        { set(15, value); }
    void setHid(KyVMInt* value)       { set(16, value); }
    void setHidsOn(KyVMInt* value)    { set(17, value); }
    void setHidsOff(KyVMInt* value)   { set(18, value); }
    void setSides(KyVMInt* value)     { set(19, value); }
    void setWeight(KyVMInt* value)    { set(20, value); }
    void setHits(KyVMInt* value)      { set(21, value); }
    
    void draw() {
        KyVMBaseImage* src = getSrc();
        
        if (src == (KyVMBaseImage*)kyVMNull || !getHide()->isZero()) {
            return;
        }
        
        // TODO: return if off screen and skip below
        
        KyInt rotation = getRotation()->value;
        KyInt flipX    = getFlipX()->value;
        KyInt flipY    = getFlipY()->value;
        KyInt w        = src->w << 16;
        KyInt h        = src->h << 16;
        KyInt ox, oy;
        
        if (rotation == 1) {
            ox = h - getInset()->getInt(3);
            oy = getInset()->getInt(0);
        } else if (rotation == 2) {
            ox = w - getInset()->getInt(2);
            oy = h - getInset()->getInt(3);
        } else if (rotation == 3) {
            ox = getInset()->getInt(1);
            oy = w - getInset()->getInt(2);
        } else {
            ox = flipX ? (w-getInset()->getInt(2)) : getInset()->getInt(0);
            oy = flipY ? (h-getInset()->getInt(3)) : getInset()->getInt(1);
        }
        
        KyInt gflags = rotation << 2 | (flipX ? 1 : 0) | (flipY ? 2 : 0);
        
        src->draw((getBnds()->getX1()->value - ox)>>16,
                  (getBnds()->getY1()->value - oy)>>16,
                  gflags);
    }
};

class KyVMTilemap : public KyVMArray {
public:
    KyVMArray* getTileset() { return (KyVMArray*)get(0); }
    KyVMBlob*  getData()    { return (KyVMBlob*)get(1); }
    KyVMInt*   getW()       { return (KyVMInt*)get(2); }
    KyVMInt*   getH()       { return (KyVMInt*)get(3); }
    KyVMInt*   getTw()      { return (KyVMInt*)get(4); }
    KyVMInt*   getTh()      { return (KyVMInt*)get(5); }
    KyVMInt*   getX()       { return (KyVMInt*)get(6); }
    KyVMInt*   getY()       { return (KyVMInt*)get(7); }
    KyVMInt*   getPw()      { return (KyVMInt*)get(8); }
    KyVMInt*   getPh()      { return (KyVMInt*)get(9); }
    
    void setTileset(KyVMArray* value) { set(0, value); }
    void setData(KyVMBlob* value)     { set(1, value); }
    void setW(KyVMInt* value)         { set(2, value); }
    void setH(KyVMInt* value)         { set(3, value); }
    void setTw(KyVMInt* value)        { set(4, value); }
    void setTh(KyVMInt* value)        { set(5, value); }
    void setX(KyVMInt* value)         { set(6, value); }
    void setY(KyVMInt* value)         { set(7, value); }
    void setPw(KyVMInt* value)        { set(8, value); }
    void setPh(KyVMInt* value)        { set(9, value); }
    
    void draw(KyInt x, KyInt y, KyInt world_w, KyInt world_h, KyInt screen_w, KyInt screen_h) {
        KyInt scroll_x, scroll_y;
        
        if (world_w <= screen_w) {
            scroll_x = 0;
        } else {
            scroll_x = (KyInt)((KyLong)x*(getPw()->value-screen_w)/(world_w-screen_w));
        }
        
        if (world_h <= screen_h) {
            scroll_y = 0;
        } else {
            scroll_y = (KyInt)((KyLong)y*(getPh()->value-screen_h)/(world_h-screen_h));
        }
        
        scroll_x = clamp(scroll_x, 0, getPw()->value-screen_w);
        scroll_y = clamp(scroll_y, 0, getPh()->value-screen_h);
        
        KyInt tw = getTw()->value;
        KyInt th = getTh()->value;
        
        KyVMImage::drawMap((KyVMBaseImage**)getTileset()->sub(1)->data,
                           ((KyVMBlob*)getData())->data,
                           getW()->value,
                           getH()->value,
                           scroll_x/tw,
                           scroll_y/th,
                           screen_w/tw+1,
                           screen_h/th+1,
                           tw>>16,
                           th>>16,
                           (getX()->value+x-scroll_x)>>16,
                           (getY()->value+y-scroll_y)>>16);
    }
    
    static void setTilesetFrame(KyVMArray* ts, KyVMInt* t) {
        KyVMArray* tiles = ts->sub(1);
        KyVMArray* anims = ts->sub(2); 
        for (KyInt i = 0; i < anims->len; i++) {
            KyVMArray* anim = anims->sub(i);
            tiles->set(anim->getInt(0), tiles->get(anim->sub(1)->getInt(t->value%anim->sub(1)->len)));
        }
    }

    static void drawTileObj(KyVMArray* tileobj) {
        KyVMArray* ts    = tileobj->sub(0);
        KyVMArray* tiles = ts->sub(1);
        KyVMBaseImage* tileImage = (KyVMBaseImage*)tiles->get(tileobj->getInt(1));
        tileImage->drawStretched(tileobj->getInt(2)>>16,
                                 tileobj->getInt(3)>>16,
                                 tileobj->getInt(4)>>16,
                                 tileobj->getInt(5)>>16,
                                 tileobj->getInt(6));
    }
};

//
// DELEGATE
//

class OpenGLDelegate : public KyVMDelegate {
public:
    KyOpenGLView* view;
    KyVMCam* cam;
    KyInt scrollX, scrollY, virtualW, virtualH, worldW, worldH;
    
    OpenGLDelegate(KyOpenGLView* view) : KyVMDelegate() {
        this->view       = view;
        this->scrollX    = 0;
        this->scrollY    = 0;
        this->virtualW   = 0;
        this->virtualH   = 0;
        this->worldW     = 0;
        this->worldH     = 0;
        this->cam        = (KyVMCam*)(new KyVMCam())->ref();
    }
    
    ~OpenGLDelegate() {
        delete this->cam;
    }
    
    KyVMObject* eval(KyVM* vm, KyInt opcode, KyVMObject** args, KyInt argc) {
        [[view openGLContext] makeCurrentContext];
        
        switch (opcode) {
            case NATIVE_GINIT:
                glEnable(GL_TEXTURE_2D);
                glDisable(GL_LIGHTING);
                glShadeModel(GL_FLAT);
                glDisable(GL_CULL_FACE);
                glDisable(GL_DEPTH_TEST);
                glEnable (GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glClearColor(0xEF/255.0f, 0xEF/255.0f, 0xEF/255.0f, 1.0f);
                glClear(GL_COLOR_BUFFER_BIT);
                [view window].title = [NSString stringWithUTF8String:((KyVMStr*)(args[0]))->str];
                virtualW = ((KyVMInt*)args[1])->toCInt();
                virtualH = ((KyVMInt*)args[2])->toCInt();
                break;
            case NATIVE_GBEGIN:
            {
                glMatrixMode(GL_PROJECTION);
                glLoadIdentity();
                glOrtho(0.0f, virtualW, virtualH, 0.0f, -1.0f, 1.0f);
                
                // Fit the "virtual screen" to the OpenGL view
                // keeping original aspect ratio, centering inside the view
                KyInt   sw = virtualW, sh = virtualH;
                KyInt   dw = view.frame.size.width, dh = view.frame.size.height;
                KyFloat vx, vy, vw, vh;
                KyFloat ri = ((KyFloat)sw) / sh;           
                KyFloat rs = ((KyFloat)dw) / dh;
                if (rs > ri) {
                    vw = sw * dh / sh;
                    vh = dh;
                } else {
                    vw = dw;
                    vh = sh * dw / sw;
                }
                vx = (dw-vw) / 2;
                vy = (dh-vh) / 2;
                
                glViewport(vx, vy, vw, vh);
                glScissor(vx, vy, vw, vh);
                glEnable(GL_SCISSOR_TEST);
                
                glMatrixMode(GL_MODELVIEW);
                glLoadIdentity();
                
                throw KyDrawBegin();
            }
            case NATIVE_GEND:
            {
                glFlush();
                //[[view openGLContext] flushBuffer]; // enable for double buffering
                throw KyDrawEnd();
            }
            case NATIVE_GENABLE:
            {
                // TODO: use this to control drawing, if it ever seems necessary!
                // KyBool enable = ((KyVMInt*)args[0])->value;
                break;
            }
            case NATIVE_CLEAR: 
            {
                KyInt c = ((KyVMInt*)args[0])->value;
                glClearColor(((c>>16)&0xFF)/255.0f,
                             ((c>> 8)&0xFF)/255.0f,
                             ((c    )&0xFF)/255.0f,
                             255.0f);
                glClear(GL_COLOR_BUFFER_BIT);
                break;
            }
            case NATIVE_SETVIEWPORT:
            {
                scrollX = ((KyVMInt*)args[0])->toCInt();
                scrollY = ((KyVMInt*)args[1])->toCInt();
                glLoadIdentity();
                glScalef(1.0f/(1.0f-((KyVMInt*)args[2])->toCFloat()), 
                         1.0f/(1.0f-((KyVMInt*)args[2])->toCFloat()), 
                         1.0f);
                glTranslatef(-scrollX, -scrollY, 0.0f);
                break;
            }
            case NATIVE_PROGSTATE:
                return new KyVMInt([view programState]);
            case NATIVE_POLLKEYS:
            {
                KyVMBlob* keyStates = new KyVMBlob(128);
                @synchronized(view->keyStatesLock) {
                    for (KyInt i = 0; i < 128; i++) {
                        KyByte keyState = view->keyStates[i];
                        keyState |=  ((keyState<<2)&4);
                        keyState &= ~((keyState<<1)&4);
                        keyStates->data[i] = keyState;
                        keyStates->data[0]|= keyState;
                        view->keyStates[i] = keyState & 4;
                    }
                    KyInt mouseX = (KyInt)view.mouseLocation.x * virtualW / view.frame.size.width;
                    KyInt mouseY = (KyInt)view.mouseLocation.y * virtualH / view.frame.size.height;
                    mouseY = virtualH - mouseY; // y is inverted 
                    keyStates->data[0x3A] = mouseX>>8&0xFF;
                    keyStates->data[0x3B] = mouseX   &0xFF;
                    keyStates->data[0x3C] = mouseY>>8&0xFF;
                    keyStates->data[0x3D] = mouseY   &0xFF;
                }
                return keyStates;
            }
                
                //
                // Graphics - Image
                //
            case NATIVE_IMG:
            {
                KyChar* b64str = ((KyVMStr*)args[0])->str;
                KyInt   len    = Base64decode_len(b64str);
                KyByte* data   = (KyByte*)malloc(len);
                Ky_assert(Base64decode((KyChar*)data, b64str)<=len);
                return new KyVMImage(data, len);
            }
            case NATIVE_SUBIMGEX:
                return new KyVMSubImage(((KyVMBaseImage*)args[0]), 
                                        ((KyVMInt*)args[1])->toCInt(), 
                                        ((KyVMInt*)args[2])->toCInt(),
                                        ((KyVMInt*)args[3])->toCInt(), 
                                        ((KyVMInt*)args[4])->toCInt(), 
                                        ((KyVMInt*)args[5])->value, 
                                        ((KyVMInt*)args[6])->value);
            case NATIVE_IMGW:
                return new KyVMInt(((KyVMBaseImage*)args[0])->w<<16);
            case NATIVE_IMGH:
                return new KyVMInt(((KyVMBaseImage*)args[0])->h<<16);
            case NATIVE_DRAWEX:
                ((KyVMBaseImage*)args[0])->draw(((KyVMInt*)args[1])->toCInt(), 
                                                ((KyVMInt*)args[2])->toCInt(),
                                                ((KyVMInt*)args[3])->value);
                break;                
            case NATIVE_DRAWSUBEX:
                ((KyVMBaseImage*)args[0])->drawRegion(((KyVMInt*)args[1])->toCInt(),
                                                      ((KyVMInt*)args[2])->toCInt(),
                                                      ((KyVMInt*)args[5])->toCInt(), 
                                                      ((KyVMInt*)args[6])->toCInt(),
                                                      ((KyVMInt*)args[3])->toCInt(), 
                                                      ((KyVMInt*)args[4])->toCInt(),
                                                      ((KyVMInt*)args[5])->toCInt(), 
                                                      ((KyVMInt*)args[6])->toCInt(),
                                                      ((KyVMInt*)args[7])->value);
                break;
            case NATIVE_DRAWSTRETCHEDEX:
                ((KyVMBaseImage*)args[0])->drawStretched(((KyVMInt*)args[1])->toCInt(), 
                                                         ((KyVMInt*)args[2])->toCInt(),
                                                         ((KyVMInt*)args[3])->toCInt(), 
                                                         ((KyVMInt*)args[4])->toCInt(),
                                                         ((KyVMInt*)args[5])->value);
                break;
            case NATIVE_DRAWMAP:
            {
                KyInt tw = ((KyVMInt*)args[4])->toCInt(), 
                      th = ((KyVMInt*)args[5])->toCInt();                
                KyVMImage::drawMap((KyVMBaseImage**)((KyVMArray*)args[0])->data,
                                   ((KyVMBlob*)args[1])->data,
                                   ((KyVMInt*)args[2])->value,
                                   ((KyVMInt*)args[3])->value,
                                   scrollX/tw, 
                                   scrollY/th,
                                   virtualW/tw+1,
                                   virtualH/th+1,
                                   tw, th,
                                   ((KyVMInt*)args[6])->toCInt(),
                                   ((KyVMInt*)args[7])->toCInt());
                break;
            }
            case NATIVE_DRAWTEXT:
                KyVMImage::drawText((KyVMBaseImage**)((KyVMArray*)args[0])->data,
                                    (KyVMInt**)((KyVMArray*)args[1])->data,
                                    ((KyVMStr*)args[2])->str,
                                    ((KyVMInt*)args[3])->toCInt(),
                                    ((KyVMInt*)args[4])->toCInt(),
                                    ((KyVMInt*)args[5])->toCInt(),
                                    ((KyVMInt*)args[6])->toCInt(),
                                    (KyVMInt**)((KyVMArray*)args[7])->data);
                break;
                            
                //
                // Graphics - Rect / Collisions
                //
            case NATIVE_REMPTY:
            {
                KyVMRect* _a = ((KyVMRect*)args[0]);
                
                KyInt a[4] = {_a->getX1()->value, _a->getY1()->value, _a->getX2()->value, _a->getY2()->value};
                
                return Ky_toVMBool(rectEmpty(a));
            }
            case NATIVE_RINTER:
            {
                KyVMRect* _a = ((KyVMRect*)args[0]);
                KyVMRect* _b = ((KyVMRect*)args[1]);
                
                KyInt a[4] = {_a->getX1()->value, _a->getY1()->value, _a->getX2()->value, _a->getY2()->value};
                KyInt b[4] = {_b->getX1()->value, _b->getY1()->value, _b->getX2()->value, _b->getY2()->value};
                KyInt c[4];
                
                rectIntersection(c, a, b);
                
                return new KyVMRect(new KyVMInt(c[0]), 
                                    new KyVMInt(c[1]), 
                                    new KyVMInt(c[2]),
                                    new KyVMInt(c[3]));
            }
            case NATIVE_RUNION:
            {
                KyVMRect* _a = ((KyVMRect*)args[0]);
                KyVMRect* _b = ((KyVMRect*)args[1]);
                
                KyInt a[4] = {_a->getX1()->value, _a->getY1()->value, _a->getX2()->value, _a->getY2()->value};
                KyInt b[4] = {_b->getX1()->value, _b->getY1()->value, _b->getX2()->value, _b->getY2()->value};
                KyInt c[4];
                
                rectUnion(c, a, b);
                
                return new KyVMRect(new KyVMInt(c[0]), 
                                    new KyVMInt(c[1]), 
                                    new KyVMInt(c[2]),
                                    new KyVMInt(c[3]));
            }
            case NATIVE_RHITPOINT:
            {
                KyVMRect* _a = ((KyVMRect*)args[0]);
                
                KyInt x = ((KyVMInt*)args[1])->value;
                KyInt y = ((KyVMInt*)args[2])->value;
                
                KyInt a[4] = {_a->getX1()->value, _a->getY1()->value, _a->getX2()->value, _a->getY2()->value};
                
                return Ky_toVMBool(rectHitPoint(a, x, y));
            }
            case NATIVE_RHITSEGMENT:
            {
                KyVMRect* _a = ((KyVMRect*)args[0]);
                
                KyInt x1 = ((KyVMInt*)args[1])->value;
                KyInt y1 = ((KyVMInt*)args[2])->value;
                KyInt x2 = ((KyVMInt*)args[3])->value;
                KyInt y2 = ((KyVMInt*)args[4])->value;
                
                KyInt a[4] = {_a->getX1()->value, _a->getY1()->value, _a->getX2()->value, _a->getY2()->value};
                
                return Ky_toVMBool(rectHitSegment(a, x1, y1, x2, y2));
            }
            case NATIVE_RESOLVEHIT:
            {
                KyVMRect* _a1     = ((KyVMRect*)args[0]);
                KyVMRect* _a2     = ((KyVMRect*)args[1]);
                KyVMRect* _b      = ((KyVMRect*)args[2]);
                KyInt     aFlags  = ((KyVMInt*)args[3])->value;
                KyInt     bFlags  = ((KyVMInt*)args[4])->value;
              
                return new KyVMInt(_a2->hitRect(_a1, _b, aFlags, bFlags));
            }
            case NATIVE_RESOLVEMAPHIT:
            {
                KyVMRect* _a1     = ((KyVMRect*)args[0]);
                KyVMRect* _a2     = ((KyVMRect*)args[1]);
                KyByte*   mapData = ((KyVMBlob*)args[2])->data;
                KyByte*   tsFlags = ((KyVMBlob*)args[3])->data;
                KyInt     mapW    = ((KyVMInt*)args[4])->value;
                KyInt     mapH    = ((KyVMInt*)args[5])->value;
                KyInt     tw      = ((KyVMInt*)args[6])->value;
                KyInt     th      = ((KyVMInt*)args[7])->value;
                KyInt     aFlags  = ((KyVMInt*)args[8])->value;
                                
                return new KyVMInt(_a2->hitMap(_a1, mapData, tsFlags, mapW, mapH, tw, th, aFlags));
            }
                
                //
                // Sound
                //
            case NATIVE_WAV:
            {
                const KyVMStr* b64str = (KyVMStr*)args[0];
                KyChar* str = (KyChar*)malloc(Base64decode_len(b64str->str));
                return new KyVMSound((KyByte*)str, Base64decode(str, b64str->str));
            }
            case NATIVE_PLAYEX:
                ((KyVMSound*)args[0])->play( ((KyVMInt*)args[1])->toCFloat(), 
                                            !((KyVMInt*)args[2])->isZero());
                break;
            case NATIVE_STOP:
                ((KyVMSound*)args[0])->stop();
                break;
                
                //
                // Game
                //
            case NATIVE_SETGAMEPARAMS:
                worldW = ((KyVMInt*)args[0])->value;
                worldH = ((KyVMInt*)args[1])->value;
                cam->setBounds(kyVMZero,
                               kyVMZero,
                               new KyVMInt(worldW-(virtualW<<16)), 
                               new KyVMInt(worldH-(virtualH<<16)));
                break;
            case NATIVE_CAMSETPATH:
                cam->setPath((KyVMArray*)args[0]);
                break;
            case NATIVE_CAMADVANCE:
                cam->advance();
                break;
            case NATIVE_CAM:
                return cam;
            case NATIVE_DRAWSPRITE:
                ((KyVMSprite*)args[0])->draw();
                break;
            case NATIVE_DRAWTILEMAP:
                ((KyVMTilemap*)args[0])->draw(scrollX<<16, 
                                              scrollY<<16,
                                              worldW,
                                              worldH,
                                              virtualW<<16,
                                              virtualH<<16);
                break;                
            case NATIVE_DRAWTILEOBJ:
                KyVMTilemap::drawTileObj((KyVMArray*)args[0]);
                break;
            case NATIVE_SETTILESETFRAME:
                KyVMTilemap::setTilesetFrame((KyVMArray*)args[0], (KyVMInt*)args[1]);
                break;
            case NATIVE_MOVESPRITE:
            {
                KyVMSprite*  spr     = (KyVMSprite*)args[0];
                KyVMArray*   others  = (KyVMArray*)args[1];
                KyVMTilemap* tilemap = (KyVMTilemap*)args[2];
                
                KyInt        dx      = spr->getFX()->value * (spr->getAX()->value >= 0 
                    ? min(spr->getVX()->value+spr->getAX()->value, spr->getVXT()->value)
                    : max(spr->getVX()->value+spr->getAX()->value, 0));
                KyInt        dy      = spr->getFY()->value * (spr->getAY()->value >= 0 
                    ? min(spr->getVY()->value+spr->getAY()->value, spr->getVYT()->value)
                    : max(spr->getVY()->value+spr->getAY()->value, 0));
                KyVMRect*    oldBnds = spr->getOldBnds();
                KyVMRect*    bnds    = spr->getBnds();
                
                oldBnds->setX1(bnds->getX1());
                oldBnds->setY1(bnds->getY1());
                oldBnds->setX2(bnds->getX2());
                oldBnds->setY2(bnds->getY2());
                
                bnds->setX1(new KyVMInt(bnds->getX1()->value+dx));
                bnds->setY1(new KyVMInt(bnds->getY1()->value+dy));
                bnds->setX2(new KyVMInt(bnds->getX2()->value+dx));
                bnds->setY2(new KyVMInt(bnds->getY2()->value+dy));

                KyInt hits = 0;
                
                // Skip checking for hits (but still zero out .hits fields)
                // if hit checking is disabled or sprite didn't move
                KyInt hidsToCheck = spr->getHidsOn()->value & ~spr->getHidsOff()->value;
                KyInt skipCheck = hidsToCheck == 0;
                KyInt hflags = 0xF0000000 | spr->getWeight()->value | hidsToCheck | 0xF;
                
                if (!skipCheck) {
                    hits = bnds->hitMap(oldBnds,
                        ((KyVMBlob*)tilemap->getData())->data,
                        ((KyVMBlob*)tilemap->getTileset()->sub(3))->data,
                        tilemap->getW()->value,
                        tilemap->getH()->value,
                        tilemap->getTw()->value,
                        tilemap->getTh()->value,
                        hflags);
                }
                
                for (KyInt i = 0; i < others->len; i++) {
                    KyVMSprite* other = (KyVMSprite*)others->get(i);
                    KyInt otherHits;
                    if (skipCheck || !(spr->getHid()->value&(other->getHidsOn()->value&~other->getHidsOff()->value))) {
                        otherHits = 0;
                    } else {
                        KyInt otherHflags = other->getHid()->value | 
                            other->getSides()->value |
                            other->getWeight()->value;
                        otherHits = bnds->hitRect(oldBnds, other->getBnds(), hflags, otherHflags);
                    }
                    other->setHits(new KyVMInt(otherHits));
                    hits |= otherHits;
                }
                
                spr->setHits(new KyVMInt(hits));
                spr->setVX(new KyVMInt((bnds->getX1()->value-oldBnds->getX1()->value)*spr->getFX()->value));
                spr->setVY(new KyVMInt((bnds->getY1()->value-oldBnds->getY1()->value)*spr->getFY()->value));
                
                break;
            }
            default:
                throw KyRuntimeError("Operation not supported");
                
        }
        return NULL;
    }
};

@implementation KyOpenGLView

@synthesize programState, keyStatesLock, keyModifierFlags, mouseLocation;

- (void)applicationDidFinishLaunching:(NSNotification *)notification {
    updateTimer   = nil;
    vmState       = KY_VM_STATE_READY;
    programState  = KY_PROGRAM_STATE_ACTIVE;
    keyStatesLock = [NSLock new];
    
    [[self window] setAcceptsMouseMovedEvents:YES];
    
    KyChar tempPath[PATH_MAX];
    [[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"main.jam"] ofType:nil]
     getCString:tempPath maxLength:PATH_MAX encoding:NSASCIIStringEncoding];
    
    KyChar* jamcode = readWholeTextFile(tempPath);
    
    vm = KyVM::fromJamcode(jamcode);
    vm->setDelegate(new OpenGLDelegate(self));
    
    free(jamcode);
    
    try {
        vm->run();
        vmState = KY_VM_STATE_FINISHED;
    } catch(const KySleep& e) {
        vmState = KY_VM_STATE_UPDATING;
        updateTimer = [NSTimer timerWithTimeInterval:e.t/1000.0
                                              target:self
                                            selector:@selector(timerFired:)
                                            userInfo:nil
                                             repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:updateTimer
                                     forMode:NSDefaultRunLoopMode];
        return;
    } catch(const KyDrawBegin& e) {
        vmState = KY_VM_STATE_DRAWING;
        [self setNeedsDisplay:YES];
        return;
    }
    
    [[self window] close];
}

- (void)applicationWillTerminate:(NSNotification *)notification {
    programState = KY_PROGRAM_STATE_DESTROYED;
}

- (void)applicationWillBecomeActive:(NSNotification *)notification {
    programState = KY_PROGRAM_STATE_ACTIVE;
}

- (void)applicationWillResignActive:(NSNotification *)notification {
    programState = KY_PROGRAM_STATE_INACTIVE;
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)application {
    return YES;
}

- (BOOL)acceptsFirstResponder {
    return YES;
}

- (void)setKeyState:(unsigned short)keyCode state:(KyByte)state {
    KyInt key;
    switch (keyCode) {
        case kVK_LeftArrow:     key = 0x25; break;
        case kVK_UpArrow:       key = 0x26; break;
        case kVK_RightArrow:    key = 0x27; break;
        case kVK_DownArrow:     key = 0x28; break;
        case kVK_Delete:        key = 0x2E; break;
        case kVK_ForwardDelete: key = 0x08; break;
        case kVK_Tab:           key = 0x09; break;
        case kVK_Return:        key = 0x0D; break;
        case kVK_Shift:         key = 0x10; break;
        case kVK_RightShift:    key = 0x10; break;
        case kVK_Control:       key = 0x11; break;
        case kVK_RightControl:  key = 0x11; break;
        case kVK_Option:        key = 0x12; break;
        case kVK_RightOption:   key = 0x12; break;
        case kVK_Escape:        key = 0x1B; break;
        case kVK_Space:         key = 0x20; break;
        case kVK_ANSI_A:        key = 'A'; break;
        case kVK_ANSI_B:        key = 'B'; break;
        case kVK_ANSI_C:        key = 'C'; break;
        case kVK_ANSI_D:        key = 'D'; break;
        case kVK_ANSI_E:        key = 'E'; break;
        case kVK_ANSI_F:        key = 'F'; break;
        case kVK_ANSI_G:        key = 'G'; break;
        case kVK_ANSI_H:        key = 'H'; break;
        case kVK_ANSI_I:        key = 'I'; break;
        case kVK_ANSI_J:        key = 'J'; break;
        case kVK_ANSI_K:        key = 'K'; break;
        case kVK_ANSI_L:        key = 'L'; break;
        case kVK_ANSI_M:        key = 'M'; break;
        case kVK_ANSI_N:        key = 'N'; break;
        case kVK_ANSI_O:        key = 'O'; break;
        case kVK_ANSI_P:        key = 'P'; break;
        case kVK_ANSI_Q:        key = 'Q'; break;
        case kVK_ANSI_R:        key = 'R'; break;
        case kVK_ANSI_S:        key = 'S'; break;
        case kVK_ANSI_T:        key = 'T'; break;
        case kVK_ANSI_U:        key = 'U'; break;
        case kVK_ANSI_V:        key = 'V'; break;
        case kVK_ANSI_W:        key = 'W'; break;
        case kVK_ANSI_X:        key = 'X'; break;
        case kVK_ANSI_Y:        key = 'Y'; break;
        case kVK_ANSI_Z:        key = 'Z'; break;
        case kVK_ANSI_0:        key = '0'; break;
        case kVK_ANSI_1:        key = '1'; break;
        case kVK_ANSI_2:        key = '2'; break;
        case kVK_ANSI_3:        key = '3'; break;
        case kVK_ANSI_4:        key = '4'; break;
        case kVK_ANSI_5:        key = '5'; break;
        case kVK_ANSI_6:        key = '6'; break;
        case kVK_ANSI_7:        key = '7'; break;
        case kVK_ANSI_8:        key = '8'; break;
        case kVK_ANSI_9:        key = '9'; break;   
        case kVK_F1:            key = 0x70; break;
        case kVK_F2:            key = 0x71; break;
        case kVK_F3:            key = 0x72; break;
        case kVK_F4:            key = 0x73; break;
        case kVK_F5:            key = 0x74; break;
        case kVK_F6:            key = 0x75; break;
        case kVK_F7:            key = 0x76; break;
        case kVK_F8:            key = 0x77; break;
        case kVK_F9:            key = 0x78; break;
        case kVK_F10:           key = 0x79; break;
        case kVK_F11:           key = 0x7A; break;
        case kVK_F12:           key = 0x7B; break;
        case kVK_F13:           key = 0x7C; break;
        case kVK_F14:           key = 0x7D; break;
        case kVK_F15:           key = 0x7E; break;
        default:
            key = 0;
            break;
    }
    @synchronized(keyStatesLock) {
        keyStates[key] |= state;
        keyStates[0]   |= state;
    }
}

- (void)keyDown:(NSEvent *)event
{
    if ([event isARepeat]) { return; }
    [self setKeyState:[event keyCode] state:1];
}

- (void)keyUp:(NSEvent *)event
{
    if ([event isARepeat]) { return; }
    [self setKeyState:[event keyCode] state:2];
}

- (void)mouseMoved:(NSEvent *)event
{
    @synchronized(keyStatesLock) {
        mouseLocation = [self convertPoint:[event locationInWindow] fromView:nil];
    }
}

- (void)mouseDragged:(NSEvent *)event
{
    @synchronized(keyStatesLock) {
        mouseLocation = [self convertPoint:[event locationInWindow] fromView:nil];
    }
}

- (void)mouseDown:(NSEvent *)event
{
    @synchronized(keyStatesLock) {
        keyStates[0x01] |= 1;
    }
}

- (void)mouseUp:(NSEvent *)event
{
    @synchronized(keyStatesLock) {
        keyStates[0x01] |= 2;
    }
}

- (void)rightMouseDown:(NSEvent *)event
{
    @synchronized(keyStatesLock) {
        keyStates[0x02] |= 1;
    }
}

- (void)rightMouseUp:(NSEvent *)event
{
    @synchronized(keyStatesLock) {
        keyStates[0x02] |= 2;
    }
}

- (void)flagsChanged:(NSEvent *)event
{
    [self setKeyState:[event keyCode] 
                state:(([event modifierFlags]^[self keyModifierFlags])&[event modifierFlags])?1:2];
    self.keyModifierFlags = [event modifierFlags]; // track state of modifier flags for diff'ing
}

- (void)timerFired:(id)sender
{
    @synchronized(self) {
        if (vmState != KY_VM_STATE_UPDATING) {
            return;
        }
        
        try {
            vm->resume();
            vmState = KY_VM_STATE_FINISHED;
        } catch(const KySleep& e) {
            vmState = KY_VM_STATE_UPDATING;
            updateTimer = [NSTimer timerWithTimeInterval:e.t/1000.0
                                                  target:self
                                                selector:@selector(timerFired:)
                                                userInfo:nil
                                                 repeats:NO];
            [[NSRunLoop currentRunLoop] addTimer:updateTimer
                                         forMode:NSDefaultRunLoopMode];
            return;
        } catch(const KyDrawBegin& e) {
            vmState = KY_VM_STATE_DRAWING;
            [self setNeedsDisplay:YES];
            return;
        }
        
        [[self window] close];
    }
}

- (void)prepareOpenGL
{
    GLint swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
}

- (void)drawRect:(NSRect)bounds
{
    @synchronized(self) {
        if (vmState != KY_VM_STATE_DRAWING) {
            return;
        }
        
        try {
            vm->resume();
            vmState = KY_VM_STATE_FINISHED;
        } catch(const KyDrawEnd& e) {
            vmState = KY_VM_STATE_UPDATING;
            updateTimer = [NSTimer timerWithTimeInterval:0
                                                  target:self
                                                selector:@selector(timerFired:)
                                                userInfo:nil
                                                 repeats:NO];
            [[NSRunLoop currentRunLoop] addTimer:updateTimer
                                         forMode:NSDefaultRunLoopMode];
            return;
        }
        
        [[self window] close];
    }
}

@end
