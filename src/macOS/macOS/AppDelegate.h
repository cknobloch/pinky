//
//  AppDelegate.h
//  macOS
//
//  Created by Curt Knobloch on 10/18/16.
//  Copyright © 2016 Curt Knobloch. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@end

