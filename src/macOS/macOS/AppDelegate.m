//
//  AppDelegate.m
//  macOS
//
//  Created by Curt Knobloch on 10/18/16.
//  Copyright © 2016 Curt Knobloch. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()


@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)notification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)notification {
    // Insert code here to tear down your application
}

- (void)applicationWillBecomeActive:(NSNotification *)notification {
    //programState = KY_PROGRAM_STATE_ACTIVE;
}

- (void)applicationWillResignActive:(NSNotification *)notification {
    //programState = KY_PROGRAM_STATE_PAUSED;
}


@end
