//
//  MyOpenGLView.h
//  macOS
//
//  Created by Curt Knobloch on 10/18/16.
//  Copyright © 2020 Curt Knobloch. All rights reserved.
//

#ifndef KyOpenGLView_h
#define KyOpenGLView_h

#import <Cocoa/Cocoa.h>

#include "Pinky.hpp"


enum KyVMState {
    KY_VM_STATE_READY,
    KY_VM_STATE_UPDATING,
    KY_VM_STATE_DRAWING,
    KY_VM_STATE_FINISHED
};

enum KyProgramState {
    KY_PROGRAM_STATE_DESTROYED,
    KY_PROGRAM_STATE_ACTIVE,
    KY_PROGRAM_STATE_INACTIVE
};

@interface KyOpenGLView : NSOpenGLView<NSApplicationDelegate>
{
    CVDisplayLinkRef     displayLink;
    NSTimer*             updateTimer;
    KyVM*                vm;
    KyVMState            vmState;
    KyProgramState       programState;
    @public
    KyByte               keyStates[128];
    NSLock*              keyStatesLock;
    NSEventModifierFlags keyModifierFlags;
    NSPoint              mouseLocation;
}

@property (readonly, atomic) KyProgramState programState;
@property (atomic) NSLock* keyStatesLock;
@property (atomic) NSEventModifierFlags keyModifierFlags;
@property (atomic) NSPoint mouseLocation;

// @property (weak) IBOutlet NSWindow *window;

- (void)setKeyState:(unsigned short)keyCode state:(KyByte)state;

@end

#endif /* KyOpenGLView_h */
