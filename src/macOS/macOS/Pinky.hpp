//
//  Pinky.hpp
//  macOS
//
//  Created by Curt Knobloch on 10/18/16.
//  Copyright © 2016 Curt Knobloch. All rights reserved.
//

#ifndef Pinky_hpp
#define Pinky_hpp

#include <sys/time.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//
// UTILITY
//

#define _MAKE_INT(a, b, c, d) (((a))<<24|((b)&0xFF)<<16|((c)&0xFF)<<8|((d)&0xFF))
#define _MAKE_SHORT(a, b) (((a)&0xFF)<<8|((b)&0xFF))
#define _MAKE_BE_INT(a, b, c, d) _MAKE_INT(a, b, c, d)
#define _MAKE_BE_SHORT(a, b)     _MAKE_SHORT(a, b)
#define _MAKE_LE_INT(a, b, c, d) _MAKE_INT(d, c, b, a)
#define _MAKE_LE_SHORT(a, b)     _MAKE_SHORT(b, a)

#define MAKE_INT   _MAKE_BE_INT
#define MAKE_SHORT _MAKE_BE_SHORT

#ifndef MIN
#define MIN(a,b) ((a)<=(b)?(a):(b))
#define MAX(a,b) ((a)>=(b)?(a):(b))
#endif

#define FALSE 0
#define TRUE  1

#define MIN_INT -0x80000000
#define MAX_INT  0x7FFFFFFF


//
// PRIMITIVE TYPES (USE THESE TYPES AND ONLY THESE TYPES IN PINKY CODE!)
//

typedef char                KyChar;
typedef unsigned char       KyByte;
typedef short int           KyShort;
typedef int                 KyInt;
typedef unsigned int        KyUInt;
typedef long long           KyLong;
typedef unsigned long long  KyULong;
typedef double              KyFloat;
typedef int                 KyBool;

//
// PINKY CONSTANTS 
// (print ''.join(hex(ord(c))[2:] for c in "xxxx"[::-1]))
//

#define JAM_NOP  0x20706f6e
#define JAM_NULL 0x6c6c756e
#define JAM_ZERO 0x6f72657a
#define JAM_ONE  0x20656e6f
#define JAM_LODI 0x69646f6c
#define JAM_LODS 0x73646f6c
#define JAM_LODF 0x66646f6c
#define JAM_ADD  0x2020202b
#define JAM_SUB  0x2020202d
#define JAM_MUL  0x2020202a
#define JAM_DIV  0x2020202f
#define JAM_MULF 0x2020662a
#define JAM_DIVF 0x2020662f
#define JAM_NOT  0x20746f6e
#define JAM_NEG  0x2067656e
#define JAM_LT   0x2020203c
#define JAM_GT   0x2020203e
#define JAM_LTE  0x20203d3c
#define JAM_GTE  0x20203d3e
#define JAM_EQ   0x20203d3d
#define JAM_NE   0x20203d21
#define JAM_JMP  0x20706d6a
#define JAM_JIF  0x2066696a
#define JAM_JZ   0x20207a6a
#define JAM_JSR  0x2072736a
#define JAM_RET  0x20746572
#define JAM_MARK 0x6b72616d
#define JAM_STEP 0x70657473
#define JAM_CLR  0x20726c63
#define JAM_DUP  0x20707564
#define JAM_DROP 0x706f7264
#define JAM_SETA 0x61746573
#define JAM_SETI 0x69746573
#define JAM_SETL 0x6c746573
#define JAM_SETG 0x67746573
#define JAM_GETA 0x61746567
#define JAM_GETI 0x69746567
#define JAM_GETL 0x6c746567
#define JAM_GETG 0x67746567
#define JAM_NEW  0x2077656e
#define JAM_NEWV 0x7677656e
#define JAM_REM  0x20202023
#define JAM_EVAL 0x6c617665
#define JAM_ARGS 0x73677261

enum {
    VM_NOP = 0,
    VM_NULL,
    VM_ZERO,
    VM_ONE,
    VM_LODI,
    VM_LODS,
    VM_LODF,
    VM_ADD,
    VM_SUB,
    VM_MUL,
    VM_DIV,
    VM_MULF,
    VM_DIVF,
    VM_NOT,
    VM_NEG,
    VM_LT,
    VM_GT,
    VM_LTE,
    VM_GTE,
    VM_EQ,
    VM_NE,
    VM_JMP,
    VM_JIF,
    VM_JZ,
    VM_JSR,
    VM_RET,
    VM_DUP,
    VM_DROP,
    VM_SETA,
    VM_SETI,
    VM_SETL,
    VM_SETG,
    VM_GETA,
    VM_GETI,
    VM_GETL,
    VM_GETG,
    VM_NEW,
    VM_NEWV,
    VM_REM,
    VM_EVAL,
    VM_MARK,
    VM_STEP,
    VM_CLR,
    VM_ARGS,
    VM_ASSERT,
    VM_PRINT,
    VM_TIME,
    VM_RAND,
    VM_SEED,
    VM_WEAK,
    VM_IS,
    VM_DEFAULT,
    VM_SELECT,
    VM_SLEEP,
    VM_MACRO,
    VM_MACROSTEP,
    VM_GETTYPE,
    VM_CLONE,
    VM_ATTRS,
    VM_AND,
    VM_OR,
    VM_XOR,
    VM_SHL,
    VM_SHR,
    VM_ROT,
    VM_BITS,
    VM_SETBITS,
    VM_SIGNED,
    VM_MOD,
    VM_MIN,
    VM_MAX,
    VM_ABS,
    VM_ICMP,
    VM_ITOS,
    VM_FMOD,
    VM_FMIN,
    VM_FMAX,
    VM_FABS,
    VM_FCMP,
    VM_FTOS,
    VM_FTOI,
    VM_ITOF,
    VM_FLOOR,
    VM_CEIL,
    VM_FRAC,
    VM_POW,
    VM_SQRT,
    VM_HYPOT,
    VM_SIN,
    VM_COS,
    VM_TAN,
    VM_ASIN,
    VM_ACOS,
    VM_ATAN,
    VM_CONCAT,
    VM_CHR,
    VM_ORD,
    VM_SLEN,
    VM_SCMP,
    VM_STOI,
    VM_DECODEB64,
    VM_B64,
    VM_SUBSTR,
    VM_SLICE,
    VM_REVERSE,
    VM_FILL,
    VM_COPY,
    VM_SORT,
    VM_APPLY,
    VM_ALEN,
    VM_ARRAY,
    VM_BLOB,
    VM_BLOAD,
    VM_BSAVE,
    VM_BLEN,
    VM_GET,
    VM_SET,
    VM_GETS,
    VM_SETS
};


//
// PINKY CLASSES
//

class KyVM;

class KyException {
public:
    const KyChar* msg;
    KyException(const KyChar* msg="") {
        this->msg = msg;
    }
};

class KyError               : public KyException { public: KyError(const KyChar* m="") : KyException(m) {} };
class KyRuntimeError        : public KyError { public: KyRuntimeError(const KyChar* m="") : KyError(m) {} };
class KyNotImplementedError : public KyError { public: KyNotImplementedError (const KyChar* m="") : KyError(m) {} };
class KyAssertionError      : public KyError { public: KyAssertionError(const KyChar* m="") : KyError(m) {} };

class KyInterrupt           : public KyException { public: KyInterrupt() {} };
class KySleep               : public KyInterrupt { public: KyInt t; KySleep(KyInt t) : t(t) {} };
class KyDrawBegin           : public KyInterrupt { public: KyDrawBegin() {} };
class KyDrawEnd             : public KyInterrupt { public: KyDrawEnd() {} };
class KyEscapeException     : public KyInterrupt { public: KyEscapeException() {} };

class KyVMObject {
public:
    KyInt rc;
    
    KyVMObject();
    virtual ~KyVMObject();
    
    virtual KyInt typeID() { return 0; }
    virtual KyBool is(KyVMObject* other);
    virtual KyVMObject* assign(KyVMObject* other);
    virtual KyVMObject* ref();
    virtual void release();
    virtual void free();
    
    static void releaseMany(KyVMObject** objs, KyInt n) {
        while (n) { objs[--n]->release(); }
    }
};

class KyVMNull : public KyVMObject {
public:
    KyVMObject* ref();
    void release();
};

class KyVMInt : public KyVMObject {
public:
    KyInt value;
    KyVMInt(KyInt value);
    KyInt typeID() { return 1; }
    KyVMInt* cmp(KyVMInt* other);
    KyBool isZero();
    static KyVMInt* fromCFloat(KyFloat v);
    KyFloat toCFloat();
    KyInt toCInt();
};

class KyVMStr : public KyVMObject {
public:
    KyChar* str;
    KyInt len;
    KyVMStr(KyChar* str);
    KyVMStr(KyInt capacity);
    KyVMStr* slice(KyInt i, KyInt len);
    KyInt typeID() { return 2; }
    void free();
};

class KyVMConstStr : public KyVMStr {
public:
    KyVMConstStr(const KyChar* str);
    void free();
};

class KyVMArray : public KyVMObject {
public:
    KyVMObject** data;
    KyInt len;
    KyVMArray(KyVMObject** data, KyInt len);
    KyVMArray(KyInt len);
    KyInt typeID() { return 3; }
    KyBool is(KyVMObject* other);
    void free();
    KyVMObject* set(KyInt i, KyVMObject* value);
    KyVMObject* get(KyInt i);
    KyInt setInt(KyInt i, KyInt value);
    KyInt getInt(KyInt i);
    KyVMArray* sub(KyInt i);
    KyVMArray* slice(KyInt i, KyInt len);
    KyVMArray* reverse();
    void fill(KyVMObject* x, KyInt start, KyInt len);
    void copy(KyVMObject** data, KyInt start, KyInt len);
    static KyVMArray* create(KyInt len, KyVMObject** initialData=NULL);
};

class KyVMWeakArray : public KyVMArray {
public:
    KyVMWeakArray(const KyVMArray* array);
    KyVMObject* ref();
    void release();
};

class KyVMBlob : public KyVMObject {
public:
    KyByte* data;
    KyInt len;
    KyVMBlob(KyInt n);
    KyInt typeID() { return 4; }
    void free();
};

class KyVMProc : public KyVMObject {
public:
    KyInt addr, vn, p;
    KyVMProc(KyInt addr, KyInt vn);
    KyInt typeID() { return 5; }
};

class KyVMFrame {
public:
    KyVMObject** vars;
    KyInt        ip, vn, lineNo;
    KyVMProc*    proc;
    KyBool       isTopLevel;
    
    ~KyVMFrame();
    
    void reset(KyInt ip, KyVMObject** vars, KyInt vn, KyVMObject** args, KyInt argc);
    void resetWithProc(KyInt ip, KyVMObject** vars, KyInt vn, KyVMObject** args, KyInt argc, KyVMProc* call);
    KyVMObject* setVar(KyInt i, KyVMObject* value);
    KyVMObject* getVar(KyInt i);
    void free();
};

class KyVMDelegate {
public:
    virtual ~KyVMDelegate() {}
    virtual KyVMObject* eval(KyVM* vm, KyInt opcode, KyVMObject** args, KyInt argc) = 0;
};

class KyVM {
protected:
    KyVM();
    
public:
    KyVMDelegate* delegate;
    KyByte*       code;
    KyInt*        cmdOffsets;
    KyVMObject*   gv[65536];
    KyVMObject*   os[500];
    KyVMFrame     fs[100];
    KyInt         osi, fsi;
    KyInt         fileOffsets[8];
    KyChar        fileNames[8][30];
    KyInt         globalCount;
    KyInt         seed;
    long long     startTime;

    virtual ~KyVM();
    
    void setDelegate(KyVMDelegate* delegate);
    void run();
    void voidCall(KyVMProc* proc, KyVMObject** args, KyInt argc);
    KyVMObject* call(KyVMProc* proc, KyVMObject** args, KyInt argc);
    void resume();
    void evalWhileFrame();
    KyInt evalLine();
    void pushFrame(KyInt ip, KyInt vn, KyVMObject** args, KyInt argc);
    void pushFrameWithProc(KyInt ip, KyInt vn, KyVMObject** args, KyInt argc, KyVMProc* proc);
    KyBool dropFrame();
    KyVMFrame* topFrame(KyInt i=-1);
    void push(KyVMObject* obj);
    KyVMObject* pop();
    void drop();
    const KyChar* findFileName(KyInt lineNo);
    void printStackTrace();
    virtual void cleanup();
    
    KyVMInt*   popInt()   { return (KyVMInt*)  pop(); }
    KyVMStr*   popStr()   { return (KyVMStr*)  pop(); }
    KyVMArray* popArray() { return (KyVMArray*)pop(); }
    KyVMBlob*  popBlob()  { return (KyVMBlob*) pop(); }
    
    static KyVM* fromJamcode(const KyChar* jamcode);
};

#ifdef __cplusplus
extern "C" {
#endif
    
    KyInt max(int a, int b);
    KyInt min(int a, int b);
    
    void Ky_assert(KyBool x);
    
    KyVMInt* Ky_toVMBool(KyBool x);
    KyVMObject* Ky_nullable(KyVMObject* x);
    
    extern KyVMNull* kyVMNull;
    extern KyVMInt*  kyVMZero;
    extern KyVMInt*  kyVMOne;
    extern KyVMInt*  kyVMNegOne;
    extern KyVMInt*  kyVMMinInt;
    extern KyVMInt*  kyVMMaxInt;
    extern KyVMStr*  kyVMEmptyStr;
    
#ifdef __cplusplus
}
#endif

#endif /* Pinky_hpp */
