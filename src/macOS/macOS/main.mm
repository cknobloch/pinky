//
//  main.m
//  macOS
//
//  Created by Curt Knobloch on 10/18/16.
//  Copyright © 2016 Curt Knobloch. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#include "Pinky.hpp"


int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
