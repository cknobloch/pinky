var Pinky = (function(pinky) {
    /** 
     * 
     * Utilities
     *
     */
    
    // Source: http://www.quirksmode.org/js/cookies.html
    function createCookie(a,b,c){if(c){var d=new Date;d.setTime(d.getTime()+24*c*60*60*1e3);var e="; expires="+d.toGMTString()}else var e="";document.cookie=a+"="+b+e+"; path=/"}function readCookie(a){for(var b=a+"=",c=document.cookie.split(";"),d=0;d<c.length;d++){for(var e=c[d];" "==e.charAt(0);)e=e.substring(1,e.length);if(0==e.indexOf(b))return e.substring(b.length,e.length)}return null}function eraseCookie(a){createCookie(a,"",-1)}

    var _nullArray = arrayFill(new Array(1024), null, 0, 1024);
    function createNullArray(capacity) {
        return capacity <= _nullArray.length
            ? _nullArray.slice(0, capacity)
            : arrayFill(new Array(capacity), null, 0, capacity);
    }
    
    function arrayPeek(a) {
        // Note that all usages of this in core vm ops have
        // been inlined for performance
        return a[a.length-1];
    }

    function arrayExtend(a, other) {
        for (var i = 0; i < other.length; i++) {
            a.push(other[i]);
        }
        return a;
    }

    function arrayFill(a, value, i, n) {
        for (var end = i+n; i < end; i++) {
            a[i] = value;
        }
        return a;
    }
    
    function arrayCopy(a, other, i, n) {
        for (var j = 0; j < n; j++) {
            a[i+j] = other[j];
        }
        return a;
    }

    function arrayToStr(array) {
        var out = "";
	    for (var i = 0; i < array.length; i++) {
		    out += String.fromCharCode(array[i]);
	    }
        return out;
    }

    function strToByteArray(str) {
        var len = str.length;
        var arrayBuffer = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            arrayBuffer[i] = str.charCodeAt(i);
        }
        return arrayBuffer.buffer;
    }

    function createBlob(capacity) {
        return new Uint8Array(capacity);
    }

    function createBlobFromB64Data(b64Data) {
        if (b64Data) {
            var data = window.atob(b64Data);
            var blob = createBlob(data.length);
            blobSetStr(blob, 0, data, data.length);
            return blob;
        } else {
            return null;
        }
    }
    
    function createBlobFromCookie(k) {
        return createBlobFromB64Data(readCookie(k))
    }

    function blobGetStr(data, i, n) {
        return arrayToStr(data.slice(i, i+n));
    }
    
    function blobSetByte(data, i, v) {
        data[i] = v & 0xFF;
    }

    function blobSetStr(data, i, s, n) {
        for (var j = 0; j < s.length; j++) {
            data[i+j] = s.charCodeAt(j) & 0xFF;
        }
    }

    function BaseObject() {}
    
    function extendClass(parentClass, subClass, prototype) {
        subClass.prototype = Object.create(parentClass.prototype);
        subClass.prototype.constructor = subClass;
        for (var k in prototype) {
            subClass.prototype[k] = prototype[k];
        }
        return subClass;
    }

    function toInt(x) {
        return x & 0xFFFFFFFF;
    }

    var _BIT_POS = [0, 0, 1, 26, 2, 23, 27, 0, 3, 16, 24, 30, 28, 11, 0, 13, 4, 7, 17, 0, 25, 22, 31, 15, 29, 10, 12, 6, 0, 21, 14, 9, 5, 20, 8, 19, 18];
    
    function ffb(x) {
        return _BIT_POS[(x&-x)%37];
    }

    function clamp(x, a, b) {
        return Math.min(b, Math.max(a, x));
    }

    function fpMul(a, b) {
        return toInt((a*b)/65536);
    }

    function fpDiv(a, b) {
        return toInt((a*65536)/b);
    }

    function fpHypot(a, b) {
        return toInt(Math.hypot(a/65536.0, b/65536.0)*65536);
    }

    function isDigit(text, i) {
        var c = text.charCodeAt(i);
        return c >= 0x30 && c <= 0x39;
    }

    function rectEmpty(a) {
        return a[0] >= a[2] || a[1] >= a[3];
    }

    function rectIntersects(a, b) {
        return a[0] < b[2] && a[2] > b[0] && a[1] < b[3] && a[3] > b[1];
    }

    function rectIntersection(a, b) {
        return [
            Math.max(a[0], b[0]),
            Math.max(a[1], b[1]),
            Math.min(a[2], b[2]),
            Math.min(a[3], b[3])
        ];
    }
    
    function rectUnion(a, b) {
        return [
            Math.min(a[0], b[0]),
            Math.min(a[1], b[1]),
            Math.max(a[2], b[2]),
            Math.max(a[3], b[3])
        ];
    }

    function rectSeparationFlags(a, b) {
        return (((a[1] >= b[3]) ? 8 : 0) |
                ((a[2] <= b[0]) ? 4 : 0) |
                ((a[3] <= b[1]) ? 2 : 0) |
                ((a[0] >= b[2]) ? 1 : 0));
    }

    function rectHitPoint(a, x, y) {
        return a[0] <= x && a[2] > x && a[1] <= y && a[3] > y;
    }

    function rectHitSegment(a, x1, y1, x2, y2) {
        var ax1 = a[0],
            ay1 = a[1],
            ax2 = a[2],
            ay2 = a[3];

        if ((x1 <  ax1 && x2 <  ax1) ||
            (y1 <  ay1 && y2 <  ay1) ||
            (x1 >= ax2 && x2 >= ax2) ||
            (y1 >= ay2 && y2 >= ay2)) {
            return false;
        }

        var dx = x2 - x1;
        var dy = y2 - y1;

        if (dx) {
            var m = dy / dx, y;
            y = y1 + m * (ax1 - x1);
            if (y >= ay1 && y < ay2) return true;
            y = y1 + m * (ax2 - x1);
            if (y >= ay1 && y < ay2) return true;
        }
        
        if (dy) {
            var m = dx / dy, x;
            x = x1 + m * (ay1 - y1);
            if (x >= ax1 && x < ax2) return true;  
            x = x1 + m * (ay2 - y1);
            if (x >= ax2 && x < ax2) return true;
        }
        
        return false;
    }
    
    function rectHitRect(a1, a2, b, aFlags, bFlags) {
        var aTypes = aFlags & 0xFFFF0
        var bType  = bFlags & 0xFFFF0
        var cTypes = aTypes & bType
        if (!cTypes) {
            return 0;
        }

        var inter  = rectIntersection(a2, b);
        if (rectEmpty(inter)) {
            return rectEmpty(rectIntersection(a1, b))
                ? (0)  // no hit/touch/unhit
                : (0x40000000&aFlags) | cTypes  // a "unhit" b
            ;
        }
        
        var aMass      = aFlags & 0xF00000;
        var bMass      = bFlags & 0xF00000;
        var cMass      = aMass && bMass;
        
        var bSideFlags = ((bFlags>>2)&3) | ((bFlags<<2)&12);
        var prevSeparationFlags = rectSeparationFlags(a1, b);
        var touchFlag  = prevSeparationFlags ? 0 : (0x80000000&aFlags);
        var cSideFlags = prevSeparationFlags & aFlags & bSideFlags;
        
        // Displace if has physical sides/barriers and has mass (otherwise it's a ghost thing)
        if (cSideFlags && cMass) {
            var s;
            if (aMass == 0xF00000) {
                s = 0.0;
            } else if (bMass == 0xF00000) {
                s = 1.0;
            } else {
                s = Math.max(
                    Math.min(
                        0.5+((bMass-aMass)>>12)/7.0,
                        1.0),
                    0.0);
            }

            if (cSideFlags&~0xA) {
                var dx   = a2[0] - a1[0];
                var nx   = dx / Math.abs(dx);
                var dx1  = inter[2] - inter[0];
                var a_dx = nx * toInt(dx1*(s));
                var b_dx = nx * toInt(Math.ceil(dx1*(1.0-s)));
                a2[0] -= a_dx;
                a2[2] -= a_dx;
                b [0] += b_dx;
                b [2] += b_dx;
            } else {
                var dy   = a2[1] - a1[1]
                var ny   = dy / Math.abs(dy);
                var dy1  = inter[3] - inter[1];
                var a_dy = ny * toInt(dy1*(s));
                var b_dy = ny * toInt(Math.ceil(dy1*(1.0-s)));
                a2[1] -= a_dy;
                a2[3] -= a_dy;
                b [1] += b_dy;
                b [3] += b_dy;
            }
        }

        return touchFlag | cMass | cTypes | cSideFlags;
    }
    
    function rectHitMap(a1, a2, map, tsFlags, mapW, mapH, tw, th, aFlags) {
        var mapRegion = rectIntersection(
            [
                toInt((a2[0])/tw),
                toInt((a2[1])/th),
                toInt((a2[2]+tw-1)/tw),
                toInt((a2[3]+th-1)/th)
            ],
            [0, 0, mapW, mapH]);

        var interXY = 0, interX = 0, interY = 0, optXFlags = 0, optYFlags = 0, cFlags = 0;

        var aTypes = (aFlags>>4) & 0xFF;

        for (var ty = mapRegion[1]; ty < mapRegion[3]; ty++) {
            for (var tx = mapRegion[0]; tx < mapRegion[2]; tx++) {
                var tileIndex = map[tx+ty*mapW];
                if (tileIndex) {
                    var bFlags    = tsFlags[tileIndex-1];
                    var bType     = 1 << (bFlags>>4) >> 1; // convert type to flag
                    var cTypeFlag = (aTypes&bType) << 4;
                    if (!cTypeFlag) {
                        continue;
                    }
                    var bx1 = tx*tw, by1 = ty*th;
                    var b = [bx1, by1, bx1+tw, by1+th];
                    var inter = rectIntersection(a2, b);
                    if (rectEmpty(inter)) {
                        continue;
                    }
                    var bSideFlags = ((bFlags>>2)&3) | ((bFlags<<2)&12);
                    var cSideFlags = rectSeparationFlags(a1, b) & aFlags;
                    if (cSideFlags&bSideFlags) {
                        cSideFlags &= bSideFlags;
                        if (cSideFlags == (cSideFlags&0x5)) {
                            interX  = Math.max(interX,  inter[2]-inter[0]);
                        } else if (cSideFlags == (cSideFlags&0xA)) {
                            interY  = Math.max(interY,  inter[3]-inter[1]);
                        } else {
                            interXY = Math.max(interXY, inter[3]-inter[1]);
                        }
                        cFlags |= cSideFlags | cTypeFlag | 0xF00000;
                    } else {
                        if (cSideFlags == (cSideFlags&0x5)) {
                            optXFlags |= cTypeFlag;
                        } else if (cSideFlags == (cSideFlags&0xA)) {
                            optYFlags |= cTypeFlag;
                        }
                    }
                }
            }
        }

        // If any collision flags have been set,
        // we need to displace the original dx/dy by the intersection
        if (interX) {
            var dx = a2[0] < a1[0] ? -interX : interX;
            a2[0] -= dx;
            a2[2] -= dx;
        } else {
            if (!interY) {
                interY = interXY;
            }
            cFlags = (cFlags&~0xF) | (cFlags&0xA) | optXFlags;
        }

        if (interY) {
            var dy = a2[1] < a1[1] ? -interY : interY;
            a2[1] -= dy;
            a2[3] -= dy;                        
        } else {
            cFlags = (cFlags&~0xF) | (cFlags&0x5) | optYFlags;
        }
                    
        return cFlags;
    }
    
    function unescapeTextArray(text) {
        var un_text = "";
        for (var i = 0; i < text.length;) {
            if (text[i] != "\\") {
                un_text += text[i];
                i += 1;
            } else {
                i += 1;
                if (isDigit(text, i)) {
                    var digits = "";
                    for (; isDigit(text, i); i++) {
                        digits += text[i];
                    }
                    un_text += String.fromCharCode(parseInt(digits));
                } else {
                    c = text[i];
                    if (c == "n") {
                        un_text += "\n";
                    } else if (c == "t") {
                        un_text += "\t";
                    } else {
                        un_text += c;
                    }
                    i += 1;
                }
            }
        }
        return un_text;
    }
    
    /** 
     * 
     * Graphics / Images
     *
     */    
    
    function PinkyGraphics(el, vw, vh) {
        this.el = el;
        this.z  = 0;
        this.vw = vw;
        this.vh = vh;
        this.ctx = el.getContext(
            '2d',
            {
                alpha: false
            });            
        this.bufferCanvas = document.createElement("canvas");
        this.bufferCanvas.width  = vw;
        this.bufferCanvas.height = vh;
        this.bufferCtx = this.bufferCanvas.getContext(
            '2d',
            {
                antialias: false
            });
    }

    extendClass(BaseObject, PinkyGraphics, {
        pushViewport: function(x, y, z) {
            this.bufferCtx.save();
            this.bufferCtx.translate(-x, -y);
            this.z = z;            
        },
        dropViewport: function() {
            this.bufferCtx.restore();
        },
        flush: function() {
            this.ctx.save();
            this.ctx.scale(
                this.el.width /(this.z*this.vw),
                this.el.height/(this.z*this.vh)
            );
            this.ctx.drawImage(this.bufferCanvas, 0, 0);
            this.ctx.restore();
        },
        clear: function(rgb) {
            var alpha = (rgb>>24) & 0xFF;
            if (alpha == 0) {
                this.bufferCtx.clearRect(0, 0, this.vw, this.vh);
            } else {
                this.bufferCtx.beginPath();
                this.bufferCtx.rect(0, 0, this.vw, this.vh);
                this.bufferCtx.fillStyle = "#"+("000000000000000"+rgb.toString(16)).substr(-6)
                this.bufferCtx.fill();
            }
        }
    });

    function PinkyImage(el) {
        this.el = el;
        this.w  = el.width;
        this.h  = el.height;
    }

    extendClass(BaseObject, PinkyImage, {
        subImage: function(x, y, w, h, flags, color) {
            return new PinkySubImage(
                blendImage(this.el, flags>>4&3, color),
                x, y, w, h,
                flags,
                color);
        },
        draw: function(g, x, y, flags) {
            this.drawStretched(g, x, y, this.w, this.h, 0, 0, this.w, this.h, flags);
        },
        drawStretched: function(g, dx, dy, dw, dh, sx, sy, sw, sh, flags) {
            if (flags&0xC0) {
                return;
            }
            var ctx = g.bufferCtx;
            ctx.save();            
            if ((flags>>4&3) == 3) {
                ctx.globalAlpha = (this.color>>24&0xFF) / 255.0;
            }
            ctx.translate(dx+dw/2, dy+dh/2);
            ctx.rotate(((flags&0xC)>>2)*Math.PI/2);            
            ctx.scale((flags&1)?-1:1, (flags&2)?-1:1);
            ctx.translate(-dw/2, -dh/2);
            ctx.drawImage(this.el, sx, sy, sw, sh, 0, 0, dw, dh);
            ctx.restore();
        },
        getWidth: function() {
            return this.w || this.el.width;
        },
        getHeight: function() {
            return this.h || this.el.height;
        }
    });
    
    function PinkySubImage(el, x, y, w, h, flags, color) {
        this.el    = el;
        this.x     = x;
        this.y     = y;
        this.w     = w;
        this.h     = h;
        this.flags = flags;
        this.color = color;
    }

    extendClass(PinkyImage, PinkySubImage, {
        subImage: function(x, y, w, h, flags, color) {
            var ax2 = this.x + this.w;
            var ay2 = this.y + this.h;
            var bx1 = this.x + x;
            var by1 = this.y + y;
            var blendMode = flags>>4&3;
            return new PinkySubImage(
                blendImage(this.el,
                           blendMode,
                           blendMode==3 ? (color|0xFF000000) : color),
                bx1, by1,
                Math.min(bx1+w, ax2) - bx1,
                Math.min(by1+h, ay2) - by1,
                flags,
                blendMode==3 ? (color|0xFFFFFF) : color);
        },
        draw: function(g, x, y, flags) {
            this.drawStretched(g, x, y, this.w, this.h, 0, 0, this.w, this.h, flags);
        },
        drawStretched: function(g, dx, dy, dw, dh, sx, sy, sw, sh, flags) {
            if ((flags&0xC0) == 0xC0) {
                return;
            }
            var ctx = g.bufferCtx;
            flags = flags ^ this.flags;
            ctx.save();            
            if ((flags>>4&3) == 3) {
                ctx.globalAlpha = (this.color>>24&0xFF) / 255.0;
            }
            ctx.translate(dx+dw/2, dy+dh/2);
            ctx.rotate(((flags&0xC)>>2)*Math.PI/2);
            ctx.scale((flags&1)?-1:1, (flags&2)?-1:1);
            ctx.translate(-dw/2, -dh/2);
            ctx.drawImage(this.el, this.x+sx, this.y+sy, sw, sh, 0, 0, dw, dh);
            ctx.restore();
        },
        getWidth: function() {
            return this.w;
        },
        getHeight: function() {
            return this.h;
        }
    });
    
    function rgbaAdd(a, b) {
        return (Math.min((a>>24&0xFF)+(b>>24&0xFF), 0xFF)<<24 |
                Math.min((a>>16&0xFF)+(b>>16&0xFF), 0xFF)<<16 |
                Math.min((a>> 8&0xFF)+(b>> 8&0xFF), 0xFF)<< 8 |
                Math.min((a    &0xFF)+(b    &0xFF), 0xFF));
    }
    
    function rgbaSubtract(a, b) {
        return (Math.max((a>>24&0xFF)-(b>>24&0xFF), 0xFF)<<24 |
                Math.max((a>>16&0xFF)-(b>>16&0xFF), 0xFF)<<16 |
                Math.max((a>> 8&0xFF)-(b>> 8&0xFF), 0xFF)<< 8 |
                Math.max((a    &0xFF)-(b    &0xFF), 0xFF));
    }
    
    function rgbaMultiply(a, b) {
        return (Math.min(((a>>24&0xFF)*(b>>24&0xFF)/0xFF)&0xFF, 0xFF)<<24 |
                Math.min(((a>>16&0xFF)*(b>>16&0xFF)/0xFF)&0xFF, 0xFF)<<16 |
                Math.min(((a>> 8&0xFF)*(b>> 8&0xFF)/0xFF)&0xFF, 0xFF)<< 8 |
                Math.min(((a    &0xFF)*(b    &0xFF)/0xFF)&0xFF, 0xFF));
    }

    function getRgbaColor(data, i) {
        return ((data[i  ]<<16) |
                (data[i+1]<< 8) |
                (data[i+2])     |
                (data[i+3]<<24));
    }

    function putRgbaColor(data, i, c) {
        data[i  ] = c>>16&0xFF;
        data[i+1] = c>> 8&0xFF;
        data[i+2] = c    &0xFF;
        data[i+3] = c>>24&0xFF;
    }
    
    function blendImage(el, blendMode, color) {
        if (blendMode == 0) {
            return el;
        }

        if (blendMode == 3 && (color&0xFFFFFF) == 0xFFFFFF) {
            return el; // opacity changes can be handled realtime
        }        

        // NOTE: ctx.globalCompositeOperation
        // might be faster for the operations below?
        
        var canvas = document.createElement('canvas');
        canvas.width = el.width;
        canvas.height = el.height;
        var ctx = canvas.getContext('2d');
        
        ctx.drawImage(el, 0, 0);

        var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        var data = imgData.data;
        var blendFunc = [null, rgbaAdd, rgbaSubtract, rgbaMultiply][blendMode]
        
        for (var i = 0; i < data.length; i += 4) {
            putRgbaColor(data, i, blendFunc(getRgbaColor(data, i), color));
        }
                
        ctx.putImageData(imgData, 0, 0);
        
        return canvas;
    }

    function getImageData(el, x, y, w, h) {
        var canvas = document.createElement('canvas');
        canvas.width  = w;
        canvas.height = h;
        
        var ctx = canvas.getContext('2d');
        
        ctx.drawImage(el, x, y, w, h, 0, 0, w, h);
        
        return ctx.getImageData(0, 0, canvas.width, canvas.height).data;
    }

    PinkyImage.loadPng = function(data, onLoad) {
        var htmlImage = new Image();
        htmlImage.onload = function() {
            onLoad(new PinkyImage(this));
        };
        htmlImage.onerror = function(error) {
            console.warn(error);
            onLoad(null);
        };
        htmlImage.src = "data:image/png;base64," + data;
    };

    // PinkyImage.drawBufferedMap = function(g, images, map, mapW, mapH,
    //                                       offX, offY, w, h, tw, th, x, y) {
    //     if (!map.bufferImage) {
    //         var buffer    = document.createElement('canvas');
    //         buffer.width  = mapW * tw;
    //         buffer.height = mapH * th;

    //         PinkyImage.drawMap(
    //             new PinkyGraphics(buffer, buffer.width, buffer.height),
    //             images,
    //             map, mapW, mapH, 0, 0, mapW, mapH, tw, th, 0, 0);
            
    //         map.bufferImage = buffer;
    //     }

    //     var x1  = (offX)   * tw;
    //     var y1  = (offY)   * th;
    //     var x2  = (offX+w) * tw;
    //     var y2  = (offY+h) * th;
    //     var ctx = g.bufferCtx;

    //     ctx.save();
    //     ctx.translate(x, y);
    //     ctx.fillStyle = ctx.createPattern(map.bufferImage, 'repeat');
    //     ctx.fillRect(x1, y1, x2-x1, y2-y1);
    //     ctx.restore();
    // };

    PinkyImage.drawMap = function(g, images, map, mapW, mapH,
                                  offX, offY, w, h, tw, th, x, y) {
        for (var y1 = offY; y1 < offY+h; y1++) {
            for (var x1 = offX; x1 < offX+w; x1++) {
                var i = map[(x1%mapW)+(y1%mapH)*mapW];
                if (i != 0) {
                    images[i-1].draw(g, x+x1*tw, y+y1*th, 0x00);
                }
            }
        }        
    };
    
    PinkyImage.drawText = function(g, images, strides, text, xi, y, off, lh, subChars) {
        var i = 0;
        var j = 0;
        var n = text.length;
        var x = xi;
        while (i < n) {
            var c = text.charCodeAt(i++);
            if (c == 0xA) {
                x = xi;
                y = y + lh;
                continue;
            } else if (c == 0x9) {
                ;
            } else if (c == 0x24) {
                c = subChars[j];
                images[c].draw(g, x, y, 0x00);
                j++;
            } else if (c != 0x20) {
                images[c].draw(g, x, y, 0x00);
            }
            x += off + (strides[c]>>16);
        }
    };

    /**
     *
     * Audio
     *
     */
    
    var audioContext = new (window.AudioContext || webkitAudioContext || mozAudioContext)();
    
    function PinkySound(buffer) {
        this.buffer = buffer;
    }

    extendClass(BaseObject, PinkySound, {
        play: function(gain, loop) {
            if (this.source) {
                return;
            }
            
            this.source = audioContext.createBufferSource();
            this.source.buffer = this.buffer;
            this.source.loop = loop;            
            this.source.onended = function(event) {
                this.source = null;
            }.bind(this);
            
            if (gain == 1.0) {
                this.source.connect(audioContext.destination);
            } else {
                var gainNode = audioContext.createGain();
                gainNode.gain.value = gain;
                gainNode.connect(audioContext.destination);
                this.source.connect(gainNode);
            }

            this.source.start(0);
        },
        stop: function() {
            if (this.source) {
                this.source.stop(0);
                this.source = null;
            }
        }
    });

    PinkySound.loadWav = function(data, onLoad) {
        audioContext.decodeAudioData(
            strToByteArray(data),
            function(buffer) {
                onLoad(new PinkySound(buffer));
            },
            function(error) {
                console.warn(error);
                onLoad(null);
            }
        );
    };

    /** 
     * 
     * Application
     *
     */

    function PinkyApplication(el) {
        this.el        = el;
        this.g         = undefined;        
        this.keyStates = new Array(256);
        this.mouseX    = 0;
        this.mouseY    = 0;
        this.active    = true;
        this.done      = false;
        
        this.eventHandlers = {
            keydown: function(e) {
                if (!e.repeat && e.keyCode <= 0xFF) {
                    this.keyStates[e.keyCode] |= 1;
                }
                e.preventDefault();
            },
            keyup: function(e) {
                if (!e.repeat && e.keyCode <= 0xFF) {
                    this.keyStates[e.keyCode] |= 2;
                }
            },
            mousemove: function(e) {
                if (!this.g) {
                    return;
                }
                this.mouseX = (e.pageX-this.el.offsetLeft) *
                    this.g.vw / this.el.offsetWidth;
                this.mouseY = (e.pageY-this.el.offsetTop)  *
                    this.g.vh / this.el.offsetHeight;
            },
            mousedown: function(e) {
                if (e.button == 0) {
                    this.keyStates[0x01] |= 1;
                } else if (e.button == 2) {
                    this.keyStates[0x02] |= 1;
                }
            },
            mouseup: function(e) {
                if (e.button == 0) {
                    this.keyStates[0x01] |= 2;
                } else if (e.button == 2) {
                    this.keyStates[0x02] |= 2;
                }
            },
            focus: function() {
                this.active = true;
            },
            blur: function() {
                this.active = false;
            }
        };

        for (var k in this.eventHandlers) {
            this.eventHandlers[k] = this.eventHandlers[k].bind(this);
        }

        this.addEventListeners();
    }

    PinkyApplication.prototype.addEventListeners = function() {
        for (var k in this.eventHandlers) {
            window.addEventListener(k, this.eventHandlers[k]);
        }
    };

    PinkyApplication.prototype.removeEventListeners = function() {
        for (var k in this.eventHandlers) {
            window.removeEventListener(k, this.eventHandlers[k]);
        }                    
    };

    PinkyApplication.prototype.snapshotKeyStates = function() {
        var newKeyStates = createBlob(128);
        
        for (var i = 1; i < this.keyStates.length; i++) {
            var keyState = this.keyStates[i];
            keyState |= ((keyState<<2)&4);
            keyState &= ((keyState>>1)&2) | ~2;
            blobSetByte(newKeyStates, i, keyState);
            blobSetByte(newKeyStates, 0, newKeyStates[0]|keyState);
            this.keyStates[i] = (keyState & ~(keyState<<1)) & 4;
        }
        
        blobSetByte(newKeyStates, 0x3A, this.mouseX>>8&0xFF);
        blobSetByte(newKeyStates, 0x3B, this.mouseX   &0xFF);
        blobSetByte(newKeyStates, 0x3C, this.mouseY>>8&0xFF);
        blobSetByte(newKeyStates, 0x3D, this.mouseY   &0xFF);
        
        return newKeyStates;
    };

    PinkyApplication.prototype.initializeGraphics = function(vw, vh) {
        return this.g = new PinkyGraphics(this.el, vw, vh);
    };

    /** 
     * 
     * Game
     *
     */

    function PinkyStruct(fields, values) {
        for (var i in fields) {
            this[fields[i]] = values[i];
        }
    }

    function Sprite(args) {
        PinkyStruct.bind(this)(
            [
                'src',
                'bnds',
                'old_bnds',
                'inset',
                'rotation',
                'flip_x',
                'flip_y',
                'hide',
                'vx',
                'vy',
                'ax',
                'ay',
                'vxt',
                'vyt',
                'fx',
                'fy',
                'hid',
                'hids_on',
                'hids_off',
                'sides',
                'weight',
                'hits',
            ], args);
    }
    
    extendClass(PinkyStruct, Sprite, {
        draw: function(g, vx, vy) {
            if (this.hide) {
                return;
            }
           
            var w = this.src.getWidth () << 16;
            var h = this.src.getHeight() << 16;
            var ox, oy;
            
            if (this.rotation == 1) {
                ox = h - this.inset[3];
                oy = this.inset[0];
            } else if (this.rotation == 2) {
                ox = w - this.inset[2];
                oy = h - this.inset[3];
            } else if (this.rotation == 3) {
                ox = this.inset[1];
                oy = w - this.inset[2];
            } else {
                ox = this.flip_x ? (w-this.inset[2]) : this.inset[0];
                oy = this.flip_y ? (h-this.inset[3]) : this.inset[1];
            }

            var gflags = this.rotation << 2
                | (this.flip_x ? 1 : 0)
                | (this.flip_y ? 2 : 0);

            this.src.draw(g,
                          this.bnds[0]-ox>>16,
                          this.bnds[1]-oy>>16,
                          gflags);
        }
    });

    function Tileset(args) {
        PinkyStruct.bind(this)(
            [
                'src',
                'tiles',
                'anims',
                'flags',
                'tw',
                'th',
                'spacing',
                'margin',
            ], args);
    }
    
    extendClass(PinkyStruct, Tileset, {
        setFrame: function(t) {
            for (var i in this.anims) {
                var anim = this.anims[i];
                this.tiles[anim[0]] = this.tiles[anim[1][t%anim[1].length]];
            }
        }
    });

    function Tilemap(args) {
        PinkyStruct.bind(this)(
            [
                'tileset',
                'data',
                'w',
                'h',
                'tw',
                'th',
                'x',
                'y',
                'pw',
                'ph',                
            ], args);
        
        this.tileset = new Tileset(this.tileset);
    }
    
    extendClass(PinkyStruct, Tilemap, {
        draw: function(g, vx, vy, world_w, world_h, screen_w, screen_h) {
            var scroll_x, scroll_y;
            
            if (world_w <= screen_w) {
                scroll_x = 0;
            } else {
                scroll_x = toInt(vx*(this.pw-screen_w)/(world_w-screen_w));
            }

            if (world_h <= screen_h) {
                scroll_y = 0;
            } else {
                scroll_y = toInt(vy*(this.ph-screen_h)/(world_h-screen_h));
            }

            scroll_x = clamp(scroll_x, 0, this.pw-screen_w);
            scroll_y = clamp(scroll_y, 0, this.ph-screen_h);

            PinkyImage.drawMap(
                g,                
                this.tileset.tiles,
                this.data,
                this.w,
                this.h,
                toInt(scroll_x/this.tw),
                toInt(scroll_y/this.th),
                toInt(screen_w/this.tw)+1,
                toInt(screen_h/this.th)+1,
                this.tw>>16,
                this.th>>16,
                this.x+vx-scroll_x>>16,
                this.y+vy-scroll_y>>16);
        },
    });

    function Cam(args) {
        PinkyStruct.bind(this)(Cam._fields, args);
    }
    
    extendClass(PinkyStruct, Cam, {        
        setBounds: function(x_min, y_min, x_max, y_max) {
            this.x_min = x_min;
            this.y_min = y_min;
            this.x_max = x_max;
            this.y_max = y_max;
        },
        
        nextPath: function() {
            var dest    = this.path[this.i];
            var dx      = dest[0] - this.x;
            var dy      = dest[1] - this.y;
            var d_max   = Math.max(Math.abs(dx), Math.abs(dy));

            if (d_max) {
                this.nx = fpDiv(dx, d_max);
                this.ny = fpDiv(dy, d_max);
                this.d  = fpHypot(dx, dy);
            } else {
                this.nx = 0;
                this.ny = 0;
                this.d  = 0;
            }

            this.i += 1;
        },
        
        advance: function () {
            if (this.d_acc == this.d_max) {
                return;
            }

            function easeout(a, b, t) {
                return a + toInt((b-a)*(1.0-((1.0-t)*(1.0-t))));
            }

            // smooth out speed over entire path
            var d = easeout(
                this.path[this.i-1][2],
                1<<16,
                this.d_acc/this.d_max);

            if (d <= this.d) {
                this.x = clamp(
                    this.x+fpMul(this.nx, d), this.x_min, this.x_max);
                this.y = clamp(
                    this.y+fpMul(this.ny, d), this.y_min, this.y_max);
                this.d_acc += d;
                this.d -= d;
            } else {
                this.x = clamp(
                    this.x+fpMul(this.nx, this.d), this.x_min, this.x_max);
                this.y = clamp(
                    this.y+fpMul(this.ny, this.d), this.y_min, this.y_max);

                this.d_acc += this.d;

                if (this.i == this.path.length) {
                    this.d = 0;
                } else {
                    this.d = d - this.d;
                    this.nextPath();
                    this.advance();
                }
            }
        },
        
        setPath: function(path) {
            this.path  = path;
            this.x     = clamp(path[0][0], this.x_min, this.x_max);
            this.y     = clamp(path[0][1], this.y_min, this.y_max);
            this.i     = 1;
            this.d_acc = 0;
            this.d_max = Cam.calcPathDist(path);
            this.nextPath();
        },

        toArray: function() {
            var out = [];
            for (var i in Cam._fields) {
                out[i] = this[Cam._fields[i]];
            }
            return out;
        }
    });

    Cam._fields = [
        'x',
        'y',
        'nx',
        'ny',
        'd',
        'd_acc',
        'd_max',
        'i',
        'path',
        'x_min',
        'y_min',
        'x_max',
        'y_max',
    ];

    Cam.calcPathDist = function(path) {
        if (!path.length) {
            return 0;
        }
        d = 0;
        n = path.length - 1;
        b = path[n];
        while (n > 0) {
            n = n - 1;
            a = path[n];
            d = d + fpHypot(b[0]-a[0], b[1]-a[1]);
            b = a;
        }
        return d;
    }
    

    /** 
     * 
     * Core Virtual Machine
     *
     */    

    var NOP  = 0x00, NULL = 0x01, ZERO = 0x02, ONE  = 0x03, LODI = 0x04,
        LODS = 0x05, LODF = 0x06, ADD  = 0x07, SUB  = 0x08, MUL  = 0x09,
        DIV  = 0x0A, MULF = 0x0B, DIVF = 0x0C, NOT  = 0x0D, NEG  = 0x0E,
        LT   = 0x0F, GT   = 0x10, LTE  = 0x11, GTE  = 0x12, EQ   = 0x13,
        NE   = 0x14, JMP  = 0x15, JIF  = 0x16, JZ   = 0x17, JSR  = 0x18,
        RET  = 0x19, DUP  = 0x1A, DROP = 0x1B, SETA = 0x1C, SETI = 0x1D,
        SETL = 0x1E, SETG = 0x1F, GETA = 0x20, GETI = 0x21, GETL = 0x22,
        GETG = 0x23, NEW  = 0x24, NEWV = 0x25, REM  = 0x26, EVAL = 0x27,
        MARK = 0x28, STEP = 0x29, CLR  = 0x2A, ARGS = 0x2B, NUM_OPS = 0x2C;
    
    var KEYWORDS = [
        'nop ', 'null', 'zero', 'one ', 'lodi',
        'lods', 'lodf', '+   ', '-   ', '*   ',
        '/   ', '*f  ', '/f  ', 'not ', 'neg ',
        '<   ', '>   ', '<=  ', '>=  ', '==  ',
        '!=  ', 'jmp ', 'jif ', 'jz  ', 'jsr ',
        'ret ', 'dup ', 'drop', 'seta', 'seti',
        'setl', 'setg', 'geta', 'geti', 'getl',
        'getg', 'new ', 'newv', '#   ', 'eval',
        'mark', 'step', 'clr ', 'args'];

    function PinkyError(message) { this.message = message; }
    function PinkyRuntimeError(message) { PinkyError.call(this, message); }
    function PinkyAssertionError() {}
    function PinkyInterruptException(handler) {
        this.handler = handler;
    }

    function PinkyProc(ip, numVars) {
        this.ip = ip;        
        this.defaults = createNullArray(numVars);
        this.p = 0;
    }    

    function PinkyVM(screenEl, code, fileOffs, literals, numGlobals,
                     onError, onResize, onDestroy) {
        this.code       = code;
        this.fileOffs   = [[0, ""]].concat(fileOffs).concat([[code.length, ""]]);
        this.literals   = literals;
        this.numGlobals = numGlobals;
        this.os         = [];
        this.fs         = [];
        this.vs         = [];
        this.cs         = [];
        this.ls         = {};
        this.seed       = 0;
        this.startTime  = new Date().getTime();
        this.app        = new PinkyApplication(screenEl);
        this.g          = undefined;
        this.cam        = new Cam([0, 0, 0, 0, 0, 0, 0, 0, [], 0, 0, 0, 0]);
        this.gDisabled  = false;
        this.onError    = onError   || function(vm, error) {};
        this.onResize   = onResize  || function(w, h) {};
        this.onDestroy  = onDestroy || function() {};

        // Below are PinkyVM methods that need to run fast (access vars via closure)
        
        var os = this.os;
        var fs = this.fs;
        var ls = this.ls;
        var vs = this.vs;
        var cs = this.cs;
        var code = this.code;
        var literals = this.literals;
        var nativeLookup = this.makeNativeLookup(os, vs).map(
            function(f) {
                return f.bind(this);
            }.bind(this)
        );
        var opLookup = this.makeOpLookup(os, fs, ls, vs, cs, code, literals, nativeLookup);
        var evalUntilInterrupt = function() {
            var fi = cs[cs.length-1];
            try {
                while (fi <= fs.length) {
                    var ip = fs[fs.length-1];
                    var ci = ip << 1;
                    opLookup[code[ci++]](ci);
                    fs[fs.length-1]++;
                }                
            } catch(e) {
                if (e instanceof PinkyInterruptException) {
                    e.handler();
                    return;
                } else {
                    this.onError(this, e);
                    throw e;
                }
            }

            cs.pop();
            
            if (cs.length === 0) {
                this.destroy();
            }
            
        }.bind(this);
        
        this.call = function(proc, args) {
            fs.push(proc.ip);
            cs.push(fs.length);
            vs.push(os.length);
            os.push(proc)
            arrayExtend(os, args);
            arrayExtend(os, proc.defaults.slice(1+args.length));
            evalUntilInterrupt();
        }.bind(this);
        
        this.returnFromBuiltinInterrupt = function() {
            fs[fs.length-1]++;
            evalUntilInterrupt();
        }.bind(this);

        this.returnFromInterrupt = function() {
            var vi = vs[vs.length-1];
            os.splice(vi, os[vi].defaults.length);
            fs.pop();
            vs.pop();
            fs[fs.length-1]++;
            evalUntilInterrupt();
        }.bind(this);

        // The AudioContext was not allowed to start.
        // It must be resumed (or created) after a user gesture on the page.
        // https://goo.gl/7K7WLu
        audioContext.resume();
    }

    extendClass(BaseObject, PinkyVM, {
        _getFilenameForIp: function(ip) {
            var name = null;
            for (var i = 0; i < this.fileOffs.length; i++) {
                var fileOff = this.fileOffs[i];
                if ((ip+ip) <= fileOff[0]) {
                    break;
                }
                name = fileOff[1];
            }
            return name;
        },
        
        stackTrace: function() {
            var tb = [];
            for (var i = 0; i < this.fs.length; i++) {
                tb.push(""
                        + (this._getFilenameForIp(this.fs[i]) || '?')
                        + ":"
                        + (this.ls[i] || '?')
                        + ":"
                        + (this.fs[i]+1)
                );
            }
            return tb;
        },
        
        run: function() {
            this.call(new PinkyProc(0, this.numGlobals), []);
        },

        makeOpLookup: function(os, fs, ls, vs, cs, cv, gv, nativeLookup) {
            return [
                function(ci) { // nop
                },
                function(ci) { // null
                    os.push(null);
                },
                function(ci) { // zero
                    os.push(0);
                },
                function(ci) { // one
                    os.push(1);
                },
                function(ci) { // lodi
                    os.push(gv[cv[ci]]);
                },
                function(ci) { // lods
                    os.push(gv[cv[ci]]);
                },
                function(ci) { // lodf
                    os.push(gv[cv[ci]]);
                },
                function(ci) { // add
                    os.push(os.pop()+os.pop());
                },
                function(ci) { // sub
                    var b = os.pop(), a = os.pop();
                    os.push(a-b);
                },
                function(ci) { // mul
                    os.push(os.pop()*os.pop());                
                },
                function(ci) { // div
                    var b = os.pop(), a = os.pop();
                    os.push((a/b)&0xFFFFFFFF);
                },
                function(ci) { // mulf
                    os.push(((os.pop()*os.pop())/65536)&0xFFFFFFFF);
                },
                function(ci) { // divf
                    var b = os.pop(), a = os.pop();
                    os.push(((a*65536)/b)&0xFFFFFFFF);
                },
                function(ci) { // not
                    os.push(os.pop()==0);
                },
                function(ci) { // neg
                    os.push(-os.pop());
                },
                function(ci) { // lt
                    var b = os.pop(), a = os.pop();
                    os.push(a <b);
                },
                function(ci) { // gt
                    var b = os.pop(), a = os.pop();
                    os.push(a >b);
                },
                function(ci) { // lte
                    var b = os.pop(), a = os.pop();
                    os.push(a<=b);
                },
                function(ci) { // gte
                    var b = os.pop(), a = os.pop();
                    os.push(a>=b);
                },
                function(ci) { // eq
                    var b = os.pop(), a = os.pop();
                    os.push(a==b);
                },
                function(ci) { // ne
                    var b = os.pop(), a = os.pop();
                    os.push(a!=b);
                },
                function(ci) { // jmp
                    fs[fs.length-1] = cv[ci] - 1;      // -1 to account for post-eval inc
                },
                function(ci) { // jif
                    if (os.pop()!=0) {
                        fs[fs.length-1] = cv[ci] - 1;  // -1 to account for post-eval inc
                    }
                },
                function(ci) { // jz
                    if (os.pop()==0) {
                        fs[fs.length-1] = cv[ci] - 1;  // -1 to account for post-eval inc
                    }
                },
                function(ci) { // jsr
                    var vn = cv[ci] + 1;
                    var vi = os.length - vn;
                    var proc = os[vi];
                    proc.p++;
                    fs.push(proc.ip-1);  // -1 to account for post-eval inc
                    vs.push(vi);
                    arrayExtend(os, proc.defaults.slice(vn));
                },
                function(ci) { // ret
                    var vi = vs[vs.length-1];
                    os.splice(vi, os[vi].defaults.length);
                    fs.pop();
                    vs.pop();
                },
                function(ci) { // dup
                    os.push(os[os.length-1]);
                },
                function(ci) { // drop
                    os.pop();
                },
                function(ci) { // seta
                    var b = os.pop(), a = os.pop();
                    a[cv[ci]] = b;
                },
                function(ci) { // seti
                    var b = os.pop(), i = os.pop(), a = os.pop();
                    a[i] = b;
                },
                function(ci) { // setl
                    os[vs[vs.length-1]+cv[ci]] = os.pop();
                },
                function(ci) { // setg
                    os[cv[ci]] = os.pop();
                },                
                function(ci) { // geta
                    os.push(os.pop()[cv[ci]]);
                },
                function(ci) { // geti
                    var i = os.pop(), a = os.pop();
                    os.push(a[i]);
                },
                function(ci) { // getl
                    os.push(os[vs[vs.length-1]+cv[ci]]);
                },
                function(ci) { // getg
                    os.push(os[cv[ci]]);
                },
                function(ci) { // new
                    os.push(createNullArray(cv[ci]));
                },
                function(ci) { // newv
                    var data = new Array(cv[ci]);
                    for (var i = data.length-1; i >= 0; i--) {
                        data[i] = os.pop();
                    }
                    os.push(data);
                },
                function(ci) { // rem
                    ls[fs.length-1] = cv[ci];
                },
                function(ci) { // eval
                    var vi = vs[vs.length-1];
                    nativeLookup[cv[ci]](os.slice(vi+1));
                    os.splice(vi, os[vi].defaults.length);
                    fs.pop();
                    vs.pop();
                }.bind(this),
                function(ci) { // mark
                    os[vs[vs.length-1]].ip = fs[fs.length-1] + cv[ci] + 1;
                },
                function(ci) { // step
                    os.push(os[vs[vs.length-1]].p);
                },
                function(ci) { // clr
                    var vi = vs[vs.length-1];
                    os[vi].ip = fs[fs.length-1] + 1;
                    os[vi].p = 1;
                },
                function(ci) { // args
                    var args = os.pop();
                    for (var i = 0; i < args.length; i++) {
                        os.push(args[i]);
                    }
                },
                
                //
                // Core
                //                
                function() { // assert
                    if (os.pop() == 0) {
                        throw new PinkyAssertionError();
                    }
                },
                function() { // log
                    console.log(os.pop());
                },
                function() { // time
                    os.push(new Date().getTime()-this.startTime);
                }.bind(this),
                function() { // rand
                    var end = os.pop(), start = os.pop();
                    this.seed = (this.seed*9301+49297) % 233280;
                    os.push(toInt(start+this.seed*(end-start)/233280));
                }.bind(this),
                function() { // seed
                    this.seed = os.pop();
                }.bind(this),
                function() { // weak
                    ;
                },
                function() { // is
                    os.push(os.pop()===os.pop());
                },
                function() { // default
                    var b = os.pop(), a = os.pop();
                    os.push(a !== null ? a : b);
                },
                function() { // select
                    var b = os.pop(), a = os.pop(), x = os.pop();
                    os.push(x ? a : b);
                },
                function() { // sleep
                    var t = os.pop();
                    throw new PinkyInterruptException(
                        function() {
                            setTimeout(this.returnFromBuiltinInterrupt.bind(this), t);
                        }.bind(this)
                    );
                }.bind(this),
                function() { // macro
                    var proc = os.pop();
                    os.push(new PinkyProc(proc.ip, proc.numVars));
                },
                function() { // macrostep
                    os.push(os.pop().p);
                },
                function() { // gettype
                    var obj = os.pop();
                    if (obj === null) {
                        os.push(0);
                    } else if (typeof obj === "number") {
                        os.push(1);
                    } else if (typeof obj === "string") {
                        os.push(2);
                    } else if (obj instanceof Array) {
                        os.push(3);
                    } else if (obj instanceof Uint8Array) {
                        os.push(4);
                    } else if (obj.constructor === PinkyProc) {
                        os.push(5);
                    } else {
                        os.push(0);
                    }
                },
                function() { // clone
                    os.push(os.pop().slice());
                },
                function() { // attrs
                    ;
                },

                //
                // Integer
                //
                function() { // and
                    os.push(os.pop() &os.pop());
                },
                function() { // or
                    os.push(os.pop() |os.pop());
                },
                function() { // xor
                    os.push(os.pop() ^os.pop());
                },
                function() { // shl
                    var n = os.pop(), x = os.pop();
                    os.push(x<<n);
                },
                function() { // shr
                    var n = os.pop(), x = os.pop();                    
                    os.push(x>>n);
                },
                function() { // rot
                    var n = os.pop(), i = os.pop(), x = os.pop();
                    os.push(((x&((1<<n)-1))>>i)|(x<<(n-i)&((1<<n)-1)));
                },
                function() { // bits
                    var mask = os.pop(), x = os.pop();                 
                    os.push((x& mask)>>ffb(mask));
                },
                function() { // setbits
                    var y = os.pop(), mask = os.pop(), x = os.pop();
                    os.push((x&~mask)|(y<<ffb(mask)));
                },
                function() { // signed
                    os[os.length-1] = toInt(os[os.length-1]);
                },
                function() { // mod
                    var b = os.pop(), a = os.pop();
                    os.push(((a%b)+b)%b);
                },
                function() { // min 
                    os.push(Math.min(os.pop(), os.pop()));
                },
                function() { // max
                    os.push(Math.max(os.pop(), os.pop()));
                },
                function() { // abs
                    os.push(Math.abs(os.pop()));
                },
                function() { // icmp
                    var b = os.pop(), a = os.pop();                    
                    os.push(a < b ? -1 : (a > b ? 1 : 0));
                },
                function() { // itos
                    os.push(os.pop().toString());
                },

                //
                // Fixed-point
                //
                function() { // fmod
                    var b = os.pop(), a = os.pop();
                    os.push(((a%b)+b)%b);
                },
                function() { // fmin 
                    os.push(Math.min(os.pop(), os.pop()));
                },
                function() { // fmax
                    os.push(Math.max(os.pop(), os.pop()));
                },
                function() { // fabs
                    os.push(Math.abs(os.pop()));
                },
                function() { // fcmp
                    var b = os.pop(), a = os.pop();                    
                    os.push(a < b ? -1 : (a > b ? 1 : 0));
                },
                function() { // ftos
                    var x = os.pop();
                    os.push(
                        (x&0xFFFF) ?
                            (x/0x10000).toString() :
                            (x/0x10000).toString()+".0"
                    );
                },
                function() { // ftoi
                    os.push((os.pop()>>16));
                },
                function() { // fp
                    var frac = os.pop(), int_part = os.pop();                    
                    os.push((int_part<<16)|(frac&0xFFFF));
                },
                function() { // floor
                    os.push((os.pop())&~0xFFFF);
                },
                function() { // ceil
                    os.push((os.pop()+0xFFFF)&~0xFFFF);
                },
                function() { // frac
                    os.push((os.pop())& 0xFFFF);
                },
                function() { // pow
                    var y = os.pop(), x = os.pop();                    
                    os.push(toInt(Math.pow(x/0x10000, y/0x10000)*0x10000));
                },
                function() { // sqrt
                    os.push(toInt(Math.sqrt(os.pop()/0x10000)*0x10000));
                },
                function() { // hypot
                    var b = os.pop(), a = os.pop();                    
                    os.push(toInt(Math.hypot(a/0x10000, b/0x10000)*0x10000));
                },
                function() { // sin
                    os.push(toInt(Math.sin (os.pop()/0x10000)*0x10000));
                },
                function() { // cos
                    os.push(toInt(Math.cos (os.pop()/0x10000)*0x10000));
                },
                function() { // tan
                    os.push(toInt(Math.tan (os.pop()/0x10000)*0x10000));
                },
                function() { // asin
                    os.push(toInt(Math.asin(os.pop()/0x10000)*0x10000));
                },
                function() { // acos
                    os.push(toInt(Math.acos(os.pop()/0x10000)*0x10000));
                },
                function() { // atan
                    os.push(toInt(Math.atan(os.pop()/0x10000)*0x10000));
                },

                //
                // String
                //
                function() { // concat
                    var b = os.pop(), a = os.pop();                    
                    os.push(a+b);
                },
                function() { // chr
                    os.push(String.fromCharCode(os.pop()));
                },
                function() { // ord
                    var i = os.pop(), text = os.pop();                                        
                    os.push(text.charCodeAt(i));
                },
                function() { // slen
                    os.push(os.pop().length);
                },
                function() { // scmp
                    var b = os.pop(), a = os.pop();
                    os.push(a < b ? -1 : (a > b ? 1 : 0));
                },
                function() { // stoi
                    os.push(parseInt(os.pop()));
                },
                function() { // decodeb64
                    os.push(createBlobFromB64Data(os.pop()));
                },
                function() { // b64
                    os.push(window.btoa(arrayToStr(os.pop())));
                },
                function() { // substr
                    var n = os.pop(), i = os.pop(), text = os.pop();
                    os.push(text.substring(i, i+n));
                },

                //
                // Array
                //
                function() { // slice
                    var n = os.pop(), i = os.pop(), array = os.pop();                    
                    os.push(array.slice(i, i+n));
                },
                function() { // reverse
                    os.push(os.pop().slice().reverse());
                },
                function() { // fill
                    var n = os.pop(), i = os.pop(), value = os.pop(), array = os.pop();
                    os.push(arrayFill(array, value, i, n));
                },
                function() { // copy
                    var n = os.pop(), i = os.pop(), srcArray = os.pop(), array = os.pop();
                    os.push(arrayCopy(array, srcArray, i, n));
                },
                function() { // sort
                    var cmp = os.pop(), array = os.pop();
                    var ip = fs[fs.length-1];
                    array.sort(function(a, b) {
                        this.call(cmp, [a, b]);
                        return os.pop();
                    }.bind(this));
                    fs[fs.length-1] = ip;
                }.bind(this),
                function() { // apply
                    var proc = os.pop(), objs = os.pop();
                    var ip = fs[fs.length-1];                    
                    for (var i = 0; i < objs.length; i++) {
                        this.call(proc, [objs[i]]);
                    }
                    fs[fs.length-1] = ip;                    
                }.bind(this),
                function() { // alen
                    os.push(os.pop().length);
                },
                function() { // array
                    os.push(createNullArray(os.pop()));
                },
                    
                //
                // Blob
                //
                function() { // blob
                    os.push(createBlob(os.pop()));
                },
                function() { // bload
                    os.push(createBlobFromCookie(os.pop()));
                },
                function() { // bsave
                    var key = os.pop(), blob = os.pop();
                    createCookie(key, window.btoa(arrayToStr(blob)), 90);
                },
                function() { // blen
                    os.push(os.pop().length);
                },
                function() { // get
                    var i = os.pop(), blob = os.pop();
                    os.push(blob[i]);
                },
                function() { // set
                    var value = os.pop(), i = os.pop(), blob = os.pop();                    
                    blobSetByte(blob, i, value);
                },
                function() { // gets
                    var n = os.pop(), i = os.pop(), blob = os.pop();
                    os.push(blobGetStr(blob, i, n));
                },
                function() { // sets
                    var n = os.pop(), text = os.pop(), i = os.pop(), blob = os.pop();
                    blobSetStr(blob, i, text, n);
                }
            ]
        },

        makeNativeLookup: function(os, vs) {
            return [
                //
                // Graphics
                //
                function(args) { // ginit
                    document.title = "Pinky | " + args[0];
                    this.g = this.app.initializeGraphics(args[1]>>16, args[2]>>16);
                    this.onResize(this.g.vw, this.g.vh);
                },
                function(args) { // gbegin
                    if (this.gDisabled) { return; }
                    this.g.pushViewport(0, 0, 1.0);
                },
                function(args) { // gend
                    if (this.gDisabled) { return; }                
                    this.g.dropViewport();
                    throw new PinkyInterruptException(function() {
                        window.requestAnimationFrame(function(t) {
                            this.g.flush();
                            this.returnFromInterrupt();
                        }.bind(this));
                    }.bind(this));
                },
                function(args) { // genable
                    this.gDisabled = args[0]==0;
                },
                function(args) { // clear
                    if (this.gDisabled) { return; }                    
                    this.g.clear(args[0]);
                },
                function(args) { // setviewport
                    if (this.gDisabled) { return; }
                    this.g.dropViewport();
                    this.g.pushViewport(
                        args[0]>>16,
                        args[1]>>16,
                        1.0-(args[2]/0x10000));
                },
                function(args) { // progstate
                    os.push(this.app.done?0:(this.app.active?1:2));
                },
                function(args) { // pollkeys
                    os.push(this.app.snapshotKeyStates());
                },

                //
                // Graphics - Image
                //
                function(args) { // img
                    throw new PinkyInterruptException(function() {
                        PinkyImage.loadPng(
                            args[0],
                            function(img) {
                                os.push(img);
                                this.returnFromInterrupt();
                            }.bind(this)
                        );
                    }.bind(this));
                },
                function(args) { // subimg
                    os.push(args[0].subImage(
                        args[1]>>16,
                        args[2]>>16,
                        args[3]>>16,
                        args[4]>>16,
                        args[5],
                        args[6]));
                },
                function(args) { // imgw
                    this.pushInt32(args[0].getWidth() <<16);
                },
                function(args) { // imgh
                    this.pushInt32(args[0].getHeight()<<16);
                },
                function(args) { // draw
                    if (this.gDisabled) { return; }
                    args[0].draw(this.g, args[1]>>16, args[2]>>16, args[3]);
                },
                function(args) { // drawsub
                    if (this.gDisabled) { return; }
                    args[0].drawStretched(
                        this.g,
                        args[1]>>16,
                        args[2]>>16,
                        args[5]>>16,
                        args[6]>>16,
                        args[3]>>16,
                        args[4]>>16,
                        args[5]>>16,
                        args[6]>>16,
                        args[7]);
                },
                function(args) { // drawstretched
                    if (this.gDisabled) { return; }
                    args[0].drawStretched(
                        this.g,
                        args[1]>>16,
                        args[2]>>16,
                        args[3]>>16,
                        args[4]>>16,
                        0,
                        0,
                        args[0].w,
                        args[0].h,
                        args[5]);
                },
                function(args) { // drawmap
                    if (this.gDisabled) { return; }
                    // TODO: use PinkyImage.drawBufferedMap() instead?
                    var tw = args[4]>>16, th = args[5]>>16;
                    PinkyImage.drawMap(
                        this.g,
                        args[0],
                        args[1],
                        args[2], args[3],
                        toInt(this.g.x /tw),
                        toInt(this.g.y /th),
                        toInt(this.g.vw/tw)+1,
                        toInt(this.g.vh/th)+1,
                        tw,
                        th,
                        args[6]>>16,
                        args[7]>>16);
                },
                function(args) { // drawtext
                    if (this.gDisabled) { return; }                    
                    PinkyImage.drawText(
                        this.g,
                        args[0],                            
                        args[1],
                        args[2],                    
                        args[3]>>16, // x
                        args[4]>>16, // y
                        args[5]>>16,
                        args[6]>>16,
                        args[7])
                },

                //
                // Graphics - Rect / Collisions
                //
                function(args) { // rempty
                    os.push(rectEmpty(args[0]));
                },
                function(args) { // rinter
                    os.push(rectIntersection(args[0], args[1]));
                },
                function(args) { // runion
                    os.push(rectUnion(args[0], args[1]));
                },
                function(args) { // rhitpoint
                    os.push(
                        rectHitPoint(args[0], args[1], args[2]));
                },
                function(args) { // rhitsegment
                    os.push(
                        rectHitSegment(
                            args[0], args[1], args[2], args[3], args[4]));
                },
                function(args) { // resolvehit
                    os.push(rectHitRect(args[0], args[1], args[2], args[3], args[4]));
                },
                function(args) { // resolvemaphit
                    var a1      = args[0];
                    var a2      = args[1];
                    var map     = args[2],
                        tsFlags = args[3],
                        mapW    = args[4],
                        mapH    = args[5],
                        tw      = args[6],
                        th      = args[7],
                        aFlags  = args[8];
                    os.push(rectHitMap(a1, a2, map, tsFlags, mapW, mapH, tw, th, aFlags));
                },
                    
                //
                // Sound
                //
                function(args) { // wav
                    throw new PinkyInterruptException(function() {
                        PinkySound.loadWav(
                            window.atob(args[0]),
                            function(wav) {
                                os.push(wav);
                                this.returnFromInterrupt();
                            }.bind(this)
                        );
                    }.bind(this));
                },
                function(args) { // playex
                    args[0].play(args[1]/0x10000, args[2]!=0);
                },
                function(args) { // stop
                    args[0].stop();
                },

                //
                // Game
                //
                function(args) {  // setgameparams
                    this.world_w = args[0];
                    this.world_h = args[1];
                    this.cam.setBounds(
                        0, 0,
                        this.world_w-(this.g.vw<<16),
                        this.world_h-(this.g.vh<<16));
                },
                function(args) {  // camreset
                    this.cam.setPath(args[0]);
                },
                function(args) {  // camadvance
                    this.cam.advance();
                },
                function(args) {  // cam
                    os.push(this.cam.toArray());
                },
                function(args) {  // drawsprite
                    if (this.gDisabled) { return; }
                    var spr = new Sprite(args[0]);
                    spr.draw(
                        this.g,
                        this.cam.x,
                        this.cam.y);
                },
                function(args) {  // drawtilemap
                    if (this.gDisabled) { return; }
                    var tilemap = new Tilemap(args[0]);
                    tilemap.draw(
                        this.g,
                        this.cam.x,
                        this.cam.y,
                        this.world_w,
                        this.world_h,
                        this.g.vw<<16,
                        this.g.vh<<16);
                },
                function(args) {  // drawtileobj
                    if (this.gDisabled) { return; }               
                    var tileobj = args[0];
                    var spr = tileobj[0][1][tileobj[1]];
                    spr.drawStretched(
                        this.g,
                        tileobj[2]>>16,
                        tileobj[3]>>16,
                        tileobj[4]>>16,
                        tileobj[5]>>16,
                        0,
                        0,
                        tileobj[4]>>16,
                        tileobj[5]>>16,
                        tileobj[6]);
                },
                function(args) {  // settilesetframe
                    var tileset = new Tileset(args[0]);
                    tileset.setFrame(args[1]);
                },
                function(args) { // movesprite
                    var spr = new Sprite(args[0]);
                    var otherSprites = args[1];
                    var tilemap = new Tilemap(args[2]);
                    
                    var dx = spr.fx * (
                        spr.ax >= 0
                            ? Math.min(spr.vx+spr.ax, spr.vxt)
                            : Math.max(spr.vx+spr.ax, 0));
                    var dy = spr.fy * (
                        spr.ay >= 0
                            ? Math.min(spr.vy+spr.ay, spr.vyt)
                            : Math.max(spr.vy+spr.ay, 0));
                    spr.old_bnds[0] = spr.bnds[0];
                    spr.old_bnds[1] = spr.bnds[1];
                    spr.old_bnds[2] = spr.bnds[2];
                    spr.old_bnds[3] = spr.bnds[3];
                    spr.bnds[0] += dx;
                    spr.bnds[1] += dy;
                    spr.bnds[2] += dx;
                    spr.bnds[3] += dy;

                    var hits = 0;

                    // Skip checking for hits (but still zero out .hits fields)
                    // if hit checking is disabled or sprite didn't move
                    var hidsToCheck = spr.hids_on & ~spr.hids_off;
                    var skipCheck = hidsToCheck == 0;
                    var hflags = 0xF0000000 | spr.weight | hidsToCheck | 0xF;
                    
                    if (!skipCheck) {
                        hits = rectHitMap(
                            spr.old_bnds,
                            spr.bnds,
                            tilemap.data,
                            tilemap.tileset.flags,
                            tilemap.w,
                            tilemap.h,
                            tilemap.tw,
                            tilemap.th,
                            hflags);
                    }

                    for (var i = 0; i < otherSprites.length; i++) {
                        var otherHits;
                        var other = new Sprite(otherSprites[i]);
                        // Other sprite must have reciprocal interest in sprite, in order to check
                        if (skipCheck || (spr.hid&(other.hids_on&~other.hids_off)) == 0) {
                            otherHits = 0;
                        } else {
                            otherHits = rectHitRect(
                                spr.old_bnds,
                                spr.bnds,
                                other.bnds,
                                hflags,
                                other.hid|other.sides|other.weight);
                        }
                        args[1][i][21] = otherHits;
                        hits |= otherHits;
                    }
                    
                    args[0][21] = hits;
                    args[0][8]  = (spr.bnds[0]-spr.old_bnds[0]) * spr.fx;
                    args[0][9]  = (spr.bnds[1]-spr.old_bnds[1]) * spr.fy;
                }
            ];
        },

        pushInt32: function(value) {
            this.os.push(toInt(value));
        },
                        
        requestDestroy: function() {
            this.app.done = true;
        },
        
        destroy: function() {
            this.app.removeEventListeners();
            this.onDestroy();
        }
    });

    PinkyVM.fromJamcode = function(jamcode, screenEl, onError, onResize) {
        var code = [];
        var lines = jamcode.split("\n");
        var maxGlobalIndex = 0;
        var literals = [];
        var fileOffs = [];

        for (var i = 0; i < lines.length; i++) {
            if (!lines[i]) {
                continue;
            }

            var line = lines[i];
            var kw = line.slice(0, 4).toLowerCase();
            var operand = line.slice(5);
            var intOperand = 0;
            var opcode = KEYWORDS.findIndex(
                function(el, i, kws) {
                    return el == kw;
                });

            if (opcode == -1) {
                opcode = parseInt(kw.slice(1), 16);
            }

            if (opcode == LODI) {
                intOperand = literals.length;
                literals.push(parseInt(operand)&0xFFFFFFFF);
            } else if (opcode == LODF) {
                intOperand = literals.length;
                var operands = operand.split(' ');
                literals.push(new PinkyProc(
                    parseInt(operands[0]),
                    parseInt(operands[1])
                ));
            } else if (opcode == LODS) {
                intOperand = literals.length;
                literals.push(unescapeTextArray(operand));
            } else {
                intOperand = parseInt(operand) || 0;
                if (opcode == GETG || opcode == SETG) {
                    maxGlobalIndex = Math.max(maxGlobalIndex, intOperand)
                } else if (opcode == REM) {
                    if (operand[0] == '@') {
                        fileOffs.push([code.length, operand.slice(1)]);
                    }
                }
            }

            code.push(opcode);            
            code.push(intOperand);
        }
        
        return new PinkyVM(screenEl, code, fileOffs, literals, maxGlobalIndex+1, onError, onResize);
    }

    /**
     *
     * Entry-point to the Pinky VM
     *
     */
    
    pinky.run = function(screenEl, jamcode, onError, onResize, onDestroy) {
        var vm = PinkyVM.fromJamcode(
            jamcode,
            screenEl,
            function(vm, e) {
                var errorText;
                if (e instanceof String) {
                    errorText = "\""+e+"\"";
                } else {
                    errorText = e && ("`"+e.constructor.name+"`");
                }
                var tb = vm.stackTrace().join("\n");
                (onError||console.error)("Error "+errorText+":\n"+tb);
            },
            onResize,
            onDestroy
        ).run();
    };

    return pinky;
}(Pinky || {}));
