# README #

### What is this repository for? ###

Pinky is a minimalistic language inspired by QBasic. The core of the compiler is around 1000 lines of Python code, so anyone can read it, learn from it, and build on it! Despite the tiny footprint, Pinky has everything a hobbyist needs to make small games, out of the box.

### How do I get set up? ###

Python 3 is required (ported from 2.7 at the start of 2020).

If you choose to run programs that use sound or graphics, the only other dependency is [pygame](http://www.pygame.org/download.shtml).
