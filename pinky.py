#!/usr/local/bin/python3

#
# Copyright (c) 2020 Curt Knobloch
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import struct, array, base64, math, os, time, functools
from collections import OrderedDict as D

LIB_PATH = os.path.dirname(__file__)
VERSION = 'pork chop'

#
# UTILITIES
#

def unescape_text(text):
    i, n = 0, len(text)
    un_text = ''        
    while i < n:
        if text[i] != '\\':
            un_text += text[i]
            i += 1                            
        else:
            i += 1
            if text[i].isdigit():
                digits = ''
                while text[i].isdigit():
                    digits += text[i]
                    i += 1
                un_text += chr(int(digits))
            else:
                c = text[i]
                un_text += {'n': '\n', 't': '\t'}.get(c, c)
                i += 1
    return un_text

def create_null_list(data=None, n=None):
    if n is not None:
        data = [None for _ in range(n)]
        for i, v in enumerate(data or ()):
            data[i] = v
    return data

def list_fill(data, v, i, n):
    for i in range(i, i+n):
        data[i] = v
    return data

def list_copy(data, other, i, n):
    for j in range(n):
        data[i+j] = other[j]
    return data

def cmp(a, b):
    return (a > b) - (a < b)

def int32(value):
    return ((value&0xFFFFFFFF)^0x80000000) - 0x80000000

_BIT_POS = [0, 0, 1, 26, 2, 23, 27, 0, 3, 16, 24, 30, 28, 11, 0, 13, 4, 7, 17, 0, 25, 22, 31, 15, 29, 10, 12, 6, 0, 21, 14, 9, 5, 20, 8, 19, 18]

def ffb(x):
    # NOTE: this only works for values where 1 bit is set
    return _BIT_POS[(x&-x)%37]
    
def clamp(x, a, b):
    return min(max(x, a), b)

def float_to_fp(value):
    return int(value * 0x10000)

def fp_to_float(value):
    return value / 0x10000

def fp_div(a, b):
    return (a<<16) // b

def fp_mul(a, b):
    return (a*b) >> 16

def fp_hypot(a, b):
    return int(math.hypot(fp_to_float(a), fp_to_float(b))*0x10000)

#
# VIRTUAL MACHINE
#

NOP , NULL, ZERO, ONE , LODI, LODS, LODF, ADD ,\
SUB , MUL , DIV , MULF, DIVF, NOT , NEG , LT  ,\
GT  , LTE , GTE , EQ  , NE  , JMP , JIF , JZ  ,\
JSR , RET , DUP , DROP, SETA, SETI, SETL, SETG,\
GETA, GETI, GETL, GETG, NEW , NEWV, REM , EVAL,\
MARK, STEP, CLR , ARGS = JAM_OPS = range(44)

# These are common combinations of operations that exist
# strictly for a performance kick, even moreso than standard builtins
COMPOUND_BUILTINS = ('GETL_GETL', 'GETL_GETA', 'GETL_LODI', 'ADD_MUL', 'INCL', 'GETL_LODI_ADD_SETL', 'GETL_GETL_GETI_SETL', 'GETL_GETL_LT_JZ', 'GETL_1', 'ZERO_NE_JZ')

BUILTINS = {op: i+len(JAM_OPS) for i, op in enumerate(('ASSERT', 'PRINT', 'TIME', 'RAND', 'SEED', 'WEAK', 'IS', 'DEFAULT', 'SELECT', 'SLEEP', 'MACRO', 'MACROSTEP', 'GETTYPE', 'IDENTITY', 'ATTRS', 'AND', 'OR', 'XOR', 'SHL', 'SHR', 'ROT', 'BITS', 'SETBITS', 'SIGNED', 'MOD', 'MIN', 'MAX', 'ABS', 'ICMP', 'ITOS', 'FMOD', 'FMIN', 'FMAX', 'FABS', 'FCMP', 'FTOS', 'FTOI', 'ITOF', 'FLOOR', 'CEIL', 'FRAC', 'POW', 'SQRT', 'HYPOT', 'SIN', 'COS', 'TAN', 'ASIN', 'ACOS', 'ATAN', 'CONCAT', 'CHR', 'ORD', 'SLEN', 'SCMP', 'STOI', 'DECODEB64', 'B64', 'SUBSTR', 'SLICE', 'REVERSE', 'FILL', 'COPY', 'SORT', 'APPLY', 'ALEN', 'ARRAY', 'BLOB', 'BLOAD', 'BSAVE', 'BLEN', 'GET', 'SET', 'GETS', 'SETS')+COMPOUND_BUILTINS)}

JAM_COMMANDS_TO_OPS = dict(zip((
    'nop ', 'null', 'zero', 'one ', 'lodi', 'lods', 'lodf', '+   ',
    '-   ', '*   ', '/   ', '*f  ', '/f  ', 'not ', 'neg ', '<   ',
    '>   ', '<=  ', '>=  ', '==  ', '!=  ', 'jmp ', 'jif ', 'jz  ',
    'jsr ', 'ret ', 'dup ', 'drop', 'seta', 'seti', 'setl', 'setg',
    'geta', 'geti', 'getl', 'getg', 'new ', 'newv', '#   ', 'eval',
    'mark', 'step', 'clr ', 'args'), JAM_OPS))

class RuntimeError(Exception):
    def __init__(self, message):
        super(RuntimeError, self).__init__(message)

class PinkyObject:
    type_id = 0

class PinkyBlob(PinkyObject):
    type_id = 4
    def __init__(self, capacity=0, data=None):
        self.data = array.array('B', data or [0 for i in range(capacity)])
    def len(self):
        return len(self.data)
    @staticmethod
    def load(k):
        try:
            with open('{}.sto'.format(k), 'rb') as f:
                blob = PinkyBlob()
                blob.data.frombytes(f.read())
        except IOError:
            blob = None
        return blob
    def save(self, k):
        with open('{}.sto'.format(k), 'wb') as f:
            f.write(self.data.tobytes())
    def get_byte(self, i):
        return self.data[i]
    def get_str(self, i, n):
        return self.data[i:i+n].tobytes().decode('ascii')
    def set_byte(self, i, v):
        self.data[i] = v
    def set_str(self, i, s, n):
        for j in range(n):
            self.data[i+j] = ord(s[j])

class PinkyProc(PinkyObject):
    type_id = 5
    def __init__(self, ip, vn=None, vars=None):
        self.ip   = ip
        self.vars = (([None]*vn) if vn else []) if vars is None else vars
        self.p    = 0
        
class PinkyVMDelegate(object):
    def eval(self, vm, opcode, args):
        raise NotImplementedError

class PinkyVM:
    def __init__(self, code, file_offs=[], global_consts=[], num_globals=0, options={}):
        self.os        = []    # object stack
        self.ls        = {}    # line   stack
        self.fs        = []    # frame  stack (just the ip for each frame)
        self.vs        = []    # vars   stack (os base index for each frame)
        self.cv        = code
        self.gv        = global_consts
        self.options   = options
        self.file_offs = [(0, "")] + file_offs + [(len(code), "")]
        self._delegate = PinkyCoreDelegate(self)
        self._run_proc = PinkyProc(0, num_globals)
        self.op_lookup = self.main_ops() + self.builtins() + self.compound_ops()
        self.base_time = time.time()
        self.rand_seed = 0
                
    def main_ops(self):
        delegate = self._delegate
        os = self.os
        fs = self.fs
        vs = self.vs
        ls = self.ls
        cv = self.cv        
        gv = self.gv
        
        def nop_(li):
            fs[-1] += 1
        
        def null(ci):
            os.append(None)
            fs[-1] += 1
        
        def zero(ci):
            os.append(0)
            fs[-1] += 1
        
        def one_(ci):
            os.append(1)
            fs[-1] += 1

        def lod_(ci):
            os.append(gv[cv[ci+1]])
            fs[-1] += 1
        
        def add_(ci):
            os[-1] = os[-2] + os.pop()
            fs[-1] += 1
        
        def sub_(ci):
            os[-1] = os[-2] - os.pop()
            fs[-1] += 1
        
        def mul_(ci):
            os[-1] = os[-2] * os.pop()
            fs[-1] += 1
        
        def div_(ci):
            os[-1] = os[-2] // os.pop()
            fs[-1] += 1
        
        def mulf(ci):
            os[-1] = (os[-2]*os.pop()) >> 16
            fs[-1] += 1
        
        def divf(ci):
            os[-1] = (os[-2]<<16) // os.pop()
            fs[-1] += 1
        
        def geti(ci):
            os[-1] = os[-2][os.pop()]
            fs[-1] += 1
        
        def seti(ci):
            os[-2][os[-1]] = os.pop()
            os.pop()
            os.pop()            
            fs[-1] += 1
        
        def dup_(ci):
            os.append(os[-1])
            fs[-1] += 1
        
        def drop(ci):
            os.pop()
            fs[-1] += 1

        def not_(ci):
            os[-1] = os[-1] == 0
            fs[-1] += 1
        
        def neg_(ci):
            os[-1] = -os[-1]
            fs[-1] += 1
            
        def lt__(ci):
            os[-1] = os[-2] < os.pop()
            fs[-1] += 1
        
        def gt__(ci):
            os[-1] = os[-2] > os.pop()
            fs[-1] += 1
        
        def lte_(ci):
            os[-1] = os[-2] <=os.pop()
            fs[-1] += 1
        
        def gte_(ci):
            os[-1] = os[-2] >= os.pop()
            fs[-1] += 1
        
        def eq__(ci):
            os[-1] = os[-2] == os.pop()
            fs[-1] += 1

        def ne__(ci):
            os[-1] = os[-2] != os.pop()
            fs[-1] += 1
        
        def args(ci):
            os.extend(os.pop())
            fs[-1] += 1

        def geta(ci):
            os[-1] = os[-1][cv[ci+1]]
            fs[-1] += 1

        def getl(ci):
            os.append(os[vs[-1]+cv[ci+1]])
            fs[-1] += 1

        def getg(ci):
            os.append(os[cv[ci+1]])
            fs[-1] += 1

        def seta(ci):
            os[-1][cv[ci+1]] = os.pop()
            os.pop()
            fs[-1] += 1

        def setl(ci):
            os[vs[-1]+cv[ci+1]] = os.pop()
            fs[-1] += 1

        def setg(ci):
            os[cv[ci+1]] = os.pop()
            fs[-1] += 1

        def rem_(ci):
            ls[len(fs)-1] = cv[ci+1]
            fs[-1] += 1
            
        def new_(ci):
            os.append(create_null_list(n=cv[ci+1]))
            fs[-1] += 1

        def newv(ci):
            os.append(create_null_list(data=self.multi_pop(cv[ci+1])))
            fs[-1] += 1
            
        def jmp_(ci):
            fs[-1] = cv[ci+1]

        def jif_(ci):
            if os.pop() != 0:
                fs[-1] = cv[ci+1]
            else:
                fs[-1] += 1

        def jz__(ci):
            if os.pop() == 0:
                fs[-1] = cv[ci+1]
            else:
                fs[-1] += 1

        def eval(ci):
            delegate.eval(cv[ci+1], os[vs[-1]+1:])
            ret_(ci)  # technically ip should be incremented here, but who really cares
        
        def mark(ci):
            fs[-1] += 1
            os[vs[-1]].ip = fs[-1] + cv[ci+1]

        def step(ci):
            os.append(os[vs[-1]].p)
            fs[-1] += 1

        def clr_(ci):
            fs[-1] += 1
            proc = os[vs[-1]]
            proc.p = 1
            proc.ip = fs[-1]

        def jsr_(ci):
            vn = cv[ci+1] + 1  # number of args
            vi = len(os) - vn    # set vi to top of stack minus number of args pushed
            proc = os[vi]        # get proc to jump to
            proc.p += 1
            fs.append(proc.ip)
            vs.append(vi)
            os.extend(proc.vars[vn:])
            
        def ret_(ci):
            vi = vs[-1]
            del os[vi:vi+len(os[vi].vars)]
            fs.pop()
            vs.pop()
            fs[-1] += 1            
                    
        return [
            nop_,
            null,
            zero,
            one_,
            lod_,
            lod_,
            lod_,
            add_,
            sub_,
            mul_,
            div_,
            mulf,
            divf,
            not_,
            neg_,
            lt__,
            gt__,
            lte_,
            gte_,
            eq__,
            ne__,
            jmp_,
            jif_,
            jz__,
            jsr_,
            ret_,
            dup_,
            drop,
            seta,
            seti,
            setl,
            setg,
            geta,
            geti,
            getl,
            getg,
            new_,
            newv,
            rem_,
            eval,
            mark,
            step,
            clr_,
            args,
        ]        

    def builtins(self):
        os = self.os
        fs = self.fs
        vs = self.vs

        def _assert(ci):
            assert os.pop()
            fs[-1] += 1

        def _print(ci):
            print(os.pop())
            fs[-1] += 1            

        def _time(ci):
            os.append(int(round((time.time()-self.base_time)*1000)))
            fs[-1] += 1

        def _rand(ci):
            end, start = os.pop(), os.pop()
            self.rand_seed = (self.rand_seed*9301+49297) % 233280
            os.append(start+self.rand_seed*(end-start)//233280)
            fs[-1] += 1            

        def _seed(ci):
            self.rand_seed = os.pop()
            fs[-1] += 1            
            
        def _weak(ci):
            fs[-1] += 1        

        def _is(ci):
            os[-1] = os[-2] is os.pop()
            fs[-1] += 1
            
        def _default(ci):
            if os[-2] is None:
                os[-1] = os.pop()
            else:
                os.pop()
            fs[-1] += 1
            
        def _select(ci):
            if os.pop(-3):
                os.pop()
            else:
                os[-1] = os.pop()
            fs[-1] += 1
            
        def _sleep(ci):
            time.sleep(os.pop()/1000)
            fs[-1] += 1
            
        def _macro(ci):
            proc = os.pop()
            os.append(PinkyProc(proc.ip, vars=proc.vars))
            fs[-1] += 1
            
        def _step(ci):
            os.append(os.pop().p)
            fs[-1] += 1
            
        def _gettype(ci):
            a = os.pop()
            if   isinstance(a, int):
                os.append(1)
            elif isinstance(a, str):
                os.append(2)
            elif isinstance(a, list):
                os.append(3)
            elif isinstance(a, PinkyObject):
                os.append(a.type_id)
            else:
                os.append(0)
            fs[-1] += 1
            
        def _clone(ci):
            os.append(list(os.pop()))
            fs[-1] += 1

        def _attrs(ci):
            fs[-1] += 1

        def _and(ci):
            os[-1] = os[-2] & os.pop()
            fs[-1] += 1
            
        def _or(ci):
            os[-1] = os[-2] | os.pop()
            fs[-1] += 1
            
        def _xor(ci):
            os[-1] = os[-2] ^ os.pop()
            fs[-1] += 1
            
        def _shl(ci):
            os[-1] = os[-2] << os.pop()
            fs[-1] += 1
            
        def _shr(ci):
            os[-1] = os[-2] >> os.pop()            
            fs[-1] += 1
            
        def _rot(ci):
            n, i, x = os.pop(), os.pop(), os.pop()
            os.append(int32(((x&((1<<n)-1))>>i)|(x<<(n-i)&((1<<n)-1))))
            fs[-1] += 1
                        
        def _bits(ci):
            mask = os.pop()
            os[-1] = ((os[-1]&mask)>>ffb(mask))
            fs[-1] += 1
            
        def _setbits(ci):
            value, mask = os.pop(), os.pop()
            os[-1] = ((os[-1]&~mask)|(value<<ffb(mask)))
            fs[-1] += 1

        def _signed(ci):
            os[-1] = int32(os[-1])
            fs[-1] += 1
            
        def _mod(ci):
            os[-1] = os[-2] % os.pop()
            fs[-1] += 1
            
        def _min(ci):
            os[-1] = min(os[-2], os.pop())
            fs[-1] += 1
                        
        def _max(ci):
            os[-1] = max(os[-2], os.pop())
            fs[-1] += 1
            
        def _abs(ci):
            os[-1] = abs(os[-1])
            fs[-1] += 1
            
        def _icmp(ci):
            os[-1] = cmp(os[-2], os.pop())
            fs[-1] += 1
            
        def _itos(ci):
            os[-1] = str(os[-1])
            fs[-1] += 1
                    
        def _ftos(ci):
            os[-1] = str(fp_to_float(os[-1]))
            fs[-1] += 1
                        
        def _ftoi(ci):
            os[-1] = os[-1] >> 16
            fs[-1] += 1
                        
        def _itof(ci):
            os[-1] = (os[-2]<<16) | (os.pop()&0xFFFF)
            fs[-1] += 1
                        
        def _floor(ci):
            os[-1] = (os[-1]) & ~0xFFFF
            fs[-1] += 1
                        
        def _ceil(ci):
            os[-1] = (os[-1]+0xFFFF) & ~0xFFFF
            fs[-1] += 1
                        
        def _frac(ci):
            os[-1] = ((os[-1])& 0xFFFF)
            fs[-1] += 1
                        
        def _pow(ci):
            os[-1] = float_to_fp(pow(fp_to_float(os[-2]), fp_to_float(os.pop())))
            fs[-1] += 1
                        
        def _sqrt(ci):
            os[-1] = float_to_fp(math.sqrt (fp_to_float(os[-1])))
            fs[-1] += 1
                        
        def _hypot(ci):
            os[-1] = float_to_fp(math.hypot(fp_to_float(os[-2]), fp_to_float(os.pop())))
            fs[-1] += 1
                        
        def _sin(ci):
            os[-1] = float_to_fp(math.sin  (fp_to_float(os[-1])))
            fs[-1] += 1
            
        def _cos(ci):
            os[-1] = float_to_fp(math.cos  (fp_to_float(os[-1])))
            fs[-1] += 1
            
        def _tan(ci):
            os[-1] = float_to_fp(math.tan  (fp_to_float(os[-1])))
            fs[-1] += 1
            
        def _asin(ci):
            os[-1] = float_to_fp(math.asin (fp_to_float(os[-1])))
            fs[-1] += 1
            
        def _acos(ci):
            os[-1] = float_to_fp(math.acos (fp_to_float(os[-1])))
            fs[-1] += 1
            
        def _atan(ci):
            os[-1] = float_to_fp(math.atan (fp_to_float(os[-1])))
            fs[-1] += 1
            
        def _concat(ci):
            os[-1] = os[-2] + os.pop()
            fs[-1] += 1
            
        def _chr(ci):
            os[-1] = chr(os[-1])
            fs[-1] += 1
            
        def _ord(ci):
            i, text = os.pop(), os.pop()
            os.append(ord(text[i]))
            fs[-1] += 1
            
        def _slen(ci):
            os[-1] = len(os[-1])
            fs[-1] += 1
            
        def _scmp(ci):
            os[-1] = cmp(os[-2], os.pop())
            fs[-1] += 1
            
        def _stoi(ci):
            os[-1] = int(os[-1])
            fs[-1] += 1
            
        def _decodeb64(ci):
            os[-1] = PinkyBlob(data=base64.b64decode(os[-1]))
            fs[-1] += 1
            
        def _b64(ci):
            os[-1] = base64.b64encode(os[-1].data).decode('ascii')
            fs[-1] += 1
            
        def _substr(ci):
            n, i, text = os.pop(), os.pop(), os.pop()
            os.append(text[i:i+n])
            fs[-1] += 1
            
        def _slice(ci):
            n, i, array = os.pop(), os.pop(), os.pop()
            os.append(array[i:i+n])
            fs[-1] += 1
            
        def _reverse(ci):
            os[-1] = list(reversed(os[-1]))
            fs[-1] += 1
            
        def _fill(ci):
            n, i, value, array = os.pop(), os.pop(), os.pop(), os.pop()
            list_fill(array, value, i, n)
            fs[-1] += 1
            
        def _copy(ci):
            n, i, src_array, dest_array = os.pop(), os.pop(), os.pop(), os.pop()
            list_copy(dest_array, src_array, i, n)
            fs[-1] += 1
            
        def _sort(ci):
            cmp_func, array = os.pop(), os.pop()
            ip = fs[-1]
            array.sort(
                key=functools.cmp_to_key(lambda a, b: self.call_pop(cmp_func, [a, b]))
            )
            fs[-1] = ip + 1
            
        def _apply(ci):
            proc, array = os.pop(), os.pop()
            ip = fs[-1]
            for x in array:
                self.call(proc, [x])
            fs[-1] = ip + 1
                
        def _alen(ci):
            os[-1] = len(os[-1])
            fs[-1] += 1
            
        def _array(ci):
            os.append(create_null_list(n=os.pop()))
            fs[-1] += 1
            
        def _blob(ci):
            os.append(PinkyBlob(os.pop()))
            fs[-1] += 1
            
        def _bload(ci):
            os[-1] = PinkyBlob.load(os[-1])
            fs[-1] += 1
            
        def _bsave(ci):
            name, blob = os.pop(), os.pop()
            blob.save(name)
            fs[-1] += 1
            
        def _blen(ci):
            os[-1] = os[-1].len()
            fs[-1] += 1
            
        def _get(ci):
            i, blob = os.pop(), os.pop()
            os.append(blob.get_byte(i))
            fs[-1] += 1
            
        def _set(ci):
            value, i, blob = os.pop(), os.pop(), os.pop()
            blob.set_byte(i, value)
            fs[-1] += 1
            
        def _gets(ci):
            n, i, blob = os.pop(), os.pop(), os.pop()
            os.append(blob.get_str(i, n))
            fs[-1] += 1
            
        def _sets(ci):
            n, text, i, blob = os.pop(), os.pop(), os.pop(), os.pop()
            blob.set_str(i, text, n)
            fs[-1] += 1
            
        return [
            _assert,
            _print,
            _time,
            _rand,
            _seed,
            _weak,
            _is,
            _default,
            _select,
            _sleep,
            _macro,
            _step,
            _gettype,
            _clone,
            _attrs,
            _and,
            _or,
            _xor,
            _shl,
            _shr,
            _rot,
            _bits,
            _setbits,
            _signed,
            _mod,
            _min,
            _max,
            _abs,
            _icmp,
            _itos,
            _mod,  # fmod
            _min,  # fmin
            _max,  # fmax
            _abs,  # fabs
            _icmp, # fcmp
            _ftos,
            _ftoi,
            _itof,
            _floor,
            _ceil,
            _frac,
            _pow,
            _sqrt,
            _hypot,
            _sin,
            _cos,
            _tan,
            _asin,
            _acos,
            _atan,
            _concat,
            _chr,
            _ord,
            _slen,
            _scmp,
            _stoi,
            _decodeb64,
            _b64,
            _substr,
            _slice,
            _reverse,
            _fill,
            _copy,
            _sort,
            _apply,
            _alen,
            _array,
            _blob,
            _bload,
            _bsave,
            _blen,
            _get,
            _set,
            _gets,
            _sets,
        ]
    
    def compound_ops(self):
        os = self.os
        fs = self.fs
        vs = self.vs
        cv = self.cv
        gv = self.gv
        
        def _getl_getl(ci):
            os.append(os[vs[-1]+cv[ci+1]])
            os.append(os[vs[-1]+cv[ci+3]])
            fs[-1] += 2

        def _getl_geta(ci):
            os.append(os[vs[-1]+cv[ci+1]][cv[ci+3]])
            fs[-1] += 2

        def _getl_lodi(ci):
            os.append(os[vs[-1]+cv[ci+1]])
            os.append(gv[cv[ci+3]])
            fs[-1] += 2

        def _add_mul(ci):
            os[-1] = (os.pop()+os.pop()) * os[-1]
            fs[-1] += 2

        def _incl(ci):
            os[vs[-1]+cv[ci+7]] += 1
            fs[-1] += 4

        def _getl_lodi_add__setl(ci):
            os[vs[-1]+cv[ci+7]] = os[vs[-1]+cv[ci+1]] + gv[cv[ci+3]]
            fs[-1] += 4

        def _getl_getl_geti_setl(ci):
            os[vs[-1]+cv[ci+7]] = os[vs[-1]+cv[ci+1]][os[vs[-1]+cv[ci+3]]]
            fs[-1] += 4

        def _getl_getl_lt_jz(ci):
            if os[vs[-1]+cv[ci+1]] < os[vs[-1]+cv[ci+3]]:
                fs[-1] += 4
            else:
                fs[-1] = cv[ci+7]

        def _getl_1(ci):
            os.append(os[vs[-1]+1])
            fs[-1] += 1

        def _zero_ne_jz(ci):
            if os.pop() == 0:
                fs[-1] = cv[ci+5]
            else:
                fs[-1] += 3
            
        return [
            _getl_getl,
            _getl_geta,
            _getl_lodi,
            _add_mul,
            _incl,            
            _getl_lodi_add__setl,
            _getl_getl_geti_setl,
            _getl_getl_lt_jz,
            _getl_1,
            _zero_ne_jz,
        ]        

    def run(self, args=[]):
        try:
            self.fs.append(0)  #
            self.vs.append(0)  # Save dummy frame for final RET
            self.call(self._run_proc, args)
        except Exception as e:
            self.print_stack_trace()
            raise

    def call_pop(self, proc, args):
        self.call(proc, args)
        return self.pop()
        
    def call(self, proc, args):
        os, fs, vs, cv, op_lookup = self.os, self.fs, self.vs, self.cv, self.op_lookup
        fs.append(proc.ip)
        vs.append(len(os))
        os.extend([proc]+args+proc.vars[1+len(args):])
        fi = len(fs)
        while fi <= len(fs):
            ci = fs[-1] << 1
            op_lookup[cv[ci]](ci)

    def push(self, obj):
        self.os.append(obj)
        
    def push_int32(self, obj):
        self.os.append(int32(obj))

    def push_bool(self, obj):
        self.os.append(1 if obj else 0)
        
    def pop(self):
        return self.os.pop()

    def multi_pop(self, n):
        if not n:
            return []
        objs = self.os[-n:]
        del self.os[-n:]
        return objs   

    def _get_filename_for_ip(self, ip):
        name = None
        for off, _name in self.file_offs:
            if (ip+ip) <= off:
                break
            name = _name
        return name

    def print_stack_trace(self):
        for i, ip in enumerate(self.fs):
            print("{}:{}:{}".format(
                self._get_filename_for_ip(ip) or '?',
                ip+1,  # tools usually number lines starting at 1
                self.ls.get(i, '?')))

    @staticmethod
    def optimize_code(code):
        def match_code_pattern(pattern):
            pattern_size = len(pattern)
            def inner(code, i):
                return pattern_size if list(code[i:i+pattern_size*2:2]) == pattern else 0
            return inner
        incl_pattern_matcher = match_code_pattern([GETL, ONE, ADD, SETL])
        def match_incl(code, i):
            pattern_size = incl_pattern_matcher(code, i)
            return pattern_size if pattern_size and code[i+1] == code[i+3*2+1] else 0
        def match_get_first_arg(code, i):
            return 1 if code[i] == GETL and code[i+1] == 1 else 0
        i = 0
        while i < len(code):
            for matcher, op_key in [
                    (match_incl, 'INCL'),  # ex. "i = i + 1"
                    (match_code_pattern([ZERO, NE, JZ]), 'ZERO_NE_JZ'),
                    (match_code_pattern([GETL, LODI, ADD, SETL]), 'GETL_LODI_ADD_SETL'),
                    (match_code_pattern([GETL, GETL, GETI, SETL]), 'GETL_GETL_GETI_SETL'),
                    (match_code_pattern([GETL, GETL, LT, JZ]), 'GETL_GETL_LT_JZ'),
                    (match_code_pattern([GETL, GETL]), 'GETL_GETL'),
                    (match_code_pattern([GETL, GETA]), 'GETL_GETA'),
                    (match_code_pattern([GETL, LODI]), 'GETL_LODI'),
                    (match_code_pattern([ADD, MUL]), 'ADD_MUL'),
                    (match_get_first_arg, 'GETL_1'),
            ]:
                match_size = matcher(code, i)
                if match_size:
                    code[i] = BUILTINS[op_key]
                    i += match_size * 2         
                    break
            else:
                i += 2
            
    @staticmethod
    def from_jamcode(jamcode, options={}):
        code = array.array('i')
        global_consts = []
        file_offs = []
        max_global_index = 0

        for line in jamcode.split('\n')+['ret ']:
            opcode = line[:4]
            if opcode in JAM_COMMANDS_TO_OPS:
                opcode = JAM_COMMANDS_TO_OPS[opcode]
            else:
                opcode = int(opcode[1:], 16)
                
            operand = line[5:]

            if   opcode == LODI:
                int_operand = len(global_consts)
                global_consts.append(int(operand))
            elif opcode == LODF:
                ip, vn = operand.split(' ')
                int_operand = len(global_consts)
                global_consts.append(PinkyProc(int(ip), int(vn)))
            elif opcode == LODS:
                int_operand = len(global_consts)
                global_consts.append(unescape_text(operand))
            else:
                int_operand = int(operand) if operand.isdigit() else 0
                if opcode in {GETG, SETG}:
                    max_global_index = max(max_global_index, int_operand)
                elif opcode == REM:
                    if operand[0] == '@':
                        file_offs.append((len(code), operand[1:]))

            code.append(opcode)                        
            code.append(int_operand)

        if not options.get('do_not_optimize'):
            PinkyVM.optimize_code(code)
                    
        return PinkyVM(
            code,
            file_offs,
            global_consts,
            max_global_index+1,
            options=options)

#
# VM CORE DELEGATE
#
    
class PinkyCoreDelegate(PinkyVMDelegate):    
    def __init__(self, vm):
        self.progstate = 1
        self.keystates = array.array('B', [0 for _ in range(256)])
        self.native_lookup = self.make_native_lookup(vm)

    def eval(self, opcode, args):
        self.native_lookup[opcode](args)
        
    def make_native_lookup(self, vm):
        def ginit(args):
            pygame.init()
            pygame.mixer.init(frequency=44100,
                              size=16,
                              channels=1,
                              buffer=4096)            
            pygame.display.set_caption(args[0])
            w, h = args[1]>>16, args[2]>>16
            self.g = PygameGraphics(
                max(w, 800),
                max(h, 600),
                w, h,
                options=vm.options)
            self.g.screen.fill((0x00, 0x00, 0x00))
            self.g_disabled = False
            self.cam = Cam([0, 0, 0, 0, 0, 0, 0, 0, [], 0, 0, 0, 0])
            
        def gbegin(args):
            if self.g_disabled:
                return
            self.g_tx = 0
            self.g_ty = 0
            self.g.back_buffer = pygame.transform.scale(
                self.g.back_buffer, (self.g.virtual_w, self.g.virtual_h))
            
        def gend(args):
            if self.g_disabled:
                return
            self.g.flush()
            
        def genable(args):
            self.g_disabled = args[0] is 0
            
        def clear(args):
            if self.g_disabled:
                return
            rgb = args[0]
            self.g.back_buffer.fill((rgb>>16&0xFF, rgb>>8&0xFF, rgb&0xFF))
            
        def setviewport(args):
            if self.g_disabled:
                return
            x  = args[0]
            y  = args[1]
            z  = fp_to_float(args[2])            
            vw = int((1.0-z)*self.g.virtual_w)
            vh = int((1.0-z)*self.g.virtual_h)
            if (vw < self.g.back_buffer.get_width() or
                vh < self.g.back_buffer.get_height()):
                print('WARNING: downscaling back-buffer will result '
                      'in degraded graphics quality')
            self.g_tx = x
            self.g_ty = y
            self.g.back_buffer = pygame.transform.scale(
                self.g.back_buffer,
                (vw, vh))
            
        def progstate(args):
            vm.push_int32(self.progstate)
            
        def pollkeys(args):
            self.g  # initialize if hasn't been already
            self.keystates[0] = 0
            self.keystates[0x3A] = 0
            self.keystates[0x3B] = 0
            self.keystates[0x3C] = 0
            self.keystates[0x3D] = 0
            for i, keystate in enumerate(self.keystates):
                self.keystates[i] &= 4  # clear down/up            
            for event in pygame.event.get():
                if   event.type == pygame.QUIT:
                    self.progstate = 0
                elif event.type == pygame.ACTIVEEVENT:
                    # this event doesn't seem to be accurate on my machine
                    self.progstate = 1 # if event.state == 1 else 2
                elif event.type == pygame.KEYDOWN:
                    self.keystates[pygame_key_to_vk_code(event.key, 0)] |= 1
                elif event.type == pygame.KEYUP:
                    self.keystates[pygame_key_to_vk_code(event.key, 0)] |= 2
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    self.keystates[1 if event.button==1 else 2] |= 1
                elif event.type == pygame.MOUSEBUTTONUP:
                    self.keystates[1 if event.button==1 else 2] |= 2
            for i, keystate in enumerate(self.keystates):
                self.keystates[i] |=  ((keystate<<2)&4)
                self.keystates[i] &= ~((keystate<<1)&4)
                self.keystates[0] |= self.keystates[i]
            mouse_x, mouse_y = pygame.mouse.get_pos()
            mouse_x = (mouse_x * self.g.back_buffer.get_width() //
                       self.g.screen.get_width())
            mouse_y = (mouse_y * self.g.back_buffer.get_height() //
                       self.g.screen.get_height())
            self.keystates[0x3A] = mouse_x>>8&0xFF
            self.keystates[0x3B] = mouse_x   &0xFF
            self.keystates[0x3C] = mouse_y>>8&0xFF
            self.keystates[0x3D] = mouse_y   &0xFF
            vm.push(PinkyBlob(data=self.keystates))

        def img(args):
            vm.push(PygameImage(base64.b64decode(args[0])))
            
        def subimgex(args):
            vm.push(args[0].sub_image(args[1]>>16, args[2]>>16,
                                      args[3]>>16, args[4]>>16,
                                      args[5],
                                      args[6]))
            
        def imgw(args):
            vm.push_int32(args[0].get_width ()<<16)
            
        def imgh(args):
            vm.push_int32(args[0].get_height()<<16)
            
        def drawex(args):
            if self.g_disabled:
                return
            args[0].draw(self.g,
                         args[1]-self.g_tx>>16,
                         args[2]-self.g_ty>>16,
                         args[3])
            
        def drawsubex(args):
            if self.g_disabled:
                return
            args[0].draw_sub_region(self.g,
                                    args[1]-self.g_tx>>16,
                                    args[2]-self.g_ty>>16,
                                    args[3]>>16,
                                    args[4]>>16,
                                    args[5]>>16,
                                    args[6]>>16,
                                    args[7])
            
        def drawstretchedex(args):
            if self.g_disabled:
                return
            args[0].draw_stretched(self.g,
                                   args[1]-self.g_tx>>16,
                                   args[2]-self.g_ty>>16,
                                   args[3]>>16,
                                   args[4]>>16,
                                   args[5])            
            
        def drawmap(args):
            if self.g_disabled:
                return
            tw = args[4]>>16
            th = args[5]>>16
            PygameImage.draw_map(
                self.g,                
                args[0],
                args[1].data,
                args[2], args[3],
                self.g_tx//args[4],
                self.g_ty//args[5],
                self.g.virtual_w//tw+1,
                self.g.virtual_h//th+1,
                tw,
                th,
                args[6]-self.g_tx>>16,
                args[7]-self.g_ty>>16)
            
        def drawtext(args):
            if self.g_disabled:
                return
            PygameImage.draw_text(
                self.g,
                args[0],
                args[1],
                args[2],
                args[3]-self.g_tx>>16,
                args[4]-self.g_ty>>16,
                args[5]>>16,
                args[6]>>16,
                args[7])

        def rempty(args):
            vm.push_bool(rect_empty(args[0]))
            
        def rinter(args):
            vm.push(rect_intersection(args[0], args[1]))
            
        def runion(args):
            vm.push(rect_union(args[0], args[1]))
            
        def rhitpoint(args):
            vm.push_bool(
                rect_hit_point(args[0], args[1], args[2]))
            
        def rhitsegment(args):
            vm.push_int32(
                rect_hit_segment(args[0], args[1], args[2], args[3], args[4]))
            
        def resolvehit(args):
            vm.push_int32(rect_hit_rect(*args))
            
        def resolvemaphit(args):
            vm.push_int32(rect_hit_map(*args))
            
        def wav(args):
            vm.push(PygameSound(base64.b64decode(args[0])))
            
        def playex(args):
            args[0].play(gain=args[1]//65536.0, loop=(args[2]!=0))
            
        def stop(args):
            args[0].stop()
        
        # Game functions
        
        def setgameparams(args):
            self.world_w = args[0]
            self.world_h = args[1]
            self.cam.set_bounds(
                0, 0,
                self.world_w-(self.g.virtual_w<<16),
                self.world_h-(self.g.virtual_h<<16))
            
        def camsetpath(args):
            self.cam.set_path(args[0])
            
        def camadvance(args):
            self.cam.advance()
            
        def cam(args):
            vm.push(list(self.cam.__dict__.values()))
            
        def drawsprite(args):
            if self.g_disabled:
                return
            spr = Sprite(args[0])
            spr.draw(self.g, self.g_tx, self.g_ty)
            
        def drawtilemap(args):
            if self.g_disabled:
                return
            tilemap = Tilemap(args[0])
            tilemap.draw(
                self.g,
                self.g_tx,
                self.g_ty,
                self.world_w,
                self.world_h,
                self.g.virtual_w<<16,
                self.g.virtual_h<<16)
            
        def drawtileobj(args):
            if self.g_disabled:
                return
            tileobj = args[0]
            tileobj[0][1][tileobj[1]].draw_stretched(
                self.g,
                tileobj[2]-self.g_tx>>16,
                tileobj[3]-self.g_ty>>16,
                tileobj[4]>>16,
                tileobj[5]>>16,
                tileobj[6])
            
        def settilesetframe(args):
            tileset = Tileset(args[0])
            tileset.set_frame(args[1])
            
        def advancesprite(args):
            spr = Sprite(args[0])
            keys = list(spr.__dict__.keys())            

            # Advance anim
            # finterval = 10 << 16
            # frames    = spr.anim[0]
            # fi        = spr.fi
            # src       = frames[fi//finterval]
            # fi        = fi + spr.fstep
            # fn        = len(frames) * finterval
            # if fi >= fn:
            #     fi = spr.anim[1] * finterval + (fi-fn)
            # args[0][keys.index('src')] = src
            # args[0][keys.index('fi')]  = fi

            # Advance sprites
            other_sprites = map(Sprite, args[1])
            dx = spr.fx * (min(spr.vx+spr.ax, spr.vxt) if spr.ax >= 0 else max(spr.vx+spr.ax, 0))
            dy = spr.fy * (min(spr.vy+spr.ay, spr.vyt) if spr.ay >= 0 else max(spr.vy+spr.ay, 0))
            tilemap = Tilemap(args[2])
            spr.old_bnds[0] = spr.bnds[0]
            spr.old_bnds[1] = spr.bnds[1]
            spr.old_bnds[2] = spr.bnds[2]
            spr.old_bnds[3] = spr.bnds[3]            
            spr.bnds[0] += dx
            spr.bnds[1] += dy
            spr.bnds[2] += dx
            spr.bnds[3] += dy

            hits = 0

            # Skip checking for hits (but still zero out .hits fields)
            # if hit checking is disabled or sprite didn't move
            hids_to_check = spr.hids_on & ~spr.hids_off
            skip_check = hids_to_check == 0

            if not skip_check:
                hflags = 0xF0000000 | spr.weight | hids_to_check | 0xF
                hits = rect_hit_map(
                    spr.old_bnds,
                    spr.bnds,
                    tilemap.data,
                    tilemap.tileset.flags,
                    tilemap.w,
                    tilemap.h,
                    tilemap.tw,
                    tilemap.th,
                    hflags)

            for other in other_sprites:
                # Other sprite must have reciprocal interest in sprite, in order to check
                if skip_check or (spr.hid&(other.hids_on&~other.hids_off)) == 0:
                    other_hits = 0
                else:
                    other_hits = rect_hit_rect(
                        spr.old_bnds,
                        spr.bnds,
                        other.bnds,
                        hflags,
                        other.hid|other.sides|other.weight)
                other.set_source_value('hits', other_hits)
                hits |= other_hits

            args[0][keys.index('hits')] = hits
            args[0][keys.index('vx')]   = (spr.bnds[0]-spr.old_bnds[0]) * spr.fx
            args[0][keys.index('vy')]   = (spr.bnds[1]-spr.old_bnds[1]) * spr.fy

        return [
            ginit,
            gbegin,
            gend,
            genable,
            clear,
            setviewport,
            progstate,
            pollkeys,
            img,
            subimgex,
            imgw,
            imgh,
            drawex,
            drawsubex,
            drawstretchedex,            
            drawmap,
            drawtext,
            rempty,
            rinter,
            runion,
            rhitpoint,
            rhitsegment,
            resolvehit,
            resolvemaphit,
            wav,
            playex,
            stop,
            setgameparams,
            camsetpath,
            camadvance,
            cam,
            drawsprite,
            drawtilemap,
            drawtileobj,
            settilesetframe,
            advancesprite
        ]
    
#
# GRAPHICS ROUTINES / DELEGATE
#

def rect_empty(a):
    return a[0] >= a[2] or a[1] >= a[3]

def rect_intersects(a, b):
    return a[0] < b[2] and a[2] > b[0] and a[1] < b[3] and a[3] > b[1]

def rect_intersection(a, b):
    return [max(a[0], b[0]), max(a[1], b[1]), min(a[2], b[2]), min(a[3], b[3])]

def rect_union(a, b):
    return [min(a[0], b[0]), min(a[1], b[1]), max(a[2], b[2]), max(a[3], b[3])]

def rect_hit_point(a, x, y):
    return a[0] <= x and a[2] > x and a[1] <= y and a[3] > y

def rect_hit_segment(a, x1, y1, x2, y2):
    ax1, ay1, ax2, ay2 = a
    
    if ((x1 <  ax1 and x2 <  ax1) or
        (y1 <  ay1 and y2 <  ay1) or
        (x1 >= ax2 and x2 >= ax2) or
        (y1 >= ay2 and y2 >= ay2)):
        return False

    dx = x2 - x1
    dy = y2 - y1

    if dx:
        m = dy / dx
        y = y1 + m * (ax1 - x1)
        if y >= ay1 and y < ay2: return True
        y = y1 + m * (ax2 - x1)
        if y >= ay1 and y < ay2: return True

    if dy:
        m = dx / dy
        x = x1 + m * (ay1 - y1)
        if x >= ax1 and x < ax2: return True    
        x = x1 + m * (ay2 - y1)
        if x >= ax2 and x < ax2: return True
    
    return False

def rect_hit_rect(a1, a2, b, a_flags, b_flags):
    a_types = a_flags & 0xFFFF0
    b_types = b_flags & 0xFFFF0
    c_types = a_types & b_types
    if not c_types:
        return 0
    
    inter = rect_intersection(a2, b)
    if rect_empty(inter):
        # TODO: resolve side flags for unhit
        if rect_empty(rect_intersection(a1, b)):
            return 0
        else:
            return (0x40000000&a_flags) | c_types
        
    b_side_flags = ((b_flags>>2)&0x3) | ((b_flags<<2)&0xC)
    a_mass = a_flags & 0xF00000
    b_mass = b_flags & 0xF00000
    c_mass = a_mass and b_mass
    prev_separation_flags = rect_separation_flags(a1, b)
    touch_flag = 0 if prev_separation_flags else (0x80000000&a_flags)
    c_side_flags = prev_separation_flags & a_flags & b_side_flags

    # Displace based on intersection
    if c_side_flags and c_mass:        
        if   a_mass == 0xF00000:
            s = 0.0
        elif b_mass == 0xF00000:
            s = 1.0
        else:
            s = clamp(0.5+((b_mass-a_mass)>>12)//7.0, 0.0, 1.0)

        if c_side_flags & ~0xA:
            dx     = a2[0] - a1[0]
            nx     = dx // abs(dx)
            dx1    = inter[2] - inter[0]
            dx2    = int(dx1*s)
            a_dx   = nx * (dx2)
            b_dx   = nx * (dx1-dx2)
            a2[0] -= a_dx
            a2[2] -= a_dx
            b [0] += b_dx
            b [2] += b_dx
        else:
            dy     = a2[1] - a1[1]
            ny     = dy // abs(dy)
            dy1    = inter[3] - inter[1]
            dy2    = int(dy1*s)
            a_dy   = ny * (dy2)
            b_dy   = ny * (dy1-dy2)
            a2[1] -= a_dy
            a2[3] -= a_dy
            b [1] += b_dy
            b [3] += b_dy

    return touch_flag | c_mass | c_types | c_side_flags

def rect_hit_map(a1, a2, map_, ts_flags, map_w, map_h, tw, th, a_flags):
    
    #if not a_flags&0xF00000:
    #    return 0  # tiles are all max weight so exit immediately if player is 0
    
    map_region = (
        (a2[0])      // tw,
        (a2[1])      // th,
        (a2[2]+tw-1) // tw,
        (a2[3]+th-1) // th,
    )

    map_region = rect_intersection(map_region, (0, 0, map_w, map_h))

    xy_inter = x_inter = y_inter = opt_x_flags = opt_y_flags = c_flags = 0

    a_types = (a_flags>>4) & 0xFF

    for ty in range(map_region[1], map_region[3]):
        for tx in range(map_region[0], map_region[2]):
            tile_index = map_.data[tx+ty*map_w]
            if not tile_index:
                continue
            b_flags = ts_flags.data[tile_index-1]
            b_type = 1 << (b_flags>>4) >> 1  # convert type to flag
            c_type_flag  = (a_types&b_type) << 4
            if not c_type_flag:
                continue
            bx1, by1 = tx*tw, ty*th
            b = (bx1, by1, bx1+tw, by1+th)
            inter = rect_intersection(a2, b)
            if rect_empty(inter):
                continue
            b_side_flags = ((b_flags>>2)&0x3) | ((b_flags<<2)&0xC)
            c_side_flags = rect_separation_flags(a1, b) & a_flags
            if c_side_flags & b_side_flags:
                c_side_flags &= b_side_flags          
                if   c_side_flags == (c_side_flags&0x5):
                    x_inter  = max(x_inter,  inter[2]-inter[0])
                elif c_side_flags == (c_side_flags&0xA):
                    y_inter  = max(y_inter,  inter[3]-inter[1])
                else:
                    xy_inter = max(xy_inter, inter[3]-inter[1])
                c_flags |= c_side_flags | c_type_flag | 0xF00000
            else:
                if   c_side_flags == (c_side_flags&0x5):
                    opt_x_flags |= c_type_flag
                elif c_side_flags == (c_side_flags&0xA):
                    opt_y_flags |= c_type_flag

    # If any collision flags have been set,
    # we need to displace the original dx/dy by the intersection
    if x_inter:
        dx     = -x_inter if a2[0] < a1[0] else x_inter
        a2[0] -= dx
        a2[2] -= dx
    else:
        c_flags = (c_flags&~0xF) | (c_flags&0xA) | opt_x_flags
        if not y_inter:
            # Handle lone diagonal block hit as vertical hit
            y_inter = xy_inter

    if y_inter:
        dy     = -y_inter if a2[1] < a1[1] else y_inter
        a2[1] -= dy
        a2[3] -= dy
    else:
        c_flags = (c_flags&~0xF) | (c_flags&0x5) | opt_y_flags

    return c_flags

def rect_separation_flags(a, b):
    ax1, ay1, ax2, ay2 = a
    bx1, by1, bx2, by2 = b
    return (
        int(ay1>=by2)<<3|  # top
        int(ax2<=bx1)<<2|  # right
        int(ay2<=by1)<<1|  # bottom        
        int(ax1>=bx2))     # left

try:
    import pygame
    import io
except ImportError as e:
    print("WARNING: {!s}".format(e))
    PygameGraphics = PygameImage = PygameSound = None
else:
    _pygame_key_to_vk_code = {
        pygame.K_BACKSPACE: 0x08,
        pygame.K_TAB: 0x09,
        pygame.K_RETURN: 0x0D,
        pygame.K_LSHIFT: 0x10,
        pygame.K_RSHIFT: 0x10,
        pygame.K_LCTRL: 0x11,                
        pygame.K_RCTRL: 0x11,        
        pygame.K_RALT: 0x12,
        pygame.K_LALT: 0x12,
        pygame.K_ESCAPE: 0x1B,
        pygame.K_SPACE: 0x20,
        pygame.K_DELETE: 0x2E,
        pygame.K_LEFT: 0x25,        
        pygame.K_UP: 0x26,
        pygame.K_RIGHT: 0x27,        
        pygame.K_DOWN: 0x28,
    }
    
    def pygame_key_to_vk_code(k, default=None):
        if   pygame.K_a  <= k <= pygame.K_z:
            return 0x41 + k - pygame.K_a
        elif pygame.K_0  <= k <= pygame.K_9:
            return k
        elif pygame.K_F1 <= k <= pygame.K_F15:
            return 0x70 + k - pygame.K_F1
        else:
            # 0x0E-0F, 0x3A-40 : mouse x/y
            # 0x01-02 : mouse buttons,
            # 0x88-8F : 4 controllers?
            return _pygame_key_to_vk_code.get(k, default)
        
    class PygameGraphics(PinkyObject):
        def __init__(self, w, h, virtual_w, virtual_h, options={}):
            self.screen = pygame.display.set_mode((w, h), pygame.HWSURFACE)
            self.back_buffer = self.screen.copy()
            self.virtual_w = virtual_w or w
            self.virtual_h = virtual_h or h
            self.options = options
            
        def _fit_dimensions_keep_aspect_ratio(self, src, dst):
            sw, sh = src
            dw, dh = dst
            ri = sw / sh
            rs = dw / dh
            if rs > ri:
                w, h = (sw*dh//sh, dh)
            else:
                w, h = (dw, sh*dw//sw)
            return ((dw-w)//2, (dh-h)//2, w, h)
        
        def flush(self):
            x, y, w, h = self._fit_dimensions_keep_aspect_ratio(
                (self.virtual_w, self.virtual_h), self.screen.get_size())
            scaled_back_buffer = pygame.transform.scale(
                self.back_buffer,
                (w, h))
            self.screen.blit(scaled_back_buffer, (x, y))
            pygame.display.flip()  # pygame.display.update()
            
    class PygameImage(PinkyObject):
        def __init__(self, data=None):
            self.surf = data and pygame.image.load(io.BytesIO(data))
            self.stretched_surf = None
            self._hit_mask = None

        def _transformed_surf(self, flags):
            surf = self.surf
            
            flip = flags & 0x3
            if flip:
                surf = pygame.transform.flip(surf, flip&1, flip&2)

            rotate_90 = (flags & 0xC) >> 2
            if rotate_90:
                surf = pygame.transform.rotate(surf, rotate_90*-90)

            return surf
            
        def draw(self, g, x, y, flags):
            if (flags&0xC0) == 0xC0:
                return
            
            g.back_buffer.blit(self._transformed_surf(flags), (x, y))

        def draw_sub_region(self, g, x, y, sx, sy, sw, sh, flags):
            if (flags&0xC0) == 0xC0:
                return

            g.back_buffer.blit(self._transformed_surf(flags), (x, y), (sx, sy, sw, sh))
            
        def draw_stretched(self, g, x, y, w, h, flags):
            if (flags&0xC0) == 0xC0:
                return
            
            surf = self._transformed_surf(flags)

            if self.stretched_surf and (
                    self.stretched_surf.get_width()  == w or
                    self.stretched_surf.get_height() == h):
                pygame.transform.scale(surf, (w, h), self.stretched_surf)
            else:
                self.stretched_surf = pygame.transform.scale(surf, (w, h))
            
            g.back_buffer.blit(self.stretched_surf, (x, y))
            
        @staticmethod
        def draw_map(g, images, data, map_w, map_h,
                     off_x, off_y, w, h, tw, th, x, y):
            y1 = off_y
            y2 = y1 + h
            while y1 < y2:
                x1 = off_x
                x2 = x1 + w
                while x1 < x2:
                    i = data[(x1%map_w)+(y1%map_h)*map_w]
                    if i != 0:
                        images[i-1].draw(g, x+x1*tw, y+y1*th, 0x00)
                    x1 += 1
                y1 += 1
                
        @staticmethod
        def draw_text(g, images, strides, text, xi, y, offset, line_h, sub_chars):
            i = 0
            j = 0
            n = len(text)
            x = xi
            while i < n:
                c = ord(text[i])
                i += 1
                if   c == ord('\n'):
                    x = xi
                    y = y + line_h
                    continue
                elif c == ord('\t'):
                    pass
                elif c == ord('$'):
                    c = sub_chars[j]
                    images[c].draw(g, x, y, 0x00)
                    j += 1
                elif c != ord(' '):
                    images[c].draw(g, x, y, 0x00)
                x += offset + (strides[c]>>16)
                    
        def sub_image(self, x, y, w, h, flags=0, color=0):
            image = PygameImage()
            image.surf = self.surf.subsurface(pygame.Rect(x, y, w, h))
            
            flip = flags & 0xF
            blend_mode = flags >> 4 & 0xF
            
            if flip:
                image.surf = pygame.transform.flip(image.surf, flip&1, flip&2)
            
            if blend_mode:
                blend_modes = (pygame.BLEND_RGBA_ADD,
                               pygame.BLEND_RGBA_SUB,
                               pygame.BLEND_RGBA_MULT)
                alpha_img = pygame.Surface(self.surf.get_size(),
                                           pygame.SRCALPHA)
                alpha_img.fill((color>>16&0xFF,
                                color >>8&0xFF,
                                color    &0xFF,
                                color>>24&0xFF))
                image.surf = image.surf.copy()
                image.surf.blit(
                    alpha_img,
                    (0, 0),
                    special_flags=blend_modes[blend_mode-1])
            
            return image
        
        def get_width(self):
            return self.surf.get_width()
        
        def get_height(self):
            return self.surf.get_height()

        def get_mask(self, alpha_thresh=0):
            blocks_per_line = (self.surf.get_width()+7) // 8
            mask = [0] * (blocks_per_line*self.surf.get_height())
            self.surf.lock()
            for y in range(self.surf.get_height()):
                for x in range(self.surf.get_width()):
                    bit_value = int(self.surf.get_at((x, y)).a>alpha_thresh)
                    mask[(x//8)+y*blocks_per_line] |= bit_value << (7-(x%8))
            self.surf.unlock()
            return mask

        @property
        def hit_mask(self):
            if self._hit_mask is None:
                self._hit_mask = self.get_mask()
            return self._hit_mask

    class PygameSound(PinkyObject):
        def __init__(self, data=None):
            self.src = pygame.mixer.Sound(data)

        def play(self, gain=1.0, loop=False):
            self.src.set_volume(gain)
            self.src.play(-1 if loop else 0)
            
        def stop(self):
            self.src.stop()
        
#
# GAME ROUTINES / DELEGATE SUPPORT
#

class StructWrapper:
    def __init__(self, fields, args):
        self.__dict__ = D(zip(fields, args))
        self._source = args

    def set_source_value(self, key, value):
        self._source[list(self.__dict__.keys()).index(key)] = value

class Sprite(StructWrapper):
    def __init__(self, args):
        StructWrapper.__init__(
            self,
            [
                'src',
                'bnds',
                'old_bnds',
                'inset',
                'rotation',                
                'flip_x',
                'flip_y',
                'hide',
                'vx',
                'vy',
                'ax',
                'ay',
                'vxt',
                'vyt',
                'fx',
                'fy',
                'hid',
                'hids_on',
                'hids_off',
                'sides',
                'weight',
                'hits',
            ], args)
        
    def draw(self, g, x, y):
        if self.hide:
            return

        w     = self.src.get_width () << 16
        h     = self.src.get_height() << 16
        rot   = self.rotation
        inset = self.inset  # self.anim[4]
        if   rot == 1:
            ox = h - inset[3]
            oy = inset[0]
        elif rot == 2:
            ox = w - inset[2]
            oy = h - inset[3]
        elif rot == 3:
            ox = inset[1]
            oy = w - inset[2]
        else:
            ox = w - inset[2] if self.flip_x else inset[0]
            oy = h - inset[3] if self.flip_y else inset[1]

        gflags = self.rotation << 2
        if self.flip_x:
            gflags |= 1
        if self.flip_y:
            gflags |= 2

        # Clip sprites that aren't on the screen
        # if not rect_intersects(self.bnds, (x, y, x+g.virtual_w<<16, y+g.virtual_h<<16)):
        #     return
            
        self.src.draw(g, self.bnds[0]-ox-x>>16, self.bnds[1]-oy-y>>16, gflags)

        # Draw hitbox if enabled
        if g.options.get('draw_sprite_bounds'):
            pygame.draw.rect(
                g.back_buffer,
                (255, 0, 0),
                (
                    self.bnds[0]-x>>16,
                    self.bnds[1]-y>>16,
                    self.bnds[2]-self.bnds[0]>>16,
                    self.bnds[3]-self.bnds[1]>>16
                ),
                1)        
        
class Tileset(StructWrapper):
    def __init__(self, args):
        StructWrapper.__init__(
            self,
            [
                'src',
                'tiles',
                'anims',
                'flags',
                'tw',
                'th',
                'spacing',
                'margin',
            ], args)

    def set_frame(self, t):
        for anim in self.anims:
            self.tiles[anim[0]] = self.tiles[anim[1][t%len(anim[1])]]

class Tilemap(StructWrapper):
    def __init__(self, args):
        StructWrapper.__init__(
            self,
            [
                'tileset',
                'data',
                'w',
                'h',
                'tw',
                'th',
                'x',
                'y',
                'pw',
                'ph',
            ], args)

        self.tileset = Tileset(self.tileset)

    def draw(self, g, x, y, world_w, world_h, screen_w, screen_h):
        if world_w <= screen_w:
            scroll_x = 0
        else:
            scroll_x = x * (self.pw-screen_w) // (world_w-screen_w)

        if world_h <= screen_h:
            scroll_y = 0
        else:
            scroll_y = y * (self.ph-screen_h) // (world_h-screen_h)

        scroll_x = clamp(scroll_x, 0, self.pw-screen_w)
        scroll_y = clamp(scroll_y, 0, self.ph-screen_h)

        PygameImage.draw_map(
            g,                
            self.tileset.tiles,
            self.data.data,
            self.w,
            self.h,
            scroll_x//self.tw,
            scroll_y//self.th,
            screen_w//self.tw+1,
            screen_h//self.th+1,
            self.tw>>16,
            self.th>>16,
            self.x-scroll_x>>16,
            self.y-scroll_y>>16)

class Cam(StructWrapper):
    def __init__(self, args):
        StructWrapper.__init__(
            self,
            [
                'x',
                'y',
                'nx',
                'ny',
                'd',
                'd_acc',
                'd_max',
                'i',
                'path',
                'x_min',
                'y_min',
                'x_max',
                'y_max',
            ],
            args)
    
    def set_bounds(self, x_min, y_min, x_max, y_max):
        self.x_min = x_min
        self.y_min = y_min
        self.x_max = x_max
        self.y_max = y_max

    def next_path(self):
        dest    = self.path[self.i]
        dx      = dest[0] - self.x
        dy      = dest[1] - self.y
        d_max   = max(abs(dx), abs(dy))
        
        if d_max:
            self.nx = fp_div(dx, d_max)
            self.ny = fp_div(dy, d_max)
            self.d  = fp_hypot(dx, dy)
        else:
            self.nx = 0
            self.ny = 0
            self.d  = 0
            
        self.i += 1            

    def advance(self):
        if self.d_acc == self.d_max:
            return

        def easeout(a, b, t):
            return a + int((b-a)*(1.0-((1.0-t)**2)))

        # smooth out speed over entire path
        d = easeout(
            self.path[self.i-1][2],
            1<<16,
            self.d_acc/self.d_max)
        
        if d <= self.d:
            self.x = clamp(self.x+fp_mul(self.nx, d), self.x_min, self.x_max)
            self.y = clamp(self.y+fp_mul(self.ny, d), self.y_min, self.y_max)
            self.d_acc += d
            self.d -= d
        else:
            self.x = clamp(
                self.x+fp_mul(self.nx, self.d),
                self.x_min,
                self.x_max)
            self.y = clamp(
                self.y+fp_mul(self.ny, self.d),
                self.y_min,
                self.y_max)
            
            self.d_acc += self.d
            
            if self.i == len(self.path):
                self.d = 0
            else:
                self.d = d - self.d
                self.next_path()
                self.advance()

    @staticmethod
    def calc_path_dist(path):
        if not path:
            return 0
        d = 0
        n = len(path) - 1        
        b = path[n]
        while n > 0:
            n = n - 1
            a = path[n]
            d = d + fp_hypot(b[0]-a[0], b[1]-a[1])
            b = a
        return d

    def set_path(self, path):
        self.path  = path
        self.x     = clamp(path[0][0], self.x_min, self.x_max)
        self.y     = clamp(path[0][1], self.y_min, self.y_max)
        self.i     = 1
        self.d_acc = 0
        self.d_max = Cam.calc_path_dist(path)
        self.next_path()

#
# MAIN ENTRY POINT
#

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Pinky v. {}'.format(VERSION))
    parser.add_argument('jamfile', type=str)
    args = parser.parse_args()
    try:
        PinkyVM.from_jamcode(open(args.jamfile, 'rt').read()).run()
    except KeyboardInterrupt:
        pass
