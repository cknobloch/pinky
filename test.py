
from pinkyc import Compiler, ScriptError

def compile(src, **options):
    return Compiler(libs=[], **options).compile_fragment(src).to_jamcode()

def assert_equal(a, b):
    assert a == b, '\n{!s}\n--DOES NOT EQUAL--\n{!s}'.format(a, b)

def clean_bytecode(code):
    return '\n'.join(l.lstrip() for l in code.split('\n') if l.lstrip())
    
def assert_compile(a, b):    
    assert_equal(compile(a), clean_bytecode(b))

def assert_compile_error(a, text=None, line_no=None):
    try:
        compile(a, debug=True)
    except ScriptError as e:
        if text is not None:
            assert_equal(str(e.msg), text)
        if line_no is not None:
            assert_equal(e.line_no+1, line_no)
    else:
        raise AssertionError('Error was not raised')

#
# Whitespace / Comments
#

assert_compile(
    """""",
    """jmp  1""")

assert_compile(
    """x : int\n x = 0\n""",
    """jmp  1\nlodi 0\nsetg 1""")

assert_compile(
    """  x  :    int     \n   x    =  0; """,
    """jmp  1\nlodi 0\nsetg 1""")

assert_compile(
    """     x  :    int     \n   x    =  0; """,
    """jmp  1\nlodi 0\nsetg 1""")

assert_compile(
    """x : int  # This is a test comment \n x = 0  ##### This is a another\n""",
    """jmp  1\nlodi 0\nsetg 1""")

#
# Literals
#

assert_compile(
    """x = 7;""",
    """jmp  1\nlodi 7\nsetg 1""")

assert_compile(
    """x = 0;""",
    """jmp  1\nlodi 0\nsetg 1""")

assert_compile(
    """x = 0x00;""",
    """jmp  1\nlodi 0\nsetg 1""")

assert_compile(
    """x = 0x10;""",
    """jmp  1\nlodi 16\nsetg 1""")

assert_compile(
    """x = "foo";""",
    """jmp  1\nlods foo\nsetg 1""")

assert_compile(
    """x = "";""",
    """jmp  1\nlods \nsetg 1""")

assert_compile(
    """x = " ";""",
    """jmp  1\nlods  \nsetg 1""")

assert_compile(
    """x = "\\"foo\\""; """,
    """jmp  1\nlods \\"foo\\"\nsetg 1""")

assert_compile(
    """x = "foo\\100\\nbar";""",
    """jmp  1\nlods foo\\100\\nbar\nsetg 1""")

#
# Operations
#

for op in ('+', '-', '*', '/', '<', '>', '<=', '>=', '==', '!='):
    assert_compile(
        """
        proc run()
          x = 1 {} 2
        end
        """.format(op),
        """
        jmp  8
        #    run(:void)
        lodi 1
        lodi 2
        {}
        setl 1
        ret 
        #    end proc
        lodf 1 2
        setg 1
        """.format(op.ljust(4)))

#
# Expressions
#

assert_compile(
    """x : int\n x = 0;""",
    """jmp  1\nlodi 0\nsetg 1""")

assert_compile(
    """x : *\n x = null;""",
    """jmp  1\nnull\nsetg 1""")

assert_compile_error(
    """x : int\n x = "foo"\n""",
    """Mismatched types str and int""",
    line_no=2)

assert_compile_error(
    """x $ int\n x ~= null\n""",
    """Syntax error""",
    line_no=1)

assert_compile_error(
    """x : int\n x ~= null""",
    """Syntax error""",
    line_no=2)

assert_compile_error(
    """x : int\n x = 0\n\n x = 1\n x = 3\n\nx ~= null""",
    """Syntax error""",
    line_no=7)

assert_compile(
    """
    x = 1
    """,
    """
    jmp  1
    lodi 1
    setg 1""")

assert_compile(
    """
    x = 1 + 2
    """,
    """
    jmp  1
    lodi 1
    lodi 2
    +   
    setg 1""")

assert_compile(
    """
    x = -(1+2)
    """,
    """
    jmp  1
    lodi 1
    lodi 2
    +   
    neg 
    setg 1""")

assert_compile(
    """
    x : bool
    x = ! ((1+2) == 0)
    """,
    """
    jmp  1
    lodi 1
    lodi 2
    +   
    lodi 0
    ==  
    not 
    setg 1""")

assert_compile(
    """
    x = 1 + 2 * (3 / 4) - 5
    """,
    """
    jmp  1
    lodi 1
    lodi 2
    lodi 3
    lodi 4
    /   
    *   
    +   
    lodi 5
    -   
    setg 1""")

assert_compile(
    """
    x = false | false & (false | true) | false
    """,
    """
    jmp  1
    zero
    dup 
    jif  11
    drop
    zero
    dup 
    jz   7
    drop
    zero
    dup 
    jif  3
    drop
    one 
    dup 
    jif  3
    drop
    zero
    setg 1""")

#
# Procedures
#

assert_compile(
    """
    proc foo(a : int, b : int)
    end
    """,
    """
    jmp  4
    #    foo(int,int:void)
    ret 
    #    end proc
    lodf 1 3
    setg 1""")

assert_compile_error(
    """
    proc foo(a : int, b : int) : bool
    end
    """,
    """Unexpected return value""",
    line_no=2)

assert_compile(
    """
    proc foo() : int
      x : proc(:*)
      return 1
    end
    """,
    """
    jmp  5
    #    foo(:int)
    lodi 1
    ret 
    #    end proc
    lodf 1 2
    setg 1""")

assert_compile(
    """
    y = 0
    proc foo() : int
      return y
    end
    """,
    """
    jmp  5
    #    foo(:int)
    getg 1
    ret 
    #    end proc
    lodi 0
    setg 1
    lodf 1 1
    setg 2""")

assert_compile(
    """
    bar : proc(int:int)
    proc foo(a : int, b : int) : int
      return a + b
    end
    proc bar(c : int) : int
      return c
    end
    """,
    """
    jmp  11
    #    foo(int,int:int)
    getl 1
    getl 2
    +   
    ret 
    #    end proc
    #    bar(int:int)
    getl 1
    ret 
    #    end proc
    lodf 1 3
    setg 2
    lodf 7 2
    setg 1""")

assert_compile(
    """
    proc foo(a : int, b : int) : int
      c : int
      c = a * b + 1000
      return c
    end
    """,
    """
    jmp  11
    #    foo(int,int:int)
    getl 1
    getl 2
    *   
    lodi 1000
    +   
    setl 3
    getl 3
    ret 
    #    end proc
    lodf 1 4
    setg 1""")

assert_compile(
    """
    proc foo(a:int) : int
      return foo(a)
    end
    """,
    """
    jmp  7
    #    foo(int:int)
    getg 1
    getl 1
    jsr  1
    ret 
    #    end proc
    lodf 1 2
    setg 1""")

assert_compile(
    """
    proc foo(a : int, b : int) : int
      c : int
      c = a * b + 1000
      return c
    end
    proc run()
      c : int
      c = foo(1, 2)
    end
    """,
    """
    jmp  19
    #    foo(int,int:int)
    getl 1
    getl 2
    *   
    lodi 1000
    +   
    setl 3
    getl 3
    ret 
    #    end proc
    #    run(:void)
    getg 1
    lodi 1
    lodi 2
    jsr  2
    setl 1
    ret 
    #    end proc
    lodf 1 4
    setg 1
    lodf 11 2
    setg 2""")

#
# Units
#

assert_compile(
    """unit in : 3 / 2; x = 2in;""",
    """jmp  1\nlodi 3\nsetg 1""")

assert_compile(
    """unit in : 1 / 2; x = 30in;""",
    """jmp  1\nlodi 15\nsetg 1""")

assert_compile(
    """unit in : 1 / 1; unit ft : 12in / 1; x = 2ft;""",
    """jmp  1\nlodi 24\nsetg 1""")

assert_compile(
    """unit in : 1.0 / 2.0; x = 30.0in;""",
    """jmp  1\nlodi 983040\nsetg 1""")

assert_compile(
    """unit in : 1.0 / 1.0; unit ft : 12.0in / 1.0; x = 2.5ft;""",
    """jmp  1\nlodi 1966080\nsetg 1""")

assert_compile(
    """unit ft : 1.0 / 1.0; unit in : 1.0ft / 12.0; x = 30.0in;""",
    """jmp  1\nlodi 163840\nsetg 1""")

#
# Structs
#

assert_compile(
    """struct Foo\nx : int\ny : int\nend\n""",
    """jmp  1""")

assert_compile(
    """
    struct Foo
      x : int
      y : int
    end
    foo : Foo
    foo = new Foo
    """,
    """jmp  1\nnew  2\nsetg 1""")

assert_compile(
    """
    struct Bar; end
    struct Foo
      b : Bar
    end
    struct Bar
      x : int
      y : int
    end
    foo : Foo
    foo = new Foo
    """,
    """jmp  1\nnew  1\nsetg 1""")

assert_compile(
    """
    struct Foo
      x : int
      y : int
    end
    foo : Foo[10]
    foo = new Foo[10]
    """,
    """jmp  1\nnew  10\nsetg 1""")

assert_compile(
    """
    struct Foo
      a : int
    end
    foo : Foo[10]
    foo = new Foo[15]
    """,
    """jmp  1\nnew  15\nsetg 1""")

assert_compile_error(
    """
    struct Foo
      a : int
    end
    foo : Foo[15]
    foo = new Foo[10]
    """,
    """Mismatched types Foo[10] and Foo[15]""",
    line_no=6)

assert_compile(
    """
    struct Foo
      x : int
      y : int
    end
    foo : Foo[]
    foo = new Foo[10]
    """,
    """jmp  1\nnew  10\nsetg 1""")

assert_compile(
    """
    struct Foo
      x : int
      y : int
    end
    foo : Foo
    foo = {1, 2}
    """,
    """
    jmp  1
    lodi 1
    lodi 2
    newv 2
    setg 1
    """)

assert_compile(
    """
    struct Foo
      x : int[2]
      y : int
    end
    foo : Foo
    foo = {{0, 0}, 1}
    """,
    """
    jmp  1
    lodi 0
    lodi 0
    newv 2
    lodi 1
    newv 2
    setg 1
    """)

assert_compile_error(
    """
    struct Foo
      x : int
      y : int
    end
    struct Bar
      a : int
      b : int
    end
    proc main()     
      foo : Bar
      foo = new Foo as Bar
      foo.x = 1
      foo.y = 2
    end
    """,
    """Cannot cast Foo to Bar""",
    line_no=12)

assert_compile(
    """
    struct Foo
      x : int
      y : int
    end
    struct Bar
      f : Foo
      x : int
      y : int
      z : int
      w : *
    end
    proc main()
      bar : Bar
      bar     = new Bar
      bar.x   = 1
      bar.y   = 2
      bar.z   = 3
      bar.w   = bar
      bar.f   = new Foo
      bar.f.x = 1
      bar.f.y = 2
    end
    """,
    """
    jmp  29
    #    main(:void)
    new  5
    setl 1
    getl 1
    lodi 1
    seta 1
    getl 1
    lodi 2
    seta 2
    getl 1
    lodi 3
    seta 3
    getl 1
    getl 1
    seta 4
    getl 1
    new  2
    seta 0
    getl 1
    geta 0
    lodi 1
    seta 0
    getl 1
    geta 0
    lodi 2
    seta 1
    ret 
    #    end proc
    lodf 1 2
    setg 1
    """)

assert_compile(
    """
    struct Foo
      x : int
      y : int
    end
    foo : Foo
    foo = new Foo
    proc main()
      y = `Foo.y`
      x = `Foo.x`
    end
    """,
    """
    jmp  8
    #    main(:void)
    lodi 1
    setl 1
    lodi 0
    setl 2
    ret 
    #    end proc
    new  2
    setg 1
    lodf 1 3
    setg 2""")

#
# Conditionals
#

assert_compile(
    """
    proc main()
      if true then
        x = 1
      else
        x = 2       
      end
    end
    """,
    """
    jmp  11
    #    main(:void)
    one 
    jz   7
    lodi 1
    setl 1
    jmp  9
    lodi 2
    setl 1
    ret 
    #    end proc
    lodf 1 2
    setg 1
    """)

assert_compile(
    """
    proc main()
      a = 3
      if   a == 1 then
        b = 1
      elif a == 2 then
        b = 2+2      
      elif a == 3 then
        b = 3+3+3
      else
        b = 4     
      end
    end
    """,
    """
    jmp  35
    #    main(:void)
    lodi 3
    setl 1
    getl 1
    lodi 1
    ==  
    jz   11
    lodi 1
    setl 2
    jmp  33
    getl 1
    lodi 2
    ==  
    jz   20
    lodi 2
    lodi 2
    +   
    setl 2
    jmp  33
    getl 1
    lodi 3
    ==  
    jz   31
    lodi 3
    lodi 3
    +   
    lodi 3
    +   
    setl 2
    jmp  33
    lodi 4
    setl 2
    ret 
    #    end proc
    lodf 1 3
    setg 1
    """)

assert_compile(
    """
    proc main()
      a = 3
      while a - 1 > 0 do
        a = a - 1
      end
    end
    """,
    """
    jmp  17
    #    main(:void)
    lodi 3
    setl 1
    getl 1
    lodi 1
    -   
    lodi 0
    >   
    jz   15
    getl 1
    lodi 1
    -   
    setl 1
    jmp  4
    ret 
    #    end proc
    lodf 1 2
    setg 1
    """)

assert_compile(
    """
    proc main()
      a = 3
      while a - 1 > 0 do
        a = a - 1
      end
    end
    """,
    """
    jmp  17
    #    main(:void)
    lodi 3
    setl 1
    getl 1
    lodi 1
    -   
    lodi 0
    >   
    jz   15
    getl 1
    lodi 1
    -   
    setl 1
    jmp  4
    ret 
    #    end proc
    lodf 1 2
    setg 1
    """)

print("All tests passed!")
