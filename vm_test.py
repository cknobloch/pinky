
# Last run duration (on second run): 12.104494s

from pinkyc import run, ScriptError

def run_expect_error(src, error_cls):
    try:
        run(src, debug=True)
    except Exception as e:
        if not isinstance(e, error_cls):
            raise
    else:
        raise AssertionError('Expected error did not occur')
        
run("""
    proc run()
      assert(true & true)
    end
    """)

run("""
    proc run()
      print("hello world")
    end
    """)

run("""
    proc run()
      assert(true)
    end
    """)

run_expect_error("""
    proc run()
      assert(false)
    end
    """,
    AssertionError)

run("""
    proc run()
      assert(1==1)
    end
    """)

run("""
    proc run()
      assert(1+1==2)
    end
    """)

run("""
    proc run()
      assert(5-2==3)
    end
    """)

run("""
    proc run()
      assert(2*3==6)
    end
    """)

run("""
    proc run()
      assert(8/2==4)
    end
    """)

run("""
    proc run()
      assert(1+2*3==7)
    end
    """)

run("""
    proc run()
      assert(1+2*1+3==6)
    end
    """)

run("""
    proc run()
      assert(1+2*(1+3)==9)
    end
    """)

run("""
    proc run()
      assert(-(1+2)==-3)
    end
    """)

run("""
    proc run()
      assert(!false)
    end
    """)

run("""
    proc run()
      assert(min(7, 8) == 7)
    end
    """)

run("""
    proc run()
      assert(max(7, 8) == 8)
    end
    """)

run("""
    proc run()
      assert(shr(2, 1) == 1)
    end
    """)

run("""
    proc run()
      assert(shl(1, 1) == 2)
    end
    """)

run("""
    proc run()
      assert(and(1, 0) == 0)
    end
    """)

run("""
    proc run()
      assert(and(1, 1) == 1)
    end
    """)

run("""
    proc run()
      assert(or(0, 0) == 0)
    end
    """)

run("""
    proc run()
      assert(or(1, 0) == 1)
    end
    """)

run("""
    proc run()
      assert(xor(1, 1) == 0)
    end
    """)

run("""
    proc run()
      assert(xor(0, 1) == 1)
    end
    """)

run("""
    proc run()
      assert(not(0) == -1)
    end
    """)

run("""
    proc run()
      assert(mod(11, 8) == 3)
    end
    """)

run("""
    proc run()
      assert(abs(-123) == 123)
    end
    """)

run("""
    proc run()
      assert(abs(123) == 123)
    end
    """)

run("""
    proc run()
      assert(!(false|false))
    end
    """)

run("""
    proc run()
      assert(false|true)
    end
    """)

run("""
    proc run()
      assert(!(false&true))
    end
    """)

run("""
    proc run()
      assert(true&true)
    end
    """)

run("""
    proc run()
      assert(ftoi(123.0)==123)
    end
    """)

run("""
    proc run()
      assert(pow(7.0, 5.0) == 16807.0)
    end
    """)

run("""
    proc run()
      assert(sqrt(500.0) == 22.3606719971)
    end
    """)

run("""
    proc run()
      a : int
      b : fp
      c : fp
      a = 123
      b = fp(a, 0)
      b = fp(456, 0)
      c = 123.5
    end
    """)

run("""
    proc run()
      x = itos(ftoi(fp(456, 0)*5))
    end
    """)

run_expect_error("""
    proc run()
      x = 1.0 + 1
    end
    """,
    ScriptError)

#
# Str
#

run("""
    proc run()
      assert(slen("foo") == 3)
    end
    """)

run("""
    proc run()
      # FIXME -- add join() and uncomment
      # assert(scmp(concat("foo", "-", itos(99), "-", "bar"), "foo-99-bar")==0)
    end
    """)

run("""
    proc run()
      assert(scmp(chr(32), " ")==0)
    end
    """)

run("""
    proc run()
      assert(ord("foobar", 3)==98)
    end
    """)

run("""
    proc run()
      assert(scmp(itos(123), "123")==0)
    end
    """)

run("""
    proc run()
      assert(stoi("123")==123)
    end
    """)

#
# Array
#

run("""
    proc run()
      assert(alen(new int[7]) == 7)
    end
    """)

run("""
    proc run() 
      assert(alen(new int[7]) == 7)
    end
    """)

run("""
    proc run()
      a = new bool[3]
      a[0] = false
      a[1] = false
      a[2] = true
      assert(a[2])
    end
    """)

run_expect_error("""
    proc run()
      a = new bool[3]
      a[0] = false
      a[1] = false
      a[2] = true
      assert(a[3])
    end
    """,
    IndexError)

run("""
    proc run()
      x = {0, 1, 2, 3} as int[4]
      assert(alen(x) == 4)
      assert(x[0] == 0 & x[1] == 1 & x[2] == 2 & x[3] == 3)
    end
    """)

run("""
    proc run()
      n = 3
      x = {0, 0, 0, 0} as int[4]
      x[0] = 0
      x[1] = 1
      x[2] = 2
      x[3] = 3
      assert(alen(x) == 4)
      assert(x[0] == 0 & x[1] == 1 & x[2] == 2 & x[3] == 3)
    end
    """)

run("""
    proc run()
      x = new int[6]
      x[0] = 0
      x[1] = 1
      x[2] = 2
      x[3] = 3
      x[4] = 4
      x[4] = 6
      y = slice(x, 2, 2)
      assert(y[0] == 2 & y[1] == 3)
    end
    """)

run("""
    proc run()
      x = new int[6]
      x[0] = 0
      x[1] = 1
      x[2] = 2      
      x[3] = 3
      y = new int[2]
      y[0] = 5
      y[1] = 6
      copy(x, y, 4, 2)
      z = x
      assert(z[0] == 0 & z[1] == 1 & \
             z[2] == 2 & z[3] == 3 & \
             z[4] == 5 & z[5] == 6)
    end
    """)

run("""
    proc run()
      x = new int[4]
      x[0] = 1
      x[1] = 0
      x[2] = 3      
      x[3] = 2
      sort(x, icmp)
      assert(x[0] == 0 & x[1] == 1 & x[2] == 2 & x[3] == 3)
    end
    """)

#
# Conditional statements
#

run("""
    proc run()
      if 1 == 1 then
        assert(true)
      else
        assert(false)
      end
    end
    """)

run("""
    proc run()
      x : int
      if 1 == 0 then
        x = 1
      else
        x = 3
      end
      assert(x == 3)
    end
    """)

run("""
    proc run()
      x : int
      if 1 == 0 then
        x = 1
      elif 1 == 1 then
        x = 2
      end
      assert(x == 2)
    end
    """)

run("""
    proc run()
      x : int
      if 1 == 0 then
        x = 1
      elif 1 == 1 then
        x = 2
      else
        x = 3
      end
      assert(x == 2)
    end
    """)

#
# Logical operators
#

run("""
    proc run()
      assert(!(false|false))
    end
    """)

run("""
    proc run()
      assert(false|true)
    end
    """)

run("""
    proc run()
      assert(true|false)
    end
    """)

run("""
    proc run()
      assert(true|true)
    end
    """)

run("""
    proc run()
      assert(!(false&false))
    end
    """)

run("""
    proc run()
      assert(!(false&true))
    end
    """)

run("""
    proc run()
      assert(!(true&false))
    end
    """)

run("""
    proc run()
      assert(true|true)
    end
    """)

#
# Procs
#

run("""
    proc foo(x:int) : int
      if x != 0 then
        return x + foo(x-1)
      else
        return 0
      end
    end
    proc run()
      assert(foo(3)==6)
    end
    """)

run("""
    struct A
      v : proc(int:int)
    end
    proc doit(c:int) : int
      return c + 1
    end
    proc bar(b:int) : A
      return {doit} as A
    end
    proc eleven(a:int, b:int) : int
      return a*b
    end
    proc foo(a:int) : int
      i = eleven(1, 2)
      bar(0).v = doit
      return bar(7+6+5).v(eleven(eleven(9+8,a), 2))
    end
    proc run()
      assert(foo(4)==137)
    end
    """)

#
# Structs
#

run("""
    struct A
      x : int[10]
    end

    struct B
      a : A
    end

    struct C 
      b : B
      a : A
    end

    c : C

    proc run()
      c = new C
      c.b = new B
      c.b.a = new A
      c.b.a.x = new int[10]
      c.b.a.x[0] = 123
      assert(scmp("123", itos(c.b.a.x[0])) == 0)
      assert(true)
    end
    """)

#
# Stdlib
#

run("""
    proc run()
      seed(88)
      assert(rand(0, 10)==7)
      assert(rand(0, 10)==2)
      assert(rand(0, 10)==5)
    end
    """)

run("""
    proc run()
      seed(10)
      assert(rand(0, 10)==6)
      assert(rand(0, 10)==0)
      assert(rand(0, 10)==5)
    end
    """)

run("""
    a = @"vm_test.py"
    x = decodeb64(a)
    b = b64(x)
    proc run()
      assert(scmp(a, b)==0)
    end
    """)

#
#
#

test_script = """ 
# This is a test
xxx = 0
proc foo() : int
  xxx = 1
end
struct A
  x : int[10]
end

struct B
  a : A
end

struct C 
  b : B
  a : A
end

c : C

callback : bool

d = 1 == 1 & 2 == 2
d = 9 + 1*2 == 1  | 3 != 3 | 123  /5/4/2323 >= 3 &4 < 2
v = 123

proc test(foo:proc(int,int:bool)) : int
  if foo(1, 2) then
    return 1
  else 
    return 0
  end
end

proc hello_there(a:int, b:int) : bool
end

proc hello_there2(a:int, b:int[2]) : int
end

proc do_something(x:int, y:int, z:int)
  test_var : int
  test_array : bool[100]
  a : int
  b : int[2]
  b[0] = hello_there2(a, b)
  hello_there(2, 4)
  a = test(hello_there) # yessssss
  v = 0
end

struct Object
  w : *
  v : int
end

struct Foo : Object
  x : int
  y : *
  z : Object
  something_else : Object[2]
end

struct Foo2 : Foo
end

proc do_something_else() : bool
  foo : Foo
  obj : Object
  goo = (foo.x * 300 < 4) & (1 < 1234)
  foo.x = 100
  foo.y = (foo.x + 1) as *
  foo.x = foo.y as int
  obj = foo

  foo2 : Foo2
  obj = new Foo2
  x = foo.something_else[1]
  x = do_something(0007, 1, 2+234 * 1234 /2) as Object
  y = x.v + v
  z = 1 + v == 8
  zz = null
  if 1 == 0 then
    aa = true
  else
    aa = false
  end
  pp : bool
  pp = v as bool
  return pp
end
proc run()
  print(concat("foo", itos(v)))
end
"""

test_script = """ 
struct A
  x : int[10]
end

struct B
  a : A
end

struct C 
  b : B
  a : A
end

c : C

v = 777

proc run()
  print("...")
  c = new C
  c.b = new B
  c.b.a = new A
  c.b.a.x = new int[10]
  c.b.a.x[0] = 123
  print("foo-")
  print(itos(v))
  print("-")
  print(itos(c.b.a.x[0]))
  print("done")
end
"""

test_script_func = """
proc test{}(foo:proc(int,int:bool)) : int
  if foo(1, 2) then
    return 1
  else 
    return 0
  end
  x = 1
  y = 2
  z : bool
  w = 2
  X = 2
  Y = 2 * 3 + 4
  if Y == 3 then
    while Y == 0 do
      Y = Y - 1
      # print(itos(mod(Y + 100, 10)))
    end
  end
  Z = 2
  if x == 1 then
     x = 0 + 1231231 * 124349 / 23482349
     y = 0 + 3423 - 234234 * -(234 + 4324)
     z = x == y
  elif !(y == 1) then
     x = 0
  else
     y = 1
  end
  return 1
end
"""

for i in range(1000):
    test_script += test_script_func.format(i)

import datetime
start = datetime.datetime.now()
print(str(run(test_script)))
print('{}s'.format((datetime.datetime.now()-start).total_seconds()))
