#!/usr/local/bin/python3

#
# Copyright (c) 2020 Curt Knobloch
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import array, base64, os, errno
from collections import OrderedDict as D

LIB_PATH = os.path.dirname(__file__)

VERSION = 'toner-cartridge'

#
# UTILITIES
#

def D_index(self, k):
    i = 0
    for kk in self.keys():
        if kk == k:
            return i
        i += 1
    raise KeyError(k)

def D_last(self):
    return self[next(reversed(self))]

def isword(text):
    return text.isalpha() or text.isdigit() or text == '_'

def all_bases(cls):
    """Returns all base calsses for the given class."""
    for base in cls.__bases__:
        return [cls] + all_bases(base)
    else:
        return [cls]

#
# COMPILER
#

#
# Mini BNF language reference (used to parse Pinky code)
#
# 0x00        : end of expression
# 0x01 - 0x09 : token
# 0x0A - 0x1F : keyword (see table above)
# 0x20 - 0x7F : literal character
# 0x80 - 0xFF : rule
# ()          : group
# []          : optional group
# {}          : optional repeat
# \           : escape (the symbol that follows)
# ~           : do not ignore whitespace
#

RULES       = [None] * 0xFF
RULES[0x80] = "[\xE0]{\x82\xE0}\x02"                    # syntax
RULES[0x81] = "[\xE0]{\x83\xE0}"                        # body
RULES[0x82] = "\x8A|\x8B|\x8D|\x90|\x94|\xA0|\xA8"      # stmt (in the global body)
RULES[0x83] = "\x85|\x8E|\x95|\x96|\xA0|\xA8|\x84|\x91" # stmt (in a procedure)
RULES[0x84] = "\xB0"                                    # stmt_expr
RULES[0x85] = "\x86\xE0\x81[\x88|\x87]\x10"             # stmt_if_elif
RULES[0x86] = "\x11\xB0\x1A"                            # if_then ("if ... then")
RULES[0x87] = "\x12\xE0\x81"                            # else_block
RULES[0x88] = "\x89\xE0\x81[\x88|\x87]"                 # elif_block
RULES[0x89] = "\x13\xB0\x1A"                            # elif ("elif . then")
RULES[0x8A] = "\x1D\x04[:\x04]\xE0{\x8C\xE0}\x10"       # struct
RULES[0x8B] = "\x0F\x04:\xD3/\xD3"                      # stmt_units
RULES[0x8C] = "\x04(:|!)\xD1"                           # struct_attr
RULES[0x8D] = "\x1C\x04"                                # stmt_type
RULES[0x8E] = "\x17\x8F\x1B\xE0\x81\x10"                # stmt_while ("while . do ... end")
RULES[0x8F] = "\xB0"                                    # while_expr
RULES[0x90] = "\x18\x04\xC9"                            # native
RULES[0x91] = "$($|\x83)"                               # mark
RULES[0x92] = "\x81"                                    # proc_body
RULES[0x93] = "\x19\x04"\
              "\\([\x04:\xD1{,\x04:\xD1}]\\)"\
              "[:\xD1]"                                 # proc
RULES[0x94] = "\x93\xE0[\x92]\x10"                      # stmt_proc
RULES[0x95] = "(\x14|\x1E|\x1F)[\xB0]"                  # ret_yield_after
RULES[0x96] = "\x0D\x97\x1B\xE0\x81\x10"                # stmt_for ("for x = . to . do ... end")
RULES[0x97] = "\x04=\xB0\x0E\xB0"                       # for_expr
RULES[0x9F] = "\xAB"                                    # set_one_attr (set_multi_attrs)
RULES[0xA0] = "\xA1|\xA5\xAB"                           # stmt_set
RULES[0xA1] = "\x04=\xB0"                               # set_var
RULES[0xA2] = ".\x04=\xB0"                              # set_attr
RULES[0xA3] = "\\[\xB0\\]=\xB0"                         # set_elem
RULES[0xA4] = "\xA5\xAC"                                # get_var_attr
RULES[0xA5] = "\x04"                                    # get_var
RULES[0xA6] = ".\x04"                                   # get_attr
RULES[0xA7] = "\\[\xB0\\]"                              # get_elem
RULES[0xA8] = "\x04:\xD1"                               # let
RULES[0xA9] = "\x15\xD1"                                # as
RULES[0xAA] = "\\([\xAD{,\xAD}]\\)"                     # call
RULES[0xAB] = "\xA2|\xA3|(\xA6|\xA7|\xAA)\xAB"          # set_nested_attr
RULES[0xAC] = "{\xA6|\xA7|\xA9|\xAA|\xAF}"              # get_nested_attr
RULES[0xAD] = "\xB0|\xAE"                               # arg
RULES[0xAE] = "*\xA4"                                   # args
RULES[0xAF] = "\\{[\x9F{,\x9F}]\\}"                     # set_multi_attrs
RULES[0xB0] = "    \xBB{\xB2|\xB4|\xB6|\xB8|\xBA}"      # expr
RULES[0xB2] = "\\| \xBB{\xB4|\xB6|\xB8|\xBA}"           # or_rhs
RULES[0xB4] = "\\& \xBB{\xB6|\xB8|\xBA}"                # and_rhs
RULES[0xB6] = "\x06\xBB{\xB8|\xBA}"                     # cmp_rhs
RULES[0xB8] = "(+|-)\xBB{\xBA}"                         # arith_lo_rhs
RULES[0xBA] = "(*|/)\xBB"                               # arith_hi_rhs
RULES[0xBB] = "[-|!]\xBC"                               # unary
RULES[0xBC] = "\xC0\xAC"                                # trailing_unary
RULES[0xC0] = "\xC1|\xC2|\xC3|\xC4|\xC5|\xC6|\xC7|"\
              "\xA4"                                    # term
RULES[0xC1] = "\xD3"                                    # num
RULES[0xC2] = "\x05"                                    # str
RULES[0xC3] = "\x16\xD2"                                # new
RULES[0xC4] = "\\(\xB0\\)"                              # group
RULES[0xC5] = "@\x05"                                   # bytes
RULES[0xC6] = "`\x04.\x04`"                             # attr_index
RULES[0xC7] = "\\{[\xAD{,\xAD}]\\}"                     # initializer
RULES[0xC8] = "\\[[\x03]\\]"                            # type_elem
RULES[0xC9] = "\\([\xD1{,\xD1}][:\xD1]\\)"              # type_proc
RULES[0xD1] = "(\x04|\x19\xC9|\x1D|*|\\(\xD1\\))"\
              "[\xC8]"                                  # type
RULES[0xD2] = "(\x04|\x1D|*)"\
              "[\xC8]"                                  # newable_type
RULES[0xD3] = "\x03[.\x03][~\x04]"                      # raw_num
RULES[0xE0] = "\x01{\x01}|;"                            # eol
RULES[0x00] = "\x80"  # NOTE: zero rule can't be referenced, it's always the head rule
RULES       = [r+'\x00' if r else r for r in RULES]     # append sentinel character to all rules

KEYWORDS = (
    # 0x0A
    "",
    "",
    "",
    "for",
    "to",
    "unit",
    # 0x10
    "end",
    "if",
    "else",
    "elif",
    "return",
    "as",
    "new",
    "while",
    # 0x18
    "native",
    "proc",
    "then",
    "do",
    "type",
    "struct",
    "yield",
    "after"
)

NOP , NULL, ZERO, ONE , LODI, LODS, LODF, ADD ,\
SUB , MUL , DIV , MULF, DIVF, NOT , NEG , LT  ,\
GT  , LTE , GTE , EQ  , NE  , JMP , JIF , JZ  ,\
JSR , RET , DUP , DROP, SETA, SETI, SETL, SETG,\
GETA, GETI, GETL, GETG, NEW , NEWV, REM , EVAL,\
MARK, STEP, CLR , ARGS = JAM_OPS = range(44)

JAM_OP_STRS = dict(zip(JAM_OPS, (
    'nop ', 'null', 'zero', 'one ', 'lodi', 'lods', 'lodf', '+   ',
    '-   ', '*   ', '/   ', '*f  ', '/f  ', 'not ', 'neg ', '<   ',
    '>   ', '<=  ', '>=  ', '==  ', '!=  ', 'jmp ', 'jif ', 'jz  ',
    'jsr ', 'ret ', 'dup ', 'drop', 'seta', 'seti', 'setl', 'setg',
    'geta', 'geti', 'getl', 'getg', 'new ', 'newv', '#   ', 'eval',
    'mark', 'step', 'clr ', 'args')))

class ScriptError(Exception):
    def __init__(self, msg, line_no=None):
        super(ScriptError, self).__init__()
        self.msg = msg
        self.line_no = line_no
        
    def __str__(self):
        if self.line_no is None:
            return self.msg
        else:
            return '{} (line {})'.format(self.msg, self.line_no+1)

class SyntaxTreeNode:
    def __init__(self, id):
        self.id = id

class SyntaxTreeTerminal(SyntaxTreeNode):
    def __init__(self, id, text):
        SyntaxTreeNode.__init__(self, id)
        self.text = text
        
    def __str__(self):
        return self.text
    
class SyntaxTreeExpression(SyntaxTreeNode):
    def __init__(self, id, lc=None):
        SyntaxTreeNode.__init__(self, id)
        self.sub_nodes = []
        self.lc = lc
        
    def __getitem__(self, i):
        return self.sub_nodes[i]
    
    def __len__(self):
        return len(self.sub_nodes)
    
    def __str__(self):
        return '[{}]'.format(', '.join(map(str, self.sub_nodes)))
    
    def append(self, x):
        self.sub_nodes.append(x)

class PinkyType:    
    def __str__(self):
        return '*'

    def equal(self, other):
        """Returns True if the two type instances are equal, looking at their attributes."""
        return True
    
    def is_subtype(self, other):
        """
        Returns True if this is the same type or is a subtype of other type.
        Another way to read this is: `self` can be assigned to `other`.
        """
        if self.__class__ is PinkyType:
            return True  # allow auto casting '*' to any type
        for cls in all_bases(self.__class__):
            if cls is other.__class__:
                return cls.equal(self, other)
        return False

class PinkyUserType(PinkyType):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name
    
    def equal(self, other):
        return self is other

class PinkyNumType(PinkyType):
    def __str__(self):
        return 'num'
    
class PinkyIntType(PinkyNumType):
    def __str__(self):
        return 'int'

class PinkyBoolType(PinkyIntType):
    def __str__(self):
        return 'bool'
    
class PinkyFpType(PinkyNumType):
    def __str__(self):
        return 'fp'
    
class PinkyStrType(PinkyType):
    def __str__(self):
        return 'str'    

class PinkyVoidType(PinkyType):
    def __str__(self):
        return 'void'
    
    def is_subtype(self, other):
        return other.__class__ is PinkyVoidType

class PinkyBlobType(PinkyType):
    def __str__(self):
        return 'blob'
    
class PinkyCallableType(PinkyType):
    def __init__(self, arg_types=None, ret_type=None, name=None, args=None, off=None):
        self.args = args
        self.arg_types = arg_types
        self.ret_type = ret_type
        self.name = name        
        self.off  = off
    
    def __str__(self):
        args_str = ','.join(map(str, self.arg_types)) if self.arg_types is not None else '?'
        return '{0}({1}:{2})'.format(
            self.name or 'proc',
            args_str,
            '?' if self.ret_type is None else str(self.ret_type))
    
    def get_vars(self):
        args = self.args
        if not args:
            args = ['_{}'.format(x+1) for x in range(len(self.arg_types))]
        return D(this=self, **D(zip(args, self.arg_types)))
    
    def equal(self, other):
        if other.ret_type is not None and (
                self.ret_type is None or not self.ret_type.is_subtype(other.ret_type)
        ):
            return False
        if other.arg_types is not None:
            if len(self.arg_types) != len(other.arg_types):
                return False
            for a_type, b_type in zip(self.arg_types, other.arg_types):
                if not a_type.is_subtype(b_type):
                    return False
        return True

class PinkyProcType(PinkyCallableType):
    pass

class PinkyBuiltinType(PinkyCallableType):
    pass
        
class PinkySequenceType(PinkyType):
    def __init__(self, value_types=None):
        self._value_types = value_types

    def __str__(self):
        if self._value_types is None:
            return 'sequence'
        return '{{{}}}'.format(','.join(map(str, self.value_types())))
            
    def __len__(self):
        return len(self.value_types())

    def value_types(self):
        return self._value_types or []

    def value_type_at(self, index):
        return self._value_types[index] if self._value_types else undefined_type
    
    def equal(self, other):
        if len(self) and len(other) and len(self) != len(other):
            return False
        for i, type_ in enumerate(self.value_types()):
            if not type_.is_subtype(other.value_type_at(i)):
                return False
        return True

    def is_subtype(self, other):
        if self.__class__ is PinkySequenceType and \
           isinstance(other, PinkySequenceType):
            return self.equal(other)  # initializer can be subtype of PinkySequenceType sub classes
        return PinkyType.is_subtype(self, other)
    
class PinkyStructType(PinkySequenceType):
    def __init__(self, name=None, attrs=None, sup=None):
        self.name  = name
        self.attrs = D()
        self.sup = None
        if sup:
            self.set_super(sup)
        if attrs:
            self.attrs.update(attrs)
            
    def __str__(self):
        return self.name or 'struct'

    def set_super(self, sup):
        self.sup = sup
        self.attrs.update(sup.attrs)

    def value_types(self):
        return list(self.attrs.values())
    
    def value_type_at(self, index):
        return list(self.attrs.values())[index] if self.attrs else undefined_type
        
    def equal(self, other):
        if other.name is None:
            return True
        return self.is_substruct(other)

    def is_substruct(self, other):
        if self.name == other.name:
            return True
        if self.sup is None:
            return False
        return self.sup.is_substruct(other)

class PinkyArrayType(PinkySequenceType):
    def __init__(self, value_type=None, num_values=None):
        self.value_type = undefined_type if value_type is None else value_type
        self.num_values = num_values 
        
    def __str__(self):
        return '{0}[{1}]'.format(str(self.value_type), len(self) or "")

    def __len__(self):
        return self.num_values or 0
    
    def value_types(self):
        return [self.value_type for _ in range(self.num_values)]

    def value_type_at(self, index):
        return self.value_type
    
    def equal(self, other):
        if not self.num_values or not other.num_values:
            return True
        if len(self) != len(other):
            return False
        return self.value_type.is_subtype(other.value_type)

undefined_type = PinkyType()
bool_type      = PinkyBoolType()
num_type       = PinkyNumType()
int_type       = PinkyIntType()
fp_type        = PinkyFpType()
str_type       = PinkyStrType()
void_type      = PinkyVoidType()
proc_type      = PinkyProcType()
sequence_type  = PinkySequenceType()
struct_type    = PinkyStructType()
array_type     = PinkyArrayType()
blob_type      = PinkyBlobType()

def run(src, filename='?', **options):
    from pinky import PinkyVM
    PinkyVM.from_jamcode(
        Compiler(**options).compile(
            [(filename, src)]
        )
    ).run()

class Compiler:
    def __init__(self, libs=None, resources=None, res_path='', debug=False):
        if libs is None:
            libs = ('core.ky',)
            
        self.libs = tuple(
            (f, open(os.path.join(LIB_PATH, f), 'rt').read())
            for f in libs)
        
        self.debug     = debug
        self.res_path  = res_path        
        self.resources = resources or {}
        self.code      = []
        self.code_off  = 0
        self.js        = []  # jump  stack
        self.ys        = []  # type  stack
        self.ps        = []  # param stack
        self.vs        = [D(this=undefined_type)]  # variable stack
        self.units     = {}
        self.natives   = 0
        
        self.structs   = {
            self.key_to_id('bool'): bool_type,
            self.key_to_id('*'): undefined_type,
            self.key_to_id('int') : int_type,
            self.key_to_id('fp'): fp_type,
            self.key_to_id('str') : str_type,
            self.key_to_id('proc'): proc_type,
            self.key_to_id('struct'): struct_type,
            self.key_to_id('array'): array_type,
            self.key_to_id('blob'): blob_type,
        }
        
        self.builtins = {
            name: PinkyBuiltinType(
                arg_types=arg_types,
                ret_type=ret_type,
                name=name,
                off=i+len(JAM_OPS),
            )
            for i, (name, arg_types, ret_type) in enumerate([
                    ('assert', [bool_type], void_type),
                    ('print', [str_type], void_type),
                    ('time', [], int_type),
                    ('rand', [int_type, int_type], int_type),
                    ('seed', [int_type], void_type),
                    ('weak', [struct_type], struct_type),
                    ('is', [undefined_type,undefined_type], bool_type),
                    ('default', [undefined_type,undefined_type], undefined_type),
                    ('select', [bool_type,undefined_type,undefined_type], undefined_type),
                    ('sleep', [int_type], void_type),
                    ('macro', [proc_type], proc_type),
                    ('step', [proc_type], int_type),
                    ('gettype', [undefined_type], int_type),
                    ('clone', [struct_type], struct_type),
                    ('attrs', [struct_type], PinkyArrayType()),
                    ('and', [int_type,int_type], int_type),
                    ('or', [int_type,int_type], int_type),
                    ('xor', [int_type,int_type], int_type),
                    ('shl', [int_type,int_type], int_type),
                    ('shr', [int_type,int_type], int_type),
                    ('ror', [int_type,int_type,int_type], int_type),
                    ('bits', [int_type,int_type], int_type),
                    ('setbits', [int_type,int_type,int_type], int_type),
                    ('signed', [int_type], int_type),                    
                    ('mod', [int_type,int_type], int_type),
                    ('min', [int_type,int_type], int_type),
                    ('max', [int_type,int_type], int_type),
                    ('abs', [int_type], int_type),
                    ('icmp', [int_type,int_type], int_type),
                    ('itos', [int_type], str_type),
                    ('fmod', [fp_type,fp_type], fp_type),
                    ('fmin', [fp_type,fp_type], fp_type),
                    ('fmax', [fp_type,fp_type], fp_type),
                    ('fabs', [fp_type], fp_type),
                    ('fcmp', [fp_type,fp_type], fp_type),
                    ('ftos', [fp_type], str_type),
                    ('ftoi', [fp_type], int_type),
                    ('fp', [int_type,int_type], fp_type),
                    ('floor', [fp_type], fp_type),
                    ('ceil', [fp_type], fp_type),
                    ('frac', [fp_type], fp_type),
                    ('pow', [fp_type,fp_type], fp_type),
                    ('sqrt', [fp_type], fp_type),
                    ('hypot', [fp_type,fp_type], fp_type),
                    ('sin', [fp_type], fp_type),
                    ('cos', [fp_type], fp_type),
                    ('tan', [fp_type], fp_type),
                    ('asin', [fp_type], fp_type),
                    ('acos', [fp_type], fp_type),
                    ('atan', [fp_type], fp_type),
                    ('concat', [str_type,str_type], str_type),
                    ('chr', [int_type], str_type),
                    ('ord', [str_type,int_type], int_type),
                    ('slen', [str_type], int_type),
                    ('scmp', [str_type,str_type], int_type),
                    ('stoi', [str_type], int_type),
                    ('decodeb64', [str_type], blob_type),
                    ('b64', [blob_type], str_type),
                    ('substr', [str_type,int_type,int_type], str_type),
                    ('slice', [PinkyArrayType(),int_type,int_type], PinkyArrayType()),
                    ('reverse', [PinkyArrayType()], PinkyArrayType()),
                    ('fill',
                     [
                         PinkyArrayType(),
                         undefined_type,
                         int_type,
                         int_type
                     ],
                     void_type),
                    ('copy',
                     [
                         PinkyArrayType(),
                         PinkyArrayType(),
                         int_type,
                         int_type
                     ],
                     void_type),
                    ('sort',
                     [
                         PinkyArrayType(),
                         PinkyProcType([undefined_type,undefined_type], int_type)
                     ],
                     void_type),
                    ('apply',
                     [
                         PinkyArrayType(),
                         PinkyProcType([undefined_type], void_type)
                     ],
                     void_type),
                    ('alen', [PinkyArrayType()], int_type),
                    ('array', [int_type], PinkyArrayType()),
                    ('blob', [int_type], blob_type),
                    ('bload', [str_type], blob_type),
                    ('bsave', [blob_type,str_type], void_type),
                    ('blen', [blob_type], int_type),
                    ('get', [blob_type,int_type], int_type),
                    ('set', [blob_type,int_type,int_type], void_type),
                    ('gets', [blob_type,int_type,int_type], str_type),
                    ('sets', [blob_type,int_type,str_type,int_type], void_type),
            ])
        }
        
        self.expr_to_interm_code_pre_funcs  = \
            self._expr_to_interm_code_pre_funcs()
        self.expr_to_interm_code_post_funcs = \
            self._expr_to_interm_code_post_funcs()

    def compile(self, src_list):
        # Compile all files sequentially
        for name, src in tuple(self.libs)+tuple(src_list):
            self.code.append((REM, "@"+name))
            self.compile_fragment(src)

        # Get program entry-point
        run_id = self.key_to_id('run')
        if not (self.vs[0].get(run_id).is_subtype(
                PinkyProcType(arg_types=[], ret_type=void_type))):
            raise ScriptError('No program entry point defined')

        self.code.extend([(GETG, D_index(self.vs[0], run_id)), (JSR, 0), (RET,)])

        return self.to_jamcode()
    
    def compile_fragment(self, src):
        self._expr_to_interm_code(Parser(RULES, KEYWORDS).parse(src)[0])
        return self  # allow chaining

    def to_jamcode(self):
        lines = []
        for line in self.code:
            int_opcode = line[0]
            if int_opcode < len(JAM_OPS):
                opcode = JAM_OP_STRS[int_opcode]
            else:
                opcode = "x{0:0{1}x}".format(int_opcode, 3)
            if len(line) == 1:
                lines.append((opcode,))
            else:
                lines.append((opcode, ' '.join(map(str, line[1:]))))            
        return '\n'.join(' '.join(l) for l in lines)

    def _expr_to_interm_code(self, expr):
        if not isinstance(expr, SyntaxTreeExpression):
            return

        rule = expr.id
        
        self.expr_to_interm_code_pre_funcs .get(rule, lambda _: None)(expr)
        
        for node in expr.sub_nodes:
            self._expr_to_interm_code(node)

        # This is a hacky way to avoid having to pass line number into
        # every assertion in post funcs
        self.line_no = expr.lc

        self.expr_to_interm_code_post_funcs.get(rule, lambda _: None)(expr)

    def _expr_to_interm_code_pre_funcs(self):
        
        code = self.code
        
        def _pre_syntax(expr):
            self.js.append(self.here())
            code.append((JMP, 0))
            self.code_off = self.here()

        def _pre_stmt(expr):
            if self.debug:
                code.append((REM, str(expr.lc+1)))

        def _pre_elif_else_block(expr):
            th = self.js.pop()
            self.js.append(self.here())
            code.append((JMP, 0))
            code[th] = (code[th][0], self.here()-th)

        def _pre_stmt_while(expr):
            self.js.append(self.here())

        def _pre_set_one_attr(expr):
            self.ys.append(self.ys[-1])            
            code.append((DUP,))

        def _pre_call(expr):
            self.ps.append(len(self.ys))

        def _pre_and_rhs(expr):
            code.append((DUP,))
            self.js.append(self.here())
            code.append((JZ, 0))
            code.append((DROP,))

        def _pre_or_rhs(expr):
            code.append((DUP,))
            self.js.append(self.here())
            code.append((JIF, 0))
            code.append((DROP,))

        def _pre_initializer(expr):
            self.ps.append(len(self.ys))
            
        return {
            0x80: _pre_syntax,
            0x82: _pre_stmt,
            0x83: _pre_stmt,
            0x87: _pre_elif_else_block,
            0x88: _pre_elif_else_block,
            0x8E: _pre_stmt_while,
            0x9F: _pre_set_one_attr,
            0xAA: _pre_call,
            0xB2: _pre_or_rhs,
            0xB4: _pre_and_rhs,
            0xC7: _pre_initializer,            
        }        

    def _expr_to_interm_code_post_funcs(self):
        
        code = self.code

        def _post_syntax(expr):
            th = self.js.pop()
            op, addr = code[th]
            code[th] = (op, self.code_off)
            self.code_off = self.here()

        def _post_stmt(expr):
            self.assert_(len(self.ys)==1, 'Type stack depth is not 1')

        def _post_stmt_expr(expr):
            if not self.ys.pop().is_subtype(void_type):
                code.append((DROP,))

        def _post_if_elif_while_expr(expr):
            self.assert_type(self.ys.pop(), bool_type)
            self.js.append(self.here())
            code.append((JZ, 0))

        def _post_struct(expr):
            k = self.get_id(expr[1])            
            if k in self.structs:
                self.assert_type(self.structs[k], struct_type)
                self.assert_(not self.structs[k].attrs,
                             'Struct {!r} already defined'.format(k))
                struct = self.structs[k]  # use existing struct object (or refs stay blank)
            else:
                self.structs[k] = struct = PinkyStructType(name=k)

            if self.termchr(expr[2]) == ':':
                sup = self.structs[self.get_id(expr[3])]
                expr = expr[5:]
            else:
                sup = struct_type  # all structs extend the base struct
                expr = expr[3:]

            struct.set_super(sup)
                
            attrs = self.structs[k].attrs
            i = 0
            while expr[i].id != 0x10:
                k, operator, v = expr[i]  # k : v -or- k ! v
                k = self.get_id(k)
                y = self.type_(v)
                if self.termchr(operator) == ':':
                    self.assert_(
                        k not in attrs,
                        '{}.{} already defined'.format(struct, k))
                else:
                    self.assert_(k in attrs,
                        '{}.{} not defined so cannot be overridden'.format(
                            struct, k, attrs[k], y))
                    self.assert_(
                        y.is_subtype(attrs[k]),
                        'Cannot override {}.{}:{} with {}'.format(
                            struct, k, attrs[k], y))
                attrs[k] = y
                i += 2  # skip eol

        def _post_stmt_units(expr):
            t1, v1 = self.termnum(expr[3])
            t2, v2 = self.termnum(expr[5])
            self.assert_type(t2, t1)
            self.units[self.get_id(expr[1])] = (t1, v1, v2)

        def _post_stmt_type(expr):
            k = self.get_id(expr[1])
            self.structs[k] = PinkyUserType(k)

        def _post_stmt_while(expr):
            th = self.js.pop()
            code.append((JMP, self.js.pop()-self.here()))
            code[th] = (code[th][0], self.here()-th)

        def _post_stmt_for(expr):
            i = D_index(self.vs[-1], self.get_id(expr[1][0]))
            code.append((GETL, i))
            code.append((ONE,))
            code.append((ADD,))
            code.append((SETL, i))
            
            _post_stmt_while(expr)

        def _post_for_expr(expr):
            self.assert_type(self.ys.pop(), int_type)
            self.assert_type(self.ys.pop(), int_type)

            # Initialize current/end indexes
            k = self.get_id(expr[0])
            self.vs[-1][k] = int_type
            self.vs[-1][str(len(self.vs[-1]))] = int_type
            i = D_index(self.vs[-1], k)
            code.append((SETL, i+1))
            code.append((SETL, i))

            # Mark start of loop
            self.js.append(self.here())

            # Check current index < end index
            code.append((GETL, i))
            code.append((GETL, i+1))
            code.append((LT,))
            self.ys.append(bool_type)

            _post_if_elif_while_expr(expr)

        def _post_native(expr):
            k = self.termstr(expr[1])
            self.vs[0][k] = self.prototype(expr[2])
            self.natives += 1
            code.append((EVAL, self.natives-1))
            code.append((LODF,
                         self.write_proc(self.here()-1),
                         len(self.vs[0][k].get_vars())))
            code.append((SETG, D_index(self.vs[0], k)))

        def _post_proc(expr):
            proc = self.proc_type(expr)
            self.assert_type(proc, self.vs[-1].get(proc.name, undefined_type))
            self.vs[-1][proc.name] = proc
            self.js.append(self.here())
            code.append((REM, str(proc)))
            self.vs.append(proc.get_vars())
            self.ys.append(proc.ret_type)

        def _post_stmt_proc(expr):
            self.ys.pop()
            th = self.js.pop()
            vn = len(self.vs[-1])
            self.vs.pop()
            k = self.termstr(expr[0][1])
            last_i = self.here() - 1
            while code[last_i][0] == REM:
                last_i -= 1
            if self.vs[-1][k].ret_type is not void_type:
                self.assert_(code[last_i][0] == RET, "Unexpected return value")
            else:
                code.append((RET,))
            code.append((REM, 'end proc'))
            code.append((LODF, self.write_proc(th), vn))
            code.append((SETG, D_index(self.vs[-1], k)))

        def _post_ret_yield_after(expr):
            if expr[0].id == 0x14:
                # ret
                if len(expr) == 1:
                    self.assert_type(void_type, self.ys[-1])
                else:
                    self.assert_type(self.ys.pop(), self.ys[-1])
                code.extend([(RET,)])
            else:
                # after, yield
                if expr[0].id == 0x1F:
                    # after
                    if len(expr) == 1:
                        self.assert_type(void_type, self.ys[-1])
                    elif int_type.is_subtype(self.ys[-1]):
                        self.ys.pop()
                        self.assert_type(void_type, self.ys[-1])
                        code.extend([(STEP,), (NE,), (JIF, 2)])
                    else:
                        self.assert_type(bool_type, self.ys.pop())
                        self.assert_type(void_type, self.ys[-1])
                        code.extend([(JIF, 2)])
                else:
                    # yield
                    if len(expr) == 1:
                        self.assert_type(void_type, self.ys[-1])
                    else:
                        self.assert_type(self.ys.pop(), self.ys[-1])
                code.extend([(MARK, 1), (RET,), (CLR,)])

        def _post_set_var(expr):
            k = self.get_id(expr[0])
            if   k in self.vs[-1] and len(self.vs) > 1:
                self.assert_type(self.ys.pop(), self.vs[-1][k])
                code.append((SETL, D_index(self.vs[-1], k)))
            elif k in self.vs [0]:                
                self.assert_type(self.ys.pop(), self.vs [0][k])
                code.append((SETG, D_index(self.vs [0], k)))
            else:  # implicit declaration
                if isinstance(self.ys[-1], PinkyBuiltinType):
                    self.raise_script_error(
                        'Cannot assign builtin {!s}'.format(self.ys[-1]))
                self.vs[-1][k] = self.ys.pop()
                code.append((SETL if len(self.vs) > 1 else SETG,
                             D_index(self.vs[-1], k)))         

        def _post_set_attr(expr):
            x = self.ys.pop()
            k = self.get_id(expr[1])
            y = self.ys.pop()
            self.assert_type(y, struct_type)
            self.assert_(k in y.attrs,
                'Attribute {!r} does not exist in {!s}'.format(k, y))
            self.assert_type(x, y.attrs[k])
            code.append((SETA, D_index(y.attrs, self.get_id(expr[1]))))

        def _post_set_elem(expr):
            x = self.ys.pop()
            i = self.ys.pop()
            y = self.ys.pop()
            self.assert_type(y, array_type)
            self.assert_type(i, int_type)
            self.assert_type(x, y.value_type)
            code.append((SETI,))

        def _post_get_var(expr):
            k = self.get_id(expr[0])
            if   k in self.vs[-1] and len(self.vs) > 1:
                self.ys.append(self.vs[-1][k])
                code.append((GETL, D_index(self.vs[-1], k)))
            elif k in self.vs [0]:
                self.ys.append(self.vs [0][k])
                code.append((GETG, D_index(self.vs [0], k)))
            elif k == 'true' or k == 'false':
                self.ys.append(bool_type)
                code.append((ONE if k[0]=='t' else ZERO,))
            elif k == 'null':
                self.ys.append(undefined_type)
                code.append((NULL,))
            elif k == 'this':
                self.ys.append(proc_type)
                code.append((GETL, 0))
            elif k in self.builtins:
                self.ys.append(self.builtins[k])
            else:
                self.raise_script_error('Variable {!r} undefined'.format(k))

        def _post_get_attr(expr):
            k = self.get_id(expr[1])
            y = self.ys.pop()
            self.assert_type(y, struct_type)
            self.assert_(k in y.attrs,
                'Attribute {!r} does not exist in {!s}'.format(k, y))
            self.ys.append(y.attrs[k])
            code.append((GETA, D_index(y.attrs, self.get_id(expr[1]))))

        def _post_get_elem(expr):
            i = self.ys.pop()
            y = self.ys.pop()
            self.assert_type(y, array_type)
            self.assert_type(i, int_type)
            self.ys.append(y.value_type)
            code.append((GETI,))

        def _post_let(expr):
            k = self.get_id(expr[0])
            self.assert_(
                k not in self.vs[-1],
                'Variable {!r} already defined'.format(k))
            self.vs[-1][k] = self.type_(expr[2])

        def _post_as(expr):
            y = self.type_(expr[1])
            x = self.ys.pop()
            self.assert_(
                x.is_subtype(y) or y.is_subtype(x),
                'Cannot cast {!s} to {!s}'.format(x, y))
            self.ys.append(y)

        def _post_args(expr):
            y = self.ys.pop()
            self.assert_type(y, sequence_type)
            self.ys.extend(y.value_types())
            code.append((ARGS,))

        def _post_call(expr):
            off = self.ps.pop()
            arg_types = self.ys[off:]
            del self.ys[off:]
            y = self.ys.pop()
            if isinstance(y, PinkyBuiltinType):
                self.assert_type(PinkyBuiltinType(arg_types, y.ret_type), y)
                code.append((y.off,))
            else:
                self.assert_type(PinkyProcType(arg_types, y.ret_type), y)
                code.append((JSR, len(arg_types)))
            self.ys.append(y.ret_type)

        def _post_stmt_if_elif(expr):
            th = self.js.pop()
            code[th] = (code[th][0], self.here()-th)            

        def _post_and_or(expr):
            self.assert_type(self.ys[-1], bool_type)
            self.assert_type(self.ys.pop(), self.ys[-1])
            th = self.js.pop()
            code[th] = (code[th][0], self.here()-th)

        def _post_cmp(expr):
            self.assert_type(self.ys[-1], num_type)
            self.assert_type(self.ys.pop(), self.ys.pop())            
            self.ys.append(bool_type)
            code.append(({
                '==': EQ,
                '!=': NE,
                '<=': LTE,
                '>=': GTE,
                '<' : LT,
                '>' : GT,
            }[self.termstr(expr[0])],))

        def _post_arith_lo_rhs(expr):
            self.assert_type(self.ys[-1], num_type)
            self.assert_type(self.ys.pop(), self.ys[-1])
            c = self.termchr(expr[0])
            code.append((ADD if c == '+' else SUB,))

        def _post_arith_hi_rhs(expr):
            self.assert_type(self.ys[-1], num_type)
            self.assert_type(self.ys[-2], num_type)
            y = self.ys.pop()            
            c = self.termchr(expr[0])
            if   fp_type is y and fp_type  is self.ys[-1]:
                code.append((MULF if c == '*' else DIVF,))
            elif fp_type is y:
                if c == '/':
                    self.raise_script_error('Cannot divide integer by fraction')
                code.append((MUL  if c == '*' else DIV,))
                self.ys.pop()
                self.ys.append(fp_type)
            else:  # left operand may be int or num
                code.append((MUL  if c == '*' else DIV,))

        def _post_unary(expr):
            if len(expr) > 1:
                c = self.termchr(expr[0])
                if   c == '-':  # neg
                    self.assert_type(self.ys[-1], num_type)
                    if code[-1][0] == LODI:
                        code[-1] = (LODI, -code[-1][1])
                    else:
                        code.append((NEG,))
                elif c == '!':  # not
                    self.assert_type(self.ys[-1], bool_type)
                    code.append((NOT,))

        def _post_num(expr):
            y, v = self.termnum(expr[0])
            self.ys.append(y)
            if   v == 0:
                code.append((ZERO,))
            elif v == 1:
                code.append((ONE,))
            else:
                code.append((LODI, v))

        def _post_str(expr):
            self.ys.append(str_type)            
            code.append((LODS, self.termstr(expr[0])[1:-1]))

        def _post_new(expr):
            self.ys.append(self.type_(expr[1]))
            code.append((NEW, len(self.ys[-1])))

        def _post_bytes(expr):
            self.ys.append(str_type)
            filename = self.termstr(expr[1])[1:-1]
            def append_code(f, filename):
                code.append((REM, 'file({})'.format(filename)))
                code.append((LODS, base64.b64encode(f.read()).decode('ascii')))
            if filename in self.resources:
                f = self.resources[filename]
                append_code(f, filename)
                f.seek(0)
            else:
                try:
                    with open(self.res_path+filename, 'rb') as f:
                        append_code(f, filename)
                except IOError as e:
                    if e.errno != errno.ENOENT:
                        raise
                    self.raise_script_error("Cannot find resource '{}'".format(filename))

        def _post_attr_index(expr):
            self.ys.append(int_type)
            struct_id, attr_id = self.get_id(expr[1]), self.get_id(expr[3])
            code.append((LODI, D_index(self.structs[struct_id].attrs, attr_id)))

        def _post_initializer(expr):
            off = self.ps.pop()
            values = self.ys[off:]
            del self.ys[off:]
            self.ys.append(PinkySequenceType(values))
            code.append((NEWV, len(self.ys[-1])))

        def _post_mark(expr):
            code.append((MARK, 0))            
            
        return {
            0x80: _post_syntax,
            0x83: _post_stmt,
            0x84: _post_stmt_expr,
            0x86: _post_if_elif_while_expr,
            0x89: _post_if_elif_while_expr,
            0x8A: _post_struct,
            0x8B: _post_stmt_units,
            0x8D: _post_stmt_type,
            0x8E: _post_stmt_while,
            0x8F: _post_if_elif_while_expr,
            0x90: _post_native,
            0x93: _post_proc,
            0x94: _post_stmt_proc,
            0x95: _post_ret_yield_after,
            0x96: _post_stmt_for,
            0x97: _post_for_expr,            
            0xA1: _post_set_var,
            0xA2: _post_set_attr,
            0xA3: _post_set_elem,
            0xA5: _post_get_var,
            0xA6: _post_get_attr,
            0xA7: _post_get_elem,
            0xA8: _post_let,
            0xA9: _post_as,
            0xAA: _post_call,
            0xAE: _post_args,
            0x85: _post_stmt_if_elif,
            0x88: _post_stmt_if_elif,
            0xB2: _post_and_or,
            0xB4: _post_and_or,
            0xB6: _post_cmp,
            0xB8: _post_arith_lo_rhs,
            0xBA: _post_arith_hi_rhs,
            0xBB: _post_unary,
            0xC1: _post_num,
            0xC2: _post_str,
            0xC3: _post_new,
            0xC5: _post_bytes,
            0xC6: _post_attr_index,
            0xC7: _post_initializer,
            0x91: _post_mark,
        }            
                
    def write_proc(self, th):
        code = self.code
        addr = self.code_off
        here = self.here()
        n    = here - th
        
        # Commit procedure code to current offset
        code[self.code_off:self.code_off] = code[th:]
        del code[here:]
        self.code_off += n
        
        # Resolve relative addresses
        for i in range(addr, addr+n):
            opcode = code[i][0]
            if opcode in {JMP, JIF, JZ}:
                code[i] = (opcode, i+code[i][1])
                
        return addr
                
    def type_(self, t):
        if self.termchr(t[0]) == '(':
            y = self.type_(t[1])
            t = t[3:]
        else:
            k = self.get_id(t[0])
            y = self.structs.get(k)
            t = t[1:]
            
        if y is None:
            self.assert_(y, 'Type {!r} not defined'.format(k))
            
        if t:
            t = t[0]                        
            if self.termchr(t[0]) == '[':
                n = 0 if self.termchr(t[1]) == ']' else self.termint(t[1])
                return PinkyArrayType(y, n)
            if self.termchr(t[0]) == '(':
                return self.prototype(t)
            else:
                raise ValueError('Invalid type')
            
        return y

    def prototype(self, t):
        # Examples:
        # ()
        # (a,b)
        # (a,b:c)
        
        arg_types = []
        ret_type = void_type
        
        t = t[1:-1]
        if t:
            i = 0
            while i < len(t):
                c = self.termchr(t[i])
                if   c == ':':
                    i += 1
                    ret_type = self.type_(t[i])
                elif c != ',':
                    arg_types.append(self.type_(t[i]))
                i += 1
                
        return PinkyProcType(arg_types=arg_types, ret_type=ret_type)

    def proc_type(self, t):        
        t = t[1:]
        name = self.get_id(t[0])
        args, arg_types = [], []
        
        t = t[1:]
        if self.termchr(t[1]) == ')':
            t = t[1:]
        else:
            while self.termchr(t[0]) != ')':
                args.append(self.get_id(t[1]))
                arg_types.append(self.type_(t[3]))
                t = t[4:]
                
        t = t[1:]        
        if t and self.termchr(t[0]) == ':':
            ret_type = self.type_(t[1])
        else:
            ret_type = void_type
            
        return PinkyProcType(name=name,
                             args=args,
                             arg_types=arg_types,
                             ret_type=ret_type)

    def get_id(self, term):
        return self.termstr(term)

    def key_to_id(self, k):
        return k

    def termchr(self, term):
        return str(term)

    def termstr(self, term):
        return str(term)
    
    def termint(self, term):
        return int(self.termstr(term), 0)

    def termnum(self, term):

        # All numbers at least start with an integer!
        value = self.termint(term[0])

        # Parse unit-of-measurement
        if term[-1].id != 0x04:
            unit = None
        else:
            try:
                unit = self.units[self.termstr(term[-1])]
            except KeyError as e:
                raise ScriptError(
                    'Unit of measurement {!r} '
                    'not defined'.format(e.args[0]))
            term = term[:-1]

        # Parse number
        if len(term) == 1:
            # Integer
            if unit is None:
                return int_type, value
            else:
                unit_type, x, y = unit
                self.assert_type(unit_type, int_type)
                return int_type, value*x//y
        else:
            # Decimal
            f_str = self.termstr(term[2])
            f = int(f_str.lstrip('0') or '0', 0)
            value = value<<16 | (f*(1<<16)//pow(10, len(f_str)))
            if unit is None:
                return fp_type, value
            else:
                unit_type, x, y = unit
                self.assert_type(unit_type, fp_type)                
                return fp_type, value*x//y

    def here(self):
        return len(self.code)

    def raise_script_error(self, msg):
        raise ScriptError(msg, line_no=self.line_no)

    def assert_(self, v, msg):
        if not v:
            self.raise_script_error(msg)
                        
    def assert_type(self, sub, super_):
        self.assert_(sub.is_subtype(super_),
                     'Mismatched types {!s} and {!s}'.format(sub, super_))

#
# PARSER
#

class Parser:
    pt = array.array('i', [0 for i in range(4000000)])
    
    def __init__(self, rules, keywords):
        self.rules = rules
        self.keywords = keywords
        self.reset()
        
    def begin_expression(self, rule):
        self.pt[self.i] = -rule
        self.i += 1
        
    def end_expression(self):
        self.pt[self.i] = 0
        self.i += 1
        
    def terminal(self, n):
        self.pt[self.i] = n
        self.i += 1
        
    def reset(self, i=0):
        self.i = i

    def parse(self, src):
        self.reset()
        
        tokens = Parser.tokenize(src+'\x04', self.keywords)
        
        end = self._parse_tokens_using_rules(tokens, self.rules)
        if end < 0:
            raise ScriptError(
                "Syntax error",
                line_no=src[:tokens[-end-1][1][0]].count('\n'))
        
        return self._build_tree(src, tokens, 0, 0)

    @staticmethod
    def tokenize(src, keywords):
        i = 0
        tokens = []
        while len(src) != i:            
            c = src[i]
            j, i = i, i+1
            if   c == ' ':
                continue
            elif c == '#':
                while ord(src[i]) >= 0x20:
                    i += 1
                continue
            elif c == '\n':
                while src[i] == '\n':
                    i += 1
                typ = 0x01
            elif c == '\04':
                typ = 0x02
            elif c.isdigit():
                if c == '0' and src[i] == 'x':
                    i += 1
                    while src[i].isdigit() or ('a' <= src[i].lower() <= 'f'):
                        i += 1
                else:
                    while src[i].isdigit():
                        i += 1
                typ = 0x03
            elif isword(c):
                while isword(src[i]):
                    i += 1
                try:
                    typ = 0x0A + keywords.index(src[j:i])
                except ValueError:
                    typ = 0x04
            elif c == '"':
                while src[i] != c:
                    if src[i] == '\\':
                        i += 1
                    i += 1
                typ, i = 0x05, i+1
            elif c in '<>':
                if src[i] == '=':
                    i += 1
                typ = 0x06
            elif c in '!=' and src[i] == '=':
                typ, i = 0x06, i+1
            else:
                typ = ord(c)
            
            tokens.append((typ, (j, i)))

        return tokens
        
    def _parse_tokens_using_rules(self, tokens, rules, rule=0):
        i, e = 0, 0
        gs = [None] * 1000  # [(expr, expr_i, i, si), ...]
        gi = 0
        expr = rules[rule]
        expr_i = 0
        
        def seek_group_end(expr, expr_i, pn=1):
            while True:
                c, expr_i = expr[expr_i], expr_i+1
                if   c in '[({':
                    pn += 1
                elif c in '])}':
                    pn -= 1
                    if not pn:
                        break
                elif c == '\x00':
                    break    
            return expr_i

        self.begin_expression(rule)
        
        while True:
            c, expr_i = expr[expr_i], expr_i+1
            
            # Control chars
            if   c == '\x00':
                if not gi:
                    break                
                gi -= 1
                expr, expr_i, j, _ = gs[gi]
                if i >= j:
                    self.end_expression()                
                continue                
            elif c == ' ':
                continue
            elif c == '\\':
                c, expr_i = expr[expr_i], expr_i+1
            elif c == '~':
                if tokens[i][0] == 0x01:
                    i = e = min(-i-1, e)
                continue
            elif c in '|(){}[]\\':
                if i < 0:
                    if c in '[({':
                        expr_i = seek_group_end(expr, expr_i)
                    else:
                        _, _, j, si = gs[gi-1]
                        self.reset(si)
                        if c == '|':
                            i = j
                        else:
                            gi -= 1
                            if c != ')':
                                i = j
                else:
                    if c == '|':
                        expr_i = seek_group_end(expr, expr_i) - 1
                    elif c in '[({':
                        gs[gi] = (expr, expr_i-1, i, self.i)
                        gi += 1
                    else:
                        gi -= 1
                        if c == '}':
                            expr_i = gs[gi][1]
                continue

            if i < 0:
                continue
            
            c = ord(c)
            
            # Terminal
            if c < 0x80:
                # Skip meaningful whitespace (newlines)
                # unless that's the next term
                if c != 0x01:
                    while tokens[i][0] == 0x01:
                        i += 1
                        self.terminal(i)
                        
                if tokens[i][0] == c:
                    i += 1
                    self.terminal(i)
                else:
                    i = e = min(-i-1, e)
            # Sub expression
            else:
                self.begin_expression(c)
                gs[gi], gi = (expr, expr_i, i, self.i), gi+1
                expr = rules[c]
                expr_i = 0

        self.end_expression()
                    
        return i                
        
    def _build_tree(self, src, tokens, i, lc):
        c = self.pt[i]
        if c > 0:
            id, (start, end) = tokens[c-1]
            if id == 0x01:
                pt, lc = None, lc+end-start
            else:
                pt = SyntaxTreeTerminal(id, src[start:end])
        else:
            pt, i = SyntaxTreeExpression(-c, lc), i+1
            while self.pt[i] != 0:
                sub_pt, i, lc = self._build_tree(src, tokens, i, lc)
                if sub_pt is not None:
                    pt.append(sub_pt)
        return pt, i+1, lc

#
# MAIN ENTRY POINT
#

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Pinky v. {}'.format(VERSION))
    parser.add_argument('sources', type=str, nargs='+',
                        help='source file(s)')
    parser.add_argument('-o', '--output',
                        help='output file; empty string to write to stdout')   
    parser.add_argument('-c', '--compile-only', action='store_true',
                        help='compile to jam code')
    parser.add_argument('--exclude-libs', action='store_true',
                        help='do not include libs')
    parser.add_argument('--debug', action='store_true',
                        help='compile with debug information')
    parser.add_argument('--profile', action='store_true',
                        help='run program via cProfile')
    parser.add_argument('--draw-sprite-bounds', action='store_true',
                        help='draw a red rectangle to indicate sprite bounds')
    parser.add_argument('--do-not-optimize', action='store_true',
                        help='use jamcode directly without trying to optimize it')
    
    args = parser.parse_args()

    name, ext    = args.sources[0].rsplit('.', 1)
    src_level    = {'ky': 0, 'jam': 1}[ext]
    compile_only = args.compile_only or args.output is not None
    use_stdout   = args.output == ""

    if args.exclude_libs:
        libs = []
    else:
        libs = None

    src_list = []
    for src_filename in args.sources:
        with open(src_filename, 'rt') as f:
            src_list.append((src_filename, f.read()))

    if src_level == 0:
        res_path = ''
        for src_filename in args.sources:
            src_path = src_filename.rsplit('/', 1)
            res_path = '' if len(src_path) == 1 else src_path[0] + '/'
        compiler = Compiler(libs, res_path=res_path, debug=args.debug)
        jamcode = compiler.compile(src_list)
    elif src_level == 1 and not compile_only:
        jamcode = src_list[0][1]
    else:
        raise ValueError('Source/target levels not supported')

    if compile_only:
        if use_stdout:
            print(jamcode)
        else:
            out_filename = args.output or '{}.{}'.format(name, 'jam')
            with open(out_filename, 'wt') as out_file:
                out_file.write(jamcode)
    else:
        from pinky import PinkyVM
        vm = PinkyVM.from_jamcode(jamcode, options=args.__dict__)
        try:
            if not args.profile:
                vm.run()
            else:
                import cProfile
                cProfile.run('vm.run()')
        except KeyboardInterrupt:
            pass
