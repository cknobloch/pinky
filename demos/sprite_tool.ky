struct Pt
    x : fp
    y : fp
end

struct SSAnimFrame
    src      : img
    off_x    : fp
    off_y    : fp
    src_x    : fp  # for editing only
    src_y    : fp  # for editing only
    w        : fp  # for editing only
    h        : fp  # for editing only        
end

struct SSAnim
    name     : str
    frames   : SSAnimFrame[]
    interp   : proc(fp,fp,fp:fp)
    i        : int
    n        : int
    duration : int
    loop     : bool
    max_w    : fp
    max_h    : fp
    hit_w    : fp
    hit_h    : fp
end

struct SSAnimPanel
    ai : int
    x  : fp
    y  : fp
    w  : fp
    h  : fp
end

proc acat(strs:str[]) : str
    return join("", strs)
end

proc get_box_tiles(src:img) : img[9]
    w = imgw(src) / 3
    h = imgh(src) / 3
    return {
        subimg(src, 0.0, 0.0, w, h),
        subimg(src, w, 0.0, w, h),
        subimg(src, w+w, 0.0, w, h),
        subimg(src, 0.0, h, w, h),
        subimg(src, w, h, w, h),
        subimg(src, w+w, h, w, h),
        subimg(src, 0.0, h+h, w, h),
        subimg(src, w, h+h, w, h),
        subimg(src, w+w, h+h, w, h)
    }
end

proc draw_quad(corner:img, x:fp, y:fp, w:fp, h:fp)
    x2 = x + w - imgw(corner)
    y2 = y + h - imgh(corner)
    draw(corner, x,  y)
    draw(flip(corner, FLIP_X ), x2, y)
    draw(flip(corner, FLIP_XY), x2, y2)
    draw(flip(corner, FLIP_Y ), x,  y2)
end

proc draw_stretched_box(tiles:img[9], x:fp, y:fp, w:fp, h:fp)
    edge_w = w - (imgw(tiles[0])+imgw(tiles[2]))
    edge_h = h - (imgh(tiles[0])+imgh(tiles[2]))

    draw(tiles[0], x, y)
    off_x = imgw(tiles[0])
    drawstretched(tiles[1], x+off_x, y, edge_w, imgh(tiles[1]))
    off_x = off_x + edge_w    
    draw(tiles[2], x+off_x, y)
    
    y = y + imgh(tiles[0])
    drawstretched(tiles[3], x, y, imgw(tiles[3]), edge_h)
    off_x = imgw(tiles[3])
    drawstretched(tiles[4], x+off_x, y, edge_w, edge_h)
    off_x = off_x + edge_w
    drawstretched(tiles[5], x+off_x, y, imgw(tiles[5]), edge_h)
    
    y = y + edge_h
    draw(tiles[6], x, y)
    off_x = imgw(tiles[6])
    drawstretched(tiles[7], x+off_x, y, edge_w, imgh(tiles[7]))
    off_x = off_x + edge_w    
    draw(tiles[8], x+off_x, y)
end

proc draw_quad_blinking(corner:img, x:fp, y:fp, w:fp, h:fp)
    while true do
        draw_quad(corner, x, y, w, h)
        after 10

        after 5
    end
end

proc draw_anim(anim:SSAnim, x:fp, y:fp)
    n = anim.n
    if n == 0 then
        return
    end
    
    i = anim.i
    d = anim.duration
    frame = anim.frames[i*n/d]
    
    draw(frame.src, x+frame.off_x, y+frame.off_y)
    
    if anim.loop then
        anim.i = mod(i+1, d)
    else
        anim.i = min(i+1, d-1)
    end
end

proc input(font:FixedFont, msg:str, w:int, x:fp, y:fp, vw:fp, vh:fp) : str
    max_chars = ftoi(vw/imgw(font.glyphs[0])) - slen(msg)
    out = blob(max_chars+1)
    i = 0
    loop = true
    while loop do
        keys = keystates()
        if keydown(keys, ANY_KEY) then
            if   keydown(keys, KEY_RETURN) then
                loop = false
            elif keydown(keys, KEY_ESCAPE) then
                i = 0
                loop = false
            elif keydown(keys, KEY_BACKSPACE) then
                i = max(0, i-1)
                set(out, i, 0x20)
            elif i < max_chars then
                k = KEY_A
                while k < KEY_Z do
                    if keydown(keys, k) then
                        if keyheld(keys, KEY_SHIFT) then
                            set(out, i, k)
                        else
                            set(out, i, k+32)
                        end
                        i = i + 1
                    end
                    k = k + 1
                end
                k = KEY_0
                while k < KEY_9 do
                    if keydown(keys, k) then
                        set(out, i, k)
                        i = i + 1
                    end
                    k = k + 1
                end
                if keydown(keys, KEY_SPACE) then
                    set(out, i, 0x20)
                    i = i + 1
                end
            else
                print("!")
            end
        end
        gbegin()
            draw_fixed_font(font,
                            concat(msg, gets(out, 0, i)),
                            x, y, ftoi(vw)/32, 1)
        gend()
        sleep(5)
    end
    return gets(out, 0, i)
end

proc calc_anim_max_dimensions(anim:SSAnim)
    max_w = 0.0
    max_h = 0.0  
    frames = anim.frames
    i = 0    
    while i < anim.n do
        max_w = fmax(max_w, frames[i].w)
        max_h = fmax(max_h, frames[i].h)        
        i = i + 1
    end
    anim.max_w = max_w
    anim.max_h = max_h
end

proc del_anim_frame(anim:SSAnim, i:int)
    frames = anim.frames
    anim.n = anim.n - 1
    n = anim.n
    while i < n do
        frames[i] = frames[i+1]
        i = i + 1
    end
end

proc ins_anim_frame(anim:SSAnim, i:int, frame:SSAnimFrame)
    frames = anim.frames
    n = anim.n
    if n == alen(frames) then
        frames = resize(frames, n+1) as SSAnimFrame[]
    end
    anim.frames = frames
    anim.n = n + 1
    while n > i do
        frames[n] = frames[n-1]
        n = n - 1
    end
    frames[i] = frame
end

proc unpack_anims(anims:SSAnim[], src:img) : SSAnim[]
    i = 0
    n = alen(anims)
    while i < n do
        anim = anims[i]
        if !is(anim, null) then
            j = 0
            while j < anim.n do
                f = anim.frames[j]
                f.src = subimg(src, f.src_x, f.src_y, f.w, f.h)
                j = j + 1
            end
            anim.interp = lerp  # FIXME: do not hardcode function
        end
        i = i + 1
    end
    return anims
end

proc run()
    screen_w = 800.0
    screen_h = 600.0
    
    ginit("Sprite Tool", screen_w, screen_h)

    # Load 16px default font
    font = create_fixed_font(
        blend(img(@"profont16.png"), BLEND_MUL, RED),
        16, 8, 2.0, 2.0, 1.0, 0.0)

    # Load images
    corner_cur      = img(@"cornermark.png")
    corner_mark     = img(@"cornermark2.png")    
    sprite_sheet    = img(@"HulksheetSNES.png")
    anim_sel_img    = img(@"cornermark.png")    
    anim_panel_img  = img(@"anim_panel.png")
    anim_panel_empty_img = img(@"anim_panel_empty.png")
    anim_origin_img = img(@"originmark.png")
    rounded_border_tiles = get_box_tiles(
        blend(img(@"roundedborder.png"), BLEND_MUL, BLACK))
    
    quit  = false
    spf   = 1000 / 30
    vw = screen_w
    vh = screen_h
    next_mode = 0
    mode = next_mode
    zoom = 1
    ax = 0
    ay = 0
    ai = 0
    fi = 0
    px = 0.0
    py = 0.0
    dx = 1.0
    dy = 1.0
    tx = px
    ty = py
    tw = 64.0
    th = 64.0

    cur_anim : SSAnim
    anims : SSAnim[]
    anims_blob = bload("sprite_tool.anims")
    if true then  # is(anims_blob, null) then
        anims = unpack_anims({
            {"anim_0", {{null,-8.0,-6.0,3.0,0.0,51.0,75.0}, {null,-9.0,-7.0,54.0,0.0,51.0,75.0}, {null,-9.0,-7.0,105.0,0.0,51.0,75.0}}, null, 4, 3, 30, true, 51.0, 75.0, 33.0, 62.0},
            null,
            {"anim_2", {{null,-3.0,-3.0,4.0,543.0,57.0,92.0}, {null,2.0,-3.0,61.0,543.0,51.0,92.0}, {null,10.0,-3.0,113.0,543.0,45.0,92.0}, {null,5.0,-3.0,159.0,543.0,59.0,92.0}, {null,5.0,-3.0,218.0,543.0,59.0,92.0}}, null, 16, 5, 30, true, 59.0, 92.0, 59.0, 92.0},
            null,
            {"anim_4", {{null,0.0,0.0,0.0,0.0,64.0,64.0}, {null,0.0,0.0,64.0,0.0,64.0,64.0}, {null,0.0,0.0,128.0,0.0,64.0,64.0}}, null, 18, 3, 30, true, 64.0, 64.0, 64.0, 64.0},
            null,
            null,
            null,
            null,
            null,
            null,
            null},
            sprite_sheet)
    else
        anims = unpack_anims(unpack({anims_blob, 0}) as SSAnim[], sprite_sheet)
    end

    # Initialize anim panels
    anim_panels = array(alen(anims)) as SSAnimPanel[12]    
    anim_grid_w = 3
    anim_grid_h = 2
    num_anim_pages = alen(anim_panels) / (anim_grid_w*anim_grid_h)
    anim_w  = screen_w / anim_grid_w
    anim_h  = screen_h / anim_grid_h
    anim_px = (anim_w-256.0) / 2
    anim_py = (anim_h-256.0) / 2
    
    i = 0
    n = alen(anim_panels)
    while i < n do
        j = mod(i, 6)
        anim_panels[i] = {
            i, 
            anim_px+anim_w*itof(mod(j, anim_grid_w)),
            anim_py+anim_h*itof((j/anim_grid_w)),
            256.0,
            256.0} as SSAnimPanel
        i = i + 1
    end
    
    while !quit do
        if   progstate() == 0 then
            quit = true
        elif progstate() == 2 then
            sleep(1)
        else
            t = time()
            keys = keystates()

            if mode == 0 then
                #
                # BROWSE ANIMATIONS
                #

                ai = ax + ay * anim_grid_w

                quit = keydown(keys, KEY_ESCAPE)
                
                if   keydown(keys, KEY_RETURN) then
                    if is(anims[ai], null) then
                        cur_anim = {"",
                                    array(10) as SSAnimFrame[],
                                    lerp,
                                    0, 1, 30,
                                    true,
                                    0.0, 0.0,
                                    0.0, 0.0}
                        cur_anim.frames[0] = {null, 0.0, 0.0, 0.0, 0.0, tw, th}
                    else
                        cur_anim = anims[ai]
                    end
                    fi = 0
                    mode = 1
                elif keydown(keys, KEY_E) then
                    mode = 2
                elif keydown(keys, KEY_LEFT) then
                    ax = mod(ax-1, 3)
                elif keydown(keys, KEY_RIGHT) then
                    ax = mod(ax+1, 3)
                elif keydown(keys, KEY_UP) then
                    ay = mod(ay-1, num_anim_pages*anim_grid_h)
                elif keydown(keys, KEY_DOWN) then
                    ay = mod(ay+1, num_anim_pages*anim_grid_h)
                elif keydown(keys, KEY_BACKSPACE) then
                    anims[ai] = null
                end
                
                page = ay / anim_grid_h
                page_y = page * anim_grid_h
                
                gbegin()
                    clear(WHITE)
                    
                    # Draw grid of animations!
                    i = 0
                    n = 6
                    while i < n do
                        j = page_y * anim_grid_w + i
                        anim_panel = anim_panels[j]
                        anim = anims[anim_panel.ai]
                        if is(anim, null) then
                            draw(anim_panel_empty_img,
                                 anim_panel.x,
                                 anim_panel.y)
                        else
                            draw(anim_panel_img, anim_panel.x, anim_panel.y)
                            draw_anim(
                                anim,
                                anim_panel.x+(anim_panel.w-anim.max_w)/2,
                                anim_panel.y+(anim_panel.h-anim.max_h)/2)
                        end
                        i = i + 1
                    end                    

                    # Draw selector
                    #draw_quad_blinking(
                    #    corner_cur,
                    #    itof(anim_w*ax+anim_px),
                    #    itof(anim_h*(ay-page_y)+anim_py),
                    #    256.0,
                    #    256.0)
                        
                    # Draw selector/border/box
                    draw_stretched_box(rounded_border_tiles,
                                       anim_px+anim_w*ax,
                                       anim_py+anim_h*(ay-page_y),
                                       256.0, 256.0)

                    # Draw title
                    draw_fixed_font(font, "Sprite Tool", 4.0, 0.0, 50, 2)

                    # Draw page number
                    page_str = itos(page)
                    draw_fixed_font(font,
                                    page_str,
                                    screen_w-16.0*slen(page_str),
                                    0.0,
                                    11, 1)
                gend()
            elif mode == 1 then
                #
                # SPRITE SHEET SELECTOR
                #

                cur_frame = cur_anim.frames[fi]
            
                # HANDLE INPUT
                
                if   keydown(keys, KEY_Z) then
                    zoom = mod(zoom+1, 3)
                    if   zoom == 0 then
                        vw = imgw(sprite_sheet)
                        vh = imgh(sprite_sheet)
                    elif zoom == 1 then
                        vw = screen_w
                        vh = screen_h
                    elif zoom == 2 then
                        vw = tw*2
                        vh = th*2
                    end                    
                elif keydown(keys, KEY_TAB) then
                    fi = mod(fi+1, cur_anim.n)
                elif keydown(keys, KEY_N) then
                    ins_anim_frame(
                        cur_anim,
                        cur_anim.n,
                        {null,
                         0.0, 0.0,
                         cur_frame.src_x+cur_frame.w,
                         cur_frame.src_y,
                         cur_frame.w,
                         cur_frame.h} as SSAnimFrame)
                    fi = cur_anim.n - 1
                elif keydown(keys, KEY_I) then
                    ins_anim_frame(
                        cur_anim,
                        fi,
                        {null,
                         0.0, 0.0,
                         cur_frame.src_x+cur_frame.w,
                         cur_frame.src_y,
                         cur_frame.w,
                         cur_frame.h} as SSAnimFrame)
                elif keydown(keys, KEY_BACKSPACE) then
                    del_anim_frame(cur_anim, fi)
                    fi = mod(fi, cur_anim.n)
                elif keydown(keys, KEY_RETURN) then
                    calc_anim_max_dimensions(cur_anim)
                    cur_anim.hit_w = cur_anim.max_w
                    cur_anim.hit_h = cur_anim.max_h                    
                    anims[ai] = cur_anim
                    mode = 0
                elif keydown(keys, KEY_ESCAPE) then
                    mode = 0
                end

                # good chance input above updated the current frame
                cur_frame = cur_anim.frames[fi]

                tx = cur_frame.src_x
                ty = cur_frame.src_y
                tw = cur_frame.w
                th = cur_frame.h

                if keyheld(keys, KEY_SHIFT) then
                    if   keydown(keys, KEY_RIGHT) then
                        tw = fmin(tw+1.0, 128.0)
                    elif keydown(keys, KEY_LEFT) then
                        tw = fmax(tw-1.0, 1.0)
                    end
                    if   keydown(keys, KEY_DOWN) then
                        th = fmin(th+1.0, 128.0)
                    elif keydown(keys, KEY_UP) then 
                        th = fmax(th-1.0, 1.0)
                    end
                elif keyheld(keys, KEY_ALT) then
                    if   keydown(keys, KEY_LEFT) then
                        tx = tx - tw - px
                    elif keydown(keys, KEY_RIGHT) then
                        tx = tx + tw + px
                    elif keydown(keys, KEY_UP) then
                        ty = ty - th - py
                    elif keydown(keys, KEY_DOWN) then
                        ty = ty + th + py
                    end
                else
                    tw = cur_frame.w
                    th = cur_frame.h

                    if   keydown(keys, KEY_LEFT) then
                        tx = tx - dx - px
                    elif keydown(keys, KEY_RIGHT) then
                        tx = tx + dx + px
                    elif keydown(keys, KEY_UP) then
                        ty = ty - dy - py
                    elif keydown(keys, KEY_DOWN) then
                        ty = ty + dy + py
                    end
                end

                tx = fmod(tx, imgw(sprite_sheet)-tw)
                ty = fmod(ty, imgh(sprite_sheet)-th)

                # Save back to frame immediately
                cur_frame.src_x = tx
                cur_frame.src_y = ty
                cur_frame.w     = tw
                cur_frame.h     = th
                cur_frame.src   = subimg(sprite_sheet, tx, ty, tw, th)    
                
                # DRAW

                gbegin()
                    setviewport(*viewport(
                        tx+(tw-vw)/2,
                        ty+(th-vh)/2,
                        vw,
                        vh,
                        imgw(sprite_sheet),
                        imgh(sprite_sheet),
                        screen_w,
                        screen_h))
                    
                    clear(WHITE)

                    draw(sprite_sheet, 0.0, 0.0)

                    draw_quad_blinking(corner_cur, tx, ty, tw, th)

                    setviewport(0.0, 0.0, 0.0)

                    # Draw anim frame number
                    frame_str = itos(fi)
                    draw_fixed_font(font,
                                    frame_str,
                                    screen_w-(17.0*slen(frame_str)),
                                    0.0,
                                    11, 1)
                gend()
                
            elif mode == 2 then
                #
                # SPRITE FRAME EDITOR
                #

                cur_anim  = anims[ai]
                cur_frame = cur_anim.frames[fi]                
                
                if keydown(keys, KEY_TAB) & keyheld(keys, KEY_SHIFT) then
                    fi = mod(fi-1, cur_anim.n)
                elif keydown(keys, KEY_TAB) then
                    fi = mod(fi+1, cur_anim.n)
                elif keydown(keys, KEY_RETURN) then
                    mode = 0
                end

                if keyheld(keys, KEY_SHIFT) then
                    tx = 0.0
                    ty = 0.0
                
                    if   keydown(keys, KEY_LEFT) then
                        tx = tx - 1.0
                    elif keydown(keys, KEY_RIGHT) then
                        tx = tx + 1.0
                    elif keydown(keys, KEY_UP) then
                        ty = ty - 1.0
                    elif keydown(keys, KEY_DOWN) then
                        ty = ty + 1.0
                    elif keydown(keys, KEY_W) then
                        cur_anim.hit_w = cur_anim.hit_w - 1.0
                    elif keydown(keys, KEY_H) then
                        cur_anim.hit_h = cur_anim.hit_h - 1.0
                    end
                    
                    # Save back to frames immediately
                    i = 0
                    n = cur_anim.n
                    while i < n do
                        f = cur_anim.frames[i]
                        f.off_x = f.off_x - tx
                        f.off_y = f.off_y - ty
                        i = i + 1
                    end
                else
                    tx = cur_frame.off_x
                    ty = cur_frame.off_y
                
                    if   keydown(keys, KEY_LEFT) then
                        tx = tx - 1.0
                    elif keydown(keys, KEY_RIGHT) then
                        tx = tx + 1.0
                    elif keydown(keys, KEY_UP) then
                        ty = ty - 1.0
                    elif keydown(keys, KEY_DOWN) then
                        ty = ty + 1.0
                    elif keydown(keys, KEY_W) then
                        cur_anim.hit_w = cur_anim.hit_w + 1.0
                    elif keydown(keys, KEY_H) then
                        cur_anim.hit_h = cur_anim.hit_h + 1.0
                    end
                    
                    # Save back to frame immediately
                    cur_frame.off_x = tx
                    cur_frame.off_y = ty
                end

                gbegin()
                    setviewport(*unbound_viewport(
                        -(cur_anim.max_w-cur_anim.hit_w)/2,
                        -(cur_anim.max_h-cur_anim.hit_h)/2,
                        cur_anim.max_w,
                        cur_anim.max_h,
                        screen_w,
                        screen_h))
                    
                    clear(WHITE)              

                    # Draw onion skins
                    i = mod(fi+1, cur_anim.n)
                    n = cur_anim.n
                    while i != fi do
                        f = cur_anim.frames[i]
                        draw(blend(f.src,
                                   BLEND_MUL,
                                   argb(abs((fi-i)*128/n), 0xFF, 0xFF, 0xFF)),
                             f.off_x,
                             f.off_y)
                        i = mod(i+1, n)
                    end

                    # Draw hit box
                    draw_quad(corner_cur,
                              0.0,
                              0.0,
                              cur_anim.hit_w,
                              cur_anim.hit_h)

                    # Draw current anim frame
                    draw(cur_frame.src,
                         cur_frame.off_x,
                         cur_frame.off_y)

                    setviewport(0.0, 0.0, 0.0)

                    # Draw current anim frame number
                    frame_str = itos(fi)
                    draw_fixed_font(font,
                                    frame_str,
                                    screen_w-17.0*slen(frame_str),
                                    0.0,
                                    11, 1)
                gend()
            end
            
            sleep(max(1, spf-(time()-t)))
        end
    end

    # Print out animations
    i = 0
    n = alen(anims)
    anim_strs = array(n) as str[]    
    while i < n do
        anim = anims[i]
        if is(anim, null) then
            anim_str = "null"
        else
            anim_frame_strs = array(anim.n) as str[]
            j = 0
            while j < anim.n do
                f = anim.frames[j]
                anim_frame_strs[j] = acat(
                    {"{",
                     join(",",
                         {"null",
                          ftos(f.off_x),
                          ftos(f.off_y),
                          ftos(f.src_x),
                          ftos(f.src_y),
                          ftos(f.w),
                          ftos(f.h)}),
                     "}"})
                j = j + 1
            end
            anim_frames_str = acat(
                {"{", join(", ", anim_frame_strs), "}"})
            anim_str = acat(
                {"{",
                 join(", ", {
                     acat({"\"anim_", itos(i), "\""}),
                     anim_frames_str,
                     "null",
                     itos(anim.i),
                     itos(anim.n),
                     itos(anim.duration),
                     booltostr(anim.loop),
                     ftos(anim.max_w),
                     ftos(anim.max_h),
                     ftos(anim.hit_w),
                     ftos(anim.hit_h)}),
                 "}"})
        end
        anim_strs[i] = anim_str
        i = i + 1
    end

    print("{")
    print(join(",\n", anim_strs))
    print("}")

    print("Saving anims...")

    bsave(packed(anims), "sprite_tool.anims")

    print("Done")
end
